package com.SMIT.WallPostSoftware;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.WindowManager;

import com.SMIT.WallPostSoftware.Extras.Preference;

import java.util.ArrayList;

import rebus.permissionutils.AskagainCallback;
import rebus.permissionutils.FullCallback;
import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionManager;


public class SplashScreenActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen_activity);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            // only for gingerbread and newer versions
            PermissionManager.with(SplashScreenActivity.this)
                    .permission(PermissionEnum.READ_PHONE_STATE, PermissionEnum.WRITE_EXTERNAL_STORAGE)
                    .askagain(true)
                    .askagainCallback(new AskagainCallback() {
                        @Override
                        public void showRequestPermission(AskagainCallback.UserResponse response) {
                            showDialog(response);
                        }
                    })
                    .callback(new FullCallback() {
                        @Override
                        public void result(ArrayList<PermissionEnum> permissionsGranted, ArrayList<PermissionEnum> permissionsDenied, ArrayList<PermissionEnum> permissionsDeniedForever, ArrayList<PermissionEnum> permissionsAsked) {

                            for (PermissionEnum permissionEnum: permissionsGranted){
                                handler();
                            }
                        }
                    })
                    .ask();
        }else {
            handler();
        }

    }


    private void showDialog(final AskagainCallback.UserResponse response) {
        new AlertDialog.Builder(SplashScreenActivity.this)
                .setTitle("Permission needed")
                .setMessage("This app realy need to use this permission, you wont to authorize it?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        response.result(true);
                    }
                })
                .setNegativeButton("NOT NOW", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        response.result(false);
                    }
                })
                .show();
    }

    public void handler(){

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {

//                SharedPreferences pref = getSharedPreferences("login", Context.MODE_PRIVATE);
//                pref.edit().clear().apply();
//                SharedPreferences pref = getSharedPreferences("login", 0);
//                Boolean bool = pref.getBoolean("remember",false);
//

//                Intent i = new Intent(SplashScreenActivity.this, WP_EMP_LoginActivity.class);
//                startActivity(i);
                Preference pref = new Preference(getApplicationContext(),"login");


                //Log.e("STATUS",""+bool);

                if(pref.getLogged()){
                    Log.d("BOLLEN ","FALSe");
                    Intent i = new Intent(SplashScreenActivity.this, HomeScreenActivity.class);
                    startActivity(i);

                }else {

                    Log.d("BOLLEN ","True");

                    Intent i = new Intent(SplashScreenActivity.this, WP_EMP_LoginActivity.class);
                    startActivity(i);
                }

                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionManager.handleResult(requestCode, permissions, grantResults);


    }

}
