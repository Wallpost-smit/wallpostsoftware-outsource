package com.SMIT.WallPostSoftware;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Adapter.CompanyListAdapter;
import com.SMIT.WallPostSoftware.Extras.FileUtils;
import com.SMIT.WallPostSoftware.Extras.Preference;
import com.SMIT.WallPostSoftware.Extras.RoundedImageView;
import com.SMIT.WallPostSoftware.Model_Class.CompanyList;

import java.util.ArrayList;

public class CompanyListActivity extends AppCompatActivity {

    private ArrayList<CompanyList> cmpnyList = new ArrayList<>();
    private RecyclerView recyclerView;
    private CompanyListAdapter mAdapter;

    TextView _empCompany,_empNametv,_empDesignationtv, _empLineManagertv;
    int selectedItem;
    CompanyList selected_company;
    SharedPreferences pref;
    LinearLayout column2,column3;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.company_list_activity);

        column2 = (LinearLayout)findViewById(R.id.column3);
        column3 = (LinearLayout)findViewById(R.id.column2);
        recyclerView = (RecyclerView) findViewById(R.id.company_list_recycler_view);

        setTitle("User Profile");

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        final Preference preference = new Preference(getApplicationContext(),"login");
        cmpnyList = preference.getCompanyList();
         selected_company= preference.getCompany();

        selectedItem =0;
        //Log.d("LENTTT ", empImage);
        storeCompanyDetails();

        for (int i=0 ;i<cmpnyList.size();i++){
            if (selected_company.companyName.equals(cmpnyList.get(i).companyName)){
                selectedItem = i;
            }
        }

        displayUserInfo();



         _empNametv = (TextView)findViewById(R.id.employee_name);
         _empDesignationtv = (TextView)findViewById(R.id.designation);
         _empLineManagertv = (TextView)findViewById(R.id.line_manager);


        _empNametv.setText(selected_company.employee.empName);
        _empDesignationtv.setText(selected_company.employee.empDesignation);
        _empLineManagertv.setText(selected_company.employee.empLineManager);



        // Either Company name or Employee Email or Position will be shown
        _empCompany = (TextView)findViewById(R.id.employee_company);
        _empCompany.setText(selected_company.companyName);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter = new CompanyListAdapter(cmpnyList, new CompanyListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {
                selectedItem = position;
                mAdapter.notifyDataSetChanged();
                selected_company= cmpnyList.get(position);
                final Preference preference = new Preference(getApplicationContext(),"login");
                preference.setCompany(selected_company);
                _empCompany.setText(selected_company.companyName);
                _empNametv.setText(selected_company.employee.empName);
                _empDesignationtv.setText(selected_company.employee.empDesignation);
                _empLineManagertv.setText(selected_company.employee.empLineManager);
                displayUserInfo();

            }
        },selectedItem);

        recyclerView.setAdapter(mAdapter);
    }


    public void displayUserInfo(){
        if (selected_company.companyName.isEmpty()){
            selected_company = cmpnyList.get(selectedItem);
        }
        if(selected_company.employee.empProfilePic.length() > 1){
            loadImage(selected_company.employee.empProfilePic);
        }


        if (selected_company.employee.empDesignation.isEmpty()){
            column2 = (LinearLayout)findViewById(R.id.column3);
            column2.setVisibility(View.GONE);
        }
        else{
            column2.setVisibility(View.VISIBLE);
        }
        if (selected_company.employee.empLineManager.isEmpty()){
           column3 = (LinearLayout)findViewById(R.id.column2);
            column3.setVisibility(View.GONE);
        }
        else{
            column3.setVisibility(View.VISIBLE);
        }

    }

    public void loadImage(String imgUrl){
        RoundedImageView _imgView = (RoundedImageView)findViewById(R.id.imageView);
        FileUtils.loadImage(imgUrl,getApplicationContext(),_imgView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.company_list_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_selected) {
            Intent intent = new Intent(CompanyListActivity.this, HomeScreenActivity.class);
            storeCompanyDetails();
            startActivity(intent);
            return true;
        }else if(id == android.R.id.home){
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void storeCompanyDetails(){
        Preference preference = new Preference(getApplicationContext(),"login");
        preference.setCompany(cmpnyList.get(selectedItem));
    }



}
