package com.SMIT.WallPostSoftware.Extras;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;

import static java.util.Calendar.getInstance;


/**
 * Created by my pc on 7/6/2015.
 */
public class CommonUtils {

    private CommonUtils() {

    }

    public static String getCurrentTimeInSeconds() {

        String timeStamp = String.valueOf(System.currentTimeMillis() / 1000);
        return timeStamp;
    }

    public static void getCurrentTimeStamp() {
          /*  Calendar c = Calendar.getInstance();
            System.out.println("Current time =&gt; "+c.getTime());*/

           /* SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String formattedDate = df.format(c.getTime());*/
        try {
            // String timeStamp = String.valueOf(System.currentTimeMillis()/1000);
            String timeStamp = String.valueOf(System.currentTimeMillis() / 1000);
            Log.v("UNIX", "UNIX=" + timeStamp);
            Date date = Calendar.getInstance().getTime();
            Log.v("timeStamp", "date=" + date);
            Log.v("timeStamp", "timeStamp=" + date.getTime() / 1000L);
        } catch (Exception e) {

        }

    }


//    public static String getFormattedDateFromDefaultDateString(String defaultDateString) {
//        SimpleDateFormat defaultFormat = new SimpleDateFormat(Constants.DateTimeFormat.SYSTEM_DATE_FORMAT);
//        SimpleDateFormat newFormat = new SimpleDateFormat(Constants.DateTimeFormat.DISPLAY_DATE_FORMAT);
//        Date date = null;
//        try {
//            date = defaultFormat.parse(defaultDateString);
//            return newFormat.format(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return defaultDateString;
//    }

//    public static String getFormattedTimeFromDefaultTimeString(String defaultTimeString) {
//        SimpleDateFormat defaultFormat = new SimpleDateFormat(Constants.DateTimeFormat.SYSTEM_TIME_FORMAT);
//        SimpleDateFormat newFormat = new SimpleDateFormat(Constants.DateTimeFormat.DISPLAY_TIME_FORMAT);
//        Date date = null;
//        try {
//            date = defaultFormat.parse(defaultTimeString);
//            return newFormat.format(date);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
//        return defaultTimeString;
//    }

    public static String getCurrentTimeInMillis() {

        String timeStamp = String.valueOf(System.currentTimeMillis());
        return timeStamp + getRandomNumber();

    }

    public static String getRandomNumber() {
        Random r = new Random();
        int i1 = r.nextInt(20000 - 10000) + 10000;
        return String.valueOf(i1);
    }

    public static String getUUID() {
        String uniqueId = UUID.randomUUID().toString();
        return uniqueId;
    }

    public static String getReFormattedDateAndTime(String inDate, String startTime, String endTime) {
        String fDate = getFormattedDate(inDate);
        String fStartTime = getFormattedTime(startTime);
        String fSndTime = getFormattedTime(endTime);
        StringBuilder s = new StringBuilder();
        s.append(fStartTime);
        s.append(" To ");
        s.append(fSndTime);
        s.append(", ");
        s.append(fDate);
        return s.toString();
    }

    public static String getFormattedTime(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static void longInfo(String str) {
        if (str.length() > 4000) {
            Log.i("TAG", str.substring(0, 4000));
            longInfo(str.substring(4000));
        } else
            Log.i("TAG", str);
    }

    public static String getFormattedDate(String _date) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd MMM yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(_date);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String convertTimeStampToFormattedDate(String timeStamp) {
        if (timeStamp == null)
            return "";
        long unixSeconds = Long.parseLong(timeStamp);
        Date date = new Date(unixSeconds * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("h:mm a MMM d, yyyy"); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        return formattedDate;

    }

    public static String convertTimeStampToDate(String timeStamp) {
        if (timeStamp == null)
            return "";
        long unixSeconds = Long.parseLong(timeStamp);
        Date date = new Date(unixSeconds * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d, yyyy"); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        return formattedDate;

    }

    public static void disableEditText(EditText editText) {
        editText.setFocusable(false);
        editText.setEnabled(false);
        editText.setCursorVisible(false);
        editText.setKeyListener(null);
    }

    public static String getCurrentDate() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String getDateAndTime() {
        DateFormat df = new SimpleDateFormat("h:mm a MMM d, yyyy");
        String date = df.format(getInstance().getTime());
        return date;
    }

    public static String getCurrentDateAndTime() {
        return new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
    }


//    public static void startSync(Context context) {
//
//        ConnectionInfo connectionInfo = new ConnectionInfo(context);
//        if (connectionInfo.isConnectingToInternet()) {
//            if (!SyncService.getInstance()) {
//                Intent serviceIntent = new Intent(context, SyncService.class);
//                context.startService(serviceIntent);
//            }
//
//          /*  ClassicSingleton classicSingleton = ClassicSingleton.getInstance(context);
//            classicSingleton.pushImage();*/
//        }
//    }

    private boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public static String getReference() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-SSSSSS");
        StringBuffer buffer = new StringBuffer();
        buffer.append("AD-");
        Date now = new Date();
        String strDate = sdf.format(now);
        buffer.append(strDate);
        return buffer.toString();
    }

    /*
     * Keyboard hide and show method
     */

    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getCurrentFocus().getWindowToken(), 0);
    }

    public static void showSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }


//    public static String getPrintableOccupationNameForCategory(CategoriesNamesItems category) {
//        String printableName;
//        if (category.getName().equals("carpentry")) printableName = "Carpenters";
//        else if (category.getName().equals("plumbing")) printableName = "Plumbers";
//        else if (category.getName().equals("electrical")) printableName = "Electricians";
//        else printableName = category.getName();
//        return printableName;
//    }

    public static String saveBitmap(Bitmap bitmap) {
        String url = null;
        try {
            url = getFileStoragePath();
            File photo = new File(url);
            saveBitmapAsJPG(bitmap, photo);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return url;
    }

    public static void saveBitmapAsJPG(Bitmap bitmap, File photo) throws IOException {
        Bitmap newBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(newBitmap);
        canvas.drawColor(Color.WHITE);
        canvas.drawBitmap(bitmap, 0, 0, null);
        OutputStream stream = new FileOutputStream(photo);
        newBitmap.compress(Bitmap.CompressFormat.JPEG, 80, stream);
        stream.close();
    }

    public static String getFileStoragePath() {
        File file = new File(Constants.LOCAL_IMAGE_BASE_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + CommonUtils.getUUID() + ".jpg");
        return uriSting;
    }

}
