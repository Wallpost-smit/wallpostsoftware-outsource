package com.SMIT.WallPostSoftware.Extras;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by alex on 22/08/16.
 */
public class WebManager {
    Context context;

    String authToken;
    String _lvId, _contactNo, _contactEmail, _reason;
    long _lvFrm, _lvTo;
    SharedPreferences pref;
    RetryPolicy policy;

    int socketTimeout = 30000;

    public WebManager(Context context) {
        this.context = context;
        Preference pref= new Preference(context,"login");
        authToken = pref.getAuthToken();
        //30 seconds - change to what you want
        policy = new DefaultRetryPolicy(socketTimeout, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        Log.d("WEB MANGER TOK ", authToken);
    }


    public JSONObject params = new JSONObject();

    public void addParams(String key, int value){
        try {
            params.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addParams(String key, float value){
        try {
            params.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addParams(String key, double value){
        try {
            params.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addParams(String key, Long value){
        try {
            params.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    public void addParams(String key, String value){
        if(value != null && value.length()>0){
            try {
                params.put(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    public void addParams(String key, JSONObject value){
        try{
            params.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addParams(String key, JSONArray value){
        try{
            params.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addParams(String key, ArrayList<String> fileList){
        try{
            params.put(key, fileList);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public WebManager(){}

    //LOGIN URL API

    public void Login(String url, final VolleyCallback callback){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url,params,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context,
                        VolleyErrorHelper.getMessage(error,context),
                        Toast.LENGTH_LONG).show();
                callback.onError(VolleyErrorHelper.getMessage(error,context));
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave Req Send Call");
    }


    //LOGOUT URL API
    public void LogOut(String url, final VolleyCallback callback){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                callback.onError(error.toString());

                Toast.makeText(context,
                        VolleyErrorHelper.getMessage(error,context),
                        Toast.LENGTH_LONG).show();
               ;

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(policy);

        AppController.getInstance().addToRequestQueue(jsonObjReq, "Log Out Call");
    }


    //LEAVE LIST API
    public  void LeaveList(String url, final VolleyCallback callback){

        Log.d("AUTH TOKEN LV LIST ", authToken);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                callback.onError(error.toString());
                error.printStackTrace();
                Toast.makeText(context,
                        VolleyErrorHelper.getMessage(error,context),
                        Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(policy);

        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave Req Call");

    }

    //LEAVE REQ SETTINGS API
    public void LeavReq(String url,  final VolleyCallback callback){

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                callback.onError(VolleyErrorHelper.getMessage(error,context));
//                error.printStackTrace();
//                Toast.makeText(context,
//                        VolleyErrorHelper.getMessage(error,context),
//                        Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave Req Call");

    }

    //AIRPORT DETAILS API
    public void AirportData(String url, final VolleyCallback callback){
        Log.d("AirportData AUTH TOKEN ", authToken);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(VolleyErrorHelper.getMessage(error,context));


            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave Alert Call");
    }

    //LEAVE ALERT URL
    public void LeavAlert(String url, final VolleyCallback callback){


        Log.d("AUTH TOKEN ", authToken);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.equals(error);
                Toast.makeText(context,
                        VolleyErrorHelper.getMessage(error,context),
                        Toast.LENGTH_LONG).show();
                String json = null;
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){

                    json = new String(response.data);
                    try{
                        Log.d("BEFOR OBJ ", "NOT ENTER");
                        JSONObject obj = new JSONObject(json);
                        Log.d("AFTR obj ", obj.toString());
                        callback.onError(String.valueOf(obj));
                        //trimmedString = obj.getString(key);
                    } catch(JSONException e){
                        e.printStackTrace();
                    }

                }

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave Alert Call");
    }

    //SEND LEAVE REQ API
    public void sendLvReq(String url, final VolleyCallback callback){

        Log.d("PARAMAMA ", params.toString());
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url,params,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.onError(error.toString());
                Toast.makeText(context,
                        VolleyErrorHelper.getMessage(error,context),
                        Toast.LENGTH_LONG).show();
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;

            }
        };

        jsonObjReq.setRetryPolicy(policy);

        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave Req Send Call");
    }

    //Cancel LEAVE REQ API
    public void cancelLvReq( String url, String lvId,String reason,final VolleyCallback callback){

        this._lvId = lvId;
        this._reason = reason;

        //

        JSONObject params = new JSONObject();
        try {
            params.put("id",_lvId);
            params.put("cancel_reason",reason);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url,params,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(context,
                        VolleyErrorHelper.getMessage(error,context),
                        Toast.LENGTH_LONG).show();
                callback.onError(error.toString());
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;

            }
        };
        jsonObjReq.setRetryPolicy(policy);

        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave Cancel Call");
    }

    //LEAVE APPROVAL LIST
    public void leavApprvList(String url, final VolleyCallback callback){

        Log.d("AUTH TOKEN ", authToken);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.equals(error);
                Toast.makeText(context,
                        VolleyErrorHelper.getMessage(error,context),
                        Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave Apprv List Call");
    }

    //LEAVE ACTION
    public void leavAction(String url, JSONObject jsonObject, final VolleyCallback callback){

        Log.d("MMM ", url);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                url,jsonObject,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {

                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context,
                        VolleyErrorHelper.getMessage(error,context),
                        Toast.LENGTH_LONG).show();
                callback.onError(VolleyErrorHelper.getMessage(error,context));
            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;

            }
        };
        jsonObjReq.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave ACTION Call");

    }


    //LEAVE DETAIL API
    public void leaveDetail(String url, final VolleyCallback callback){


        Log.d("AUTH TOKEN ", authToken);
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                url,
                new Response.Listener<JSONObject>(){
                    @Override
                    public void onResponse
                            (JSONObject response) {
                        callback.onSuccess(response.toString());
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                callback.equals(error);
                Toast.makeText(context,
                        VolleyErrorHelper.getMessage(error,context),
                        Toast.LENGTH_LONG).show();

            }
        }) {

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("auth-token", authToken);
                return headers;
            }

        };
        jsonObjReq.setRetryPolicy(policy);
        AppController.getInstance().addToRequestQueue(jsonObjReq, "Leave Detail Call");
    }

    public interface VolleyCallback{
        void onSuccess(String result);
        void onError(String result);
    }
}
