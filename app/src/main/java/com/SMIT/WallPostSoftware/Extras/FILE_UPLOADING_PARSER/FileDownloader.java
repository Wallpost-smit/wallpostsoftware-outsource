package com.SMIT.WallPostSoftware.Extras.FILE_UPLOADING_PARSER;

import android.content.Context;
import android.widget.ImageView;

import com.SMIT.WallPostSoftware.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class FileDownloader {
    private static final int MAX_WIDTH = 1024;
    private static final int MAX_HEIGHT = 768;
    public interface FileDownloadListener {
        void didDownloadFile(boolean didDownload, String fileName);
    }

    public static void setImage(final String file, final ImageView imageView, final Context context) {

            Picasso.with(context)
                    .load(file)
                    .placeholder(R.drawable.progress_animation)
                    .resize(MAX_WIDTH,MAX_HEIGHT)
                    .centerCrop()
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {

                        }
                    });
        }




}
