package com.SMIT.WallPostSoftware.Extras.FILE_UPLOADING_PARSER;

import android.content.Context;
import android.util.Log;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.Preference;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.SelectImage;

import org.json.JSONObject;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


/**
 * Created by Pratik Butani
 */
public class File_uploading_JSONParser {

    private static final String URL_UPLOAD_IMAGE = "http://api3.wallpostsoftware.com/api/v1/fileupload/temp";
    public static String authToken;


    public static JSONObject uploadImages(Context context, ArrayList<SelectImage> selectImageArray){
        Preference pref= new Preference(context,"login");
        authToken = pref.getAuthToken();

        String sourceImage;
        try {

            //TEST COMMIT

            MultipartBody.Builder multipartBuilder= new MultipartBody.Builder().setType(MultipartBody.FORM);

            int length = selectImageArray.size();
            int noOfImageToSend = 0;
            String KEY = "file_name";
            for(int i = 0; i < length; i++) {
                /**
                 * Getting Photo Caption and URL
                 */
                sourceImage = selectImageArray.get(i).getImagePath();

                File sourceFile = new File(sourceImage);

                if (sourceFile.exists()) {
                    final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/*");

                    String filename = sourceImage.substring(sourceImage.lastIndexOf("/")+1);

                    multipartBuilder.addFormDataPart(KEY + i, filename , RequestBody.create(MEDIA_TYPE_PNG, sourceFile));

                    Log.d("TETETET ", sourceImage);
                    noOfImageToSend++;

                }
            }
            MultipartBody requestBody = multipartBuilder.build();

            Log.d("REQUEST BODY ", requestBody.toString());

            Request request = new Request.Builder()
                    .url(Constants.FILE_UPLOAD_URL)
                    .addHeader("auth-token", authToken)
                    .post(requestBody)
                    .build();

            OkHttpClient client = new OkHttpClient();
            client.readTimeoutMillis();
            client.connectTimeoutMillis();
            Response response = client.newCall(request).execute();

            /** Your Response **/
            String responseStr = response.body().string();

            Log.d("BANG BANG ", responseStr);

            return new JSONObject(responseStr);

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("ALEX", "Error: " + e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("ALEX", "Other Error: " + e.getLocalizedMessage());
        }
        return null;
    }
    public static JSONObject uploadImage(String sourceImageFile) {

        try {
            File sourceFile = new File(sourceImageFile);

            Log.d("TAG", "File...::::" + sourceFile + " : " + sourceFile.exists());

            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/*");

            String filename = sourceImageFile.substring(sourceImageFile.lastIndexOf("/")+1);

            /**
             * OKHTTP2
             */
//            RequestBody requestBody = new MultipartBuilder()
//                    .type(MultipartBuilder.FORM)
//                    .addFormDataPart("member_id", memberId)
//                    .addFormDataPart("file", "profile.png", RequestBody.create(MEDIA_TYPE_PNG, sourceFile))
//                    .build();

            /**
             * OKHTTP3
             */
            RequestBody requestBody = new MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file_name", filename, RequestBody.create(MEDIA_TYPE_PNG, sourceFile))
                    .addFormDataPart("X", filename, RequestBody.create(MEDIA_TYPE_PNG, sourceFile))
                    .build();

            Request request = new Request.Builder()
                    .url(URL_UPLOAD_IMAGE)
                    .addHeader("auth-token", "eyJ0eXAiOiJqd3QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOjIsImFwcHR5cGUiOiJNT0JJTEUiLCJlbXBsb3llZV9pZCI6IjIiLCJhY2NvdW50Tm8iOiI3NzY5MjE3IiwiZGV2aWNldWlkIjoiYTUwZDdhNWQ1MWM2MWM4NCIsImV4cGlyeSI6MTQ3NjQzODM5OH0.yfjIaLOdnXjXXgJfcR762GUylnszts4xBCh6T-nvJRw")
                    .post(requestBody)
                    .build();

            OkHttpClient client = new OkHttpClient();
            client.connectTimeoutMillis();

            Response response = client.newCall(request).execute();
            String res = response.body().string();
            Log.e("TAG", "Error: " + res);

            Log.d("TEST IMAGE OBJECT ", String.valueOf(new JSONObject(res)));
            Log.d("FFF ", "fFF");
            return new JSONObject(res);

        } catch (UnknownHostException | UnsupportedEncodingException e) {
            Log.e("TAG", "Error: " + e.getLocalizedMessage());
        } catch (Exception e) {
            Log.e("TAG", "Other Error: " + e.getLocalizedMessage());
        }
        return null;
    }
}
