package com.SMIT.WallPostSoftware.Extras;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.SMIT.WallPostSoftware.Model_Class.CompanyList;
import com.SMIT.WallPostSoftware.Model_Class.Employee;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by alex on 08/09/16.
 */
public class Preference {

    Context context;
    CompanyList company;

    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    ArrayList<CompanyList> companyLists = new ArrayList<>();

    private Preference() {}

    public Preference(Context context,String preferenceType){
        this.context = context;

        switch (preferenceType){
            case "login" :{
                this.sharedPreferences= context.getSharedPreferences("login",0);
                this.editor = context.getSharedPreferences("login",0).edit();
            }
        }

    }

    public boolean clearPreference (){
        return this.editor.clear().commit();

    }

    public void setLogged(boolean value){
        editor.putBoolean("remember",value);
        editor.apply();
    }

    public boolean getLogged(){
     return sharedPreferences.getBoolean("remember",false);
    }

    public void setCompany(CompanyList company){

        editor.putString("Company_name", company.companyName);
        Employee emp = company.employee;
        editor.putString("Employee_id", emp.getEmpId());
        editor.putString("Employee_name", emp.getEmpName());
        editor.putString("Employee_image", emp.getEmpProfilePic());
        editor.putString("Employee_email", emp.getEmpEmail());
        editor.putString("Employee_designation", emp.getEmpDesignation());
        editor.putString("Employee_line_manager", emp.getEmpLineManager());
        editor.apply();
    }

    public  CompanyList getCompany(){

        CompanyList comapny = new CompanyList();
        comapny.employee = new Employee();
        comapny.companyName = sharedPreferences.getString("Company_name","");
        comapny.employee.empId = sharedPreferences.getString("Employee_id","");
        comapny.employee.empName= sharedPreferences.getString("Employee_name","");
        comapny.employee.empProfilePic = sharedPreferences.getString("Employee_image","");
        comapny.employee.empEmail = sharedPreferences.getString("Employee_email","");
        comapny.employee.empDesignation = sharedPreferences.getString("Employee_designation","");
        comapny.employee.empLineManager = sharedPreferences.getString("Employee_line_manager","");
        return comapny;
    }


    public void setCompanyList(String companyList){

        editor.putString("json_company_array", companyList);
        setMultipleCompanies();
        editor.apply();
    }

    public void setMultipleCompanies(){
        editor.putBoolean("more_companies",true);
    }

    public boolean getMultipleCompanies(){
        if (sharedPreferences.getBoolean("more_companies",false))
        {
            Log.d("Test","test");
        }
        return sharedPreferences.getBoolean("more_companies",false);

    }

    public ArrayList<CompanyList> getCompanyList(){
         ArrayList<CompanyList> cmpnyList = new ArrayList<>();
        String companyArrObj = sharedPreferences.getString("json_company_array",null);

        try {
            JSONArray jCmpnyArry = new JSONArray(companyArrObj);
            Log.d("ttt ", jCmpnyArry.toString());
            for(int i =0 ;i<jCmpnyArry.length(); i++){
                CompanyList companyList = new CompanyList();
                JSONObject jCmpnyCell = jCmpnyArry.getJSONObject(i);
                companyList.setCompanyId(jCmpnyCell.getString("company_id"));
                companyList.setCompanyName(jCmpnyCell.getString("company_name"));
                companyList.setCompanyLogo(jCmpnyCell.getString("company_logo"));
                companyList.setCompanyShortName(jCmpnyCell.getString("short_name"));

                Employee employee = new Employee();
                JSONObject jEmpCell = jCmpnyCell.getJSONObject("employee");
                employee.setEmpId(jEmpCell.getString("employee_id"));
                employee.setEmpName(jEmpCell.getString("name"));
                employee.setEmpEmail(jEmpCell.getString("email"));
                employee.setEmpProfilePic(jEmpCell.getString("profile_image"));
                employee.setEmpDesignation(jEmpCell.getString("designation"));
                employee.setEmpLineManager(jEmpCell.getString("line_manager"));

                companyList.setEmployee(employee);

                cmpnyList.add(companyList);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return cmpnyList;
    }

    public void setAuthToken(String token){

        editor.putString("auth_token", token);
        editor.apply();
    }

    public String getAuthToken(){

        return sharedPreferences.getString("auth_token","");
    }





    public String getGCM() {
        return sharedPreferences.getString("gcm_token","");
    }

    public void setGCM(String GCMToken) {
        editor.putString("gcm_token", GCMToken);
        editor.apply();
    }
//
//    public String getEmpId() {
//        return empId;
//    }
//
//    public void setEmpId(String empId) {
//        this.empId = empId;
//        editor.putString("Employee_id",this.empId);
//    }
//
//    public String getAuthToken() {
//        return sharedPreferences.getString("auth_token",null);
//    }
//
//    public void setAuthToken(String authToken) {
//        this.authToken = authToken;
//        editor.putString("auth_token",this.authToken);
//        editor.commit();
//    }
//
//    public String getCompany_id() {
//        return company_id;
//    }
//
//    public void setCompany_id(String company_id) {
//        this.company_id = company_id;
//    }
//
//    public String getCompanyName() {
//        return companyName;
//    }
//
//    public void setCompanyName(String companyName) {
//        this.companyName = companyName;
//    }
//
//    public String getCompanyShrtName() {
//        return companyShrtName;
//    }
//
//    public void setCompanyShrtName(String companyShrtName) {
//        this.companyShrtName = companyShrtName;
//    }
//
//    public ArrayList<CompanyList> getCompanyLists() {
//        return companyLists;
//    }
//
//    public void setCompanyLists(ArrayList<CompanyList> companyLists) {
//        this.companyLists = companyLists;
//    }
//
//    public Boolean getRemember() {
//        return remember;
//    }
//
//    public void setRemember(Boolean remember) {
//        this.remember = remember;
//        editor.putBoolean("remember",this.remember);
//        editor.commit();
//    }
}
