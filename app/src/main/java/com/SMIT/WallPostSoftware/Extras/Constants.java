package com.SMIT.WallPostSoftware.Extras;

import android.os.Environment;

/**
 * Created by alex on 20/07/16.
 */


public class Constants {

    public final static String SUCCESS="Success";
    public final static String ERROR="Error";

    //public final static String BASE_URL="http://api.wallpostsoftware.com/test/public/api/v1";
    public final static String BASE_URL="http://api3.wallpostsoftware.com/api/v1";
    public final static String SUB_MODULE ="/hr";

    public static String LOGIN_URL =BASE_URL + "/auth/login"; // Login url
    public static String LOGOUT_URL =BASE_URL+"/device/logout";

    public static String PROFILE_IMAGE_URL ="https://www.wallpostsoftware.com/wp/upload/";

    public static String CREATE_LEAVE_REQ_URL_SETTINGS = BASE_URL+ SUB_MODULE +"/settings/fieldsOptions/"; //Settings lv req

    public static String AIRPORT_DETAILS_URL = BASE_URL+SUB_MODULE+"/airports/";

    public static String LEAVE_ALERT_DIALOG_URL = BASE_URL+ SUB_MODULE +"/settings/paymentInfo/"; //View Payment and Eligibility info url

    public static String CREATE_LEAVE_URL =  BASE_URL+ SUB_MODULE +"/leave/apply/"; //Create leave

    public static String LEAVE_LIST_URL =  BASE_URL+ SUB_MODULE +"/leave/list/";
    public static String LEAVE_CANCEL_URL =  BASE_URL+ SUB_MODULE +"/leave/cancel/";

    public static String LEAVE_APPRV_LIST_URL = BASE_URL+ SUB_MODULE + "/leave/approvals";

    public static String LEAVE_ACTION_URL = BASE_URL + SUB_MODULE + "/leave/action";

    public static String LEAVE_DETAIL_URL = BASE_URL + SUB_MODULE + "/leave/";

    public static String FILE_UPLOAD_URL = BASE_URL+ "/fileupload/temp";

    public final static String LOCAL_IMAGE_BASE_PATH = Environment.getExternalStorageDirectory().getPath() + "/Android/data/com.SMIT.WallPostSoftware/images/";



    private Constants() {}



    public static class LeaveStatus {
        public final static String PENDING = "PENDING";
        public final static String APPROVED = "APPROVED";
        public final static String REJECTED = "REJECTED";
        public final static String CANCELLED = "CANCELLED";

    }

    public static class EmployementPortal {

        public final static String LEAVE = "leave";
        public final static String LEAVE_APPROVAL = "LEAVE_APPROVAL";


    }

    public static class Actions {

        public final static String CREATE = "create";
        public final static String UPDATE = "update";
        public final static String APPROVE = "approve";
        public final static String REJECT = "reject";
        public final static String CANCEL = "cancel";


    }

    public static class IntentKey {

        public static class EventNotification {

            public final static String LEAVE_REQUEST = "LEAVE_REQUEST_NOTIFICATION";
            public final static String LEAVE_APPROVAL = "LEAVE_APPROVAL_NOTIFICATION";

        }

        public static  class Key{

            public final static String FRAGMENT_ID = "FRAGMENT_ID";
            public final static String RELOAD = "RELOAD";

        }
    }










}