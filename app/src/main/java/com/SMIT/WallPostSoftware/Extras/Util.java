package com.SMIT.WallPostSoftware.Extras;

import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by alex on 22/08/16.
 */
public class Util {

    String mYear, mMonth, mDate, mHour, mMinutes, mSeconds;
    String unixTime;

    //Conversion of Time to Unix
    public long Unix(int year, int month, int date, int hour, int minutes) {


        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, date);
        c.set(Calendar.HOUR, hour);
        c.set(Calendar.MINUTE, minutes);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        Date date1 = Calendar.getInstance().getTime();
        Log.v("timeStamp", "date=" + date);
        Log.v("timeStamp", "timeStamp=" + date1.getTime() / 1000L);
        return (int) (c.getTimeInMillis() / 1000L);

    }

    public static String getDateStringFromDatePicker (int year, int monthOfYear, int dayOfMonth){
        final Calendar myCalendar = Calendar.getInstance();
        myCalendar.set(Calendar.YEAR, year);
        myCalendar.set(Calendar.MONTH, monthOfYear);
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        String myFormat = "dd-MM-yyyy"; // your format
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.getDefault());

        return sdf.format(myCalendar.getTime());
    }

    public static void getCurrentTimeStamp() {
          /*  Calendar c = Calendar.getInstance();
            System.out.println("Current time =&gt; "+c.getTime());*/

           /* SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String formattedDate = df.format(c.getTime());*/
        try {
            // String timeStamp = String.valueOf(System.currentTimeMillis()/1000);
            String timeStamp = String.valueOf(System.currentTimeMillis() / 1000);
            Log.v("UNIX", "UNIX=" + timeStamp);
            Date date = Calendar.getInstance().getTime();
            Log.v("timeStamp", "date=" + date);
            Log.v("timeStamp", "timeStamp=" + date.getTime() / 1000L);
        } catch (Exception e) {

        }

    }

    public static int getDays(String startDate, String endDate){


        SimpleDateFormat myFormat = new SimpleDateFormat("dd-MM-yyyy");
        String inputString1 = startDate;
        String inputString2 = endDate;



        try {
            Date date1 = myFormat.parse(inputString1);
            Date date2 = myFormat.parse(inputString2);
            long diff = date2.getTime() - date1.getTime();
            System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));

            int days = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

            return days;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return 0;
    }



    public static long getTimeStampFromDate(Date date) {

        try {
            Log.v("timeStamp", "date=" + date);
            Log.v("timeStamp", "timeStamp=" + date.getTime() / 1000L);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return date.getTime() / 1000L;

    }
    public static Date getCurrentDate() {
        //date output format
        Calendar cal = Calendar.getInstance();
        return cal.getTime();
    }
    public static String getFormattedTime(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static String getFormattedDate(String _date) {
        String inputPattern = "dd.MM.yyyy";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(_date);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static Date getFormattedDateFromString(String _date) {
        String inputPattern = "dd.MM.yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);

        Date date = null;
        try {
            date = inputFormat.parse(_date);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }


    public static String convertTimeStampToFormattedDate(String timeStamp) {
        if(timeStamp==null)
            return "";
        long unixSeconds = Long.parseLong(timeStamp);
        Date date = new Date(unixSeconds * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("h:mm a MMM d, yyyy"); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        return formattedDate;

    }

    public static String convertTimeStampToDate(long timeStamp) {
        String dateString = Long.toString(timeStamp);
        if(dateString==null)
            return "";
        long unixSeconds = Long.parseLong(dateString);
        Date date = new Date(unixSeconds * 1000L); // *1000 is to convert seconds to milliseconds
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd"); // the format of your date
        sdf.setTimeZone(TimeZone.getDefault()); // give a timezone reference for formating (see comment at the bottom
        String formattedDate = sdf.format(date);
        return formattedDate;

    }
}