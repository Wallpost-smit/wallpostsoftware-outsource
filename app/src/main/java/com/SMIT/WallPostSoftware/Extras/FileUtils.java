package com.SMIT.WallPostSoftware.Extras;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.widget.ImageView;

import com.SMIT.WallPostSoftware.R;
import com.squareup.picasso.Picasso;

import java.net.URISyntaxException;

/**
 * Created by alex on 18/08/16.
 */
public class FileUtils {
    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    public static  void loadImage(String imgUrl, Context context, ImageView _imgView){

        Picasso.with(context)
                .load(imgUrl.replace(" ", "%20"))
                .resize(100,100)
                .centerCrop()
                .placeholder(context.getResources().getDrawable( R.drawable.user_profile_image))// optional
                .error(context.getResources().getDrawable( R.drawable.user_profile_image))         // optional
                .into(_imgView);

    }




}
