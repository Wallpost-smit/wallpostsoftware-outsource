/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.SMIT.WallPostSoftware.GCM;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import com.SMIT.WallPostSoftware.Extras.Preference;
import com.SMIT.WallPostSoftware.GCM.NotificationHandlers.IntentHandlers;
import com.SMIT.WallPostSoftware.R;
import com.SMIT.WallPostSoftware.WP_EMP_LoginActivity;
import com.google.android.gms.gcm.GcmListenerService;

import org.greenrobot.eventbus.EventBus;

import static android.R.attr.action;
import static android.R.attr.id;

public class MyGcmListenerService extends GcmListenerService {

    private static final String TAG = "MyGcmListenerService";
    private EventBus eventBus = EventBus.getDefault();
    Preference pref;
    /**
     * Called when message is received.
     *
     * @param from SenderID of the sender.
     * @param data Data bundle containing message data as key/value pairs.
     *             For Set of keys use data.keySet().
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("message");
        Log.d(TAG, "From: " + from);
        Log.d(TAG, "Message: " + message);

        if (from.startsWith("/topics/")) {
            // message received from some topic.
        } else {
            // normal downstream message.
        }

        // [START_EXCLUDE]
        /**
         * Production applications would usually process the message here.
         * Eg: - Syncing with server.
         *     - Store message in local database.
         *     - Update UI.
         */



        /**
         * In some cases it may be useful to show a notification indicating to the user
         * that a message was received.
         */

        pref = new Preference(getApplicationContext(),"login");

        if (pref.getLogged() == true) {

            Log.d("NOTIFICATION ",data.toString());
          NotificationPayLoadHandler notificationPayLoadHandler = new NotificationPayLoadHandler(getApplicationContext(),data);
            Bundle notifyBundle = notificationPayLoadHandler.getNotifyDetail();
            notification(notifyBundle);

        }

       // sendNotification(message);
        // [END_EXCLUDE]
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received GCM message.
     *
     * @param message GCM message received.
     */

    private void notification(Bundle notification) {

        Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                .setContentTitle(notification.getString("title"))
                .setContentText(notification.getString("body"))
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(notification.getString("title")))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setAutoCancel(true).setLargeIcon(icon);

        Intent resultIntent = null;

        // Creates an explicit intent for an Activity in your app


        if (pref.getLogged() == true) {
                IntentHandlers handlers = new IntentHandlers(getApplicationContext());
                resultIntent = handlers.getIntent(notification.getString("route"),notification.getString("objectID"),notification.getString("action"));
                eventBus.post(notification);

        } else {
            resultIntent = new Intent(this,WP_EMP_LoginActivity.class);
        }


        /*
         The stack builder object will contain an artificial back stack for the started Activity.
         This ensures that navigating backward from the Activity leads out of  your application to the Home screen.
         */
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(WP_EMP_LoginActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        (int) (Math.random() * 1000),
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        notificationBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify((int) System.currentTimeMillis() /* ID of notification */, notificationBuilder.build());
    }

}
