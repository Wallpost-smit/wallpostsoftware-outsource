package com.SMIT.WallPostSoftware.GCM;

import android.content.Context;
import android.os.Bundle;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by smit on 10/6/16.
 */

public class NotificationPayLoadHandler {

    Context mContext;
    Bundle notifyBundle;

    NotificationPayLoadHandler(Context mContext, Bundle bundle) {
        this.mContext = mContext;
        this.notifyBundle = bundle;
    }

    public Bundle getNotifyDetail() {
        String message = notifyBundle.getString("custom");
        JSONObject jsonObject =null;
        JSONObject jsonNotifcatn = null;
        JSONObject jsonData=null;


        try{
            jsonObject = new JSONObject(message);
            JSONObject payload = jsonObject.getJSONObject("payload");
            jsonNotifcatn = payload.getJSONObject("notification");
            jsonData = payload.getJSONObject("data");
        }catch (JSONException e){
            e.printStackTrace();
        }

        String body = null, title = null;

        try {
            JSONObject jsonMessage = new JSONObject(String.valueOf(jsonNotifcatn));
            body = jsonMessage.getString("body");
            title = jsonMessage.getString("title");

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

//        Log.d("notification..........", "text--------------- =" + notifyBundle);
//        //String custom_message = notifyBundle.getString("custom");
        String referenceID = null, route = null, referenceIdName = null, action = null, objectID = null;

        try {
            JSONObject data = new JSONObject(String.valueOf(jsonData));
            route = jsonData.getString("route");
            action = jsonData.getString("action");
            objectID = jsonData.getString("objectID");
            referenceIdName = route.substring(route.lastIndexOf("/") + 1);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Bundle bundle = new Bundle();
        bundle.putString("title", title);
        bundle.putString("body", body);
        bundle.putString("route", referenceIdName);
        bundle.putString("action", action);
        bundle.putString("objectID", objectID);

        return bundle;
    }
}
