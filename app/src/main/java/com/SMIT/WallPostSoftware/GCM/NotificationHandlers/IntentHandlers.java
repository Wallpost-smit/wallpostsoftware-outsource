package com.SMIT.WallPostSoftware.GCM.NotificationHandlers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.HomeScreenActivity;

/**
 * Created by smit on 10/6/16.
 */

public class IntentHandlers {

    Context context;
    Intent resultIntent = null;

    public IntentHandlers(Context context) {
        this.context = context;

    }

    // GETTING INTENT BASED ON ROLES

//    public Intent getIntentByRoles(){
//
//
//        String role = PrefUtils.getRole(this);
//        switch (role) {
//            case Constants.Role.TENANT:
//                resultIntent = new Intent(context, TenantHomeActivity.class);
//        }
//    }

        public Intent getIntent(String route, String objectID,String action){

        switch (route) {
            case Constants.EmployementPortal.LEAVE:
                Log.d("TEST BEFORE","INTENT");
                Log.d("OBJECT ID ", objectID);
                resultIntent = new Intent(context, HomeScreenActivity.class);
                resultIntent.putExtra("LEAVE",objectID);

                Log.d("TEST AFTER ","INTENT");

                break;

            default:
                resultIntent = new Intent(context, HomeScreenActivity.class);
        }

        if (!action.isEmpty()){
            Log.d("ACTION ", "ENTERED");
            resultIntent.putExtra("ACTION",action);
        }



          return  resultIntent;
    }


}
