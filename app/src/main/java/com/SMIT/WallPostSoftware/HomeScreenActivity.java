package com.SMIT.WallPostSoftware;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.FileUtils;
import com.SMIT.WallPostSoftware.Extras.Preference;
import com.SMIT.WallPostSoftware.Extras.RoundedImageView;
import com.SMIT.WallPostSoftware.Extras.WebManager;
import com.SMIT.WallPostSoftware.Model_Class.CompanyList;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.MyPortalFragment;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.API.LeaveSettingsApi;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.LeaveRequestFragment;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.RequestApprovalFragment;
import com.SMIT.WallPostSoftware.common.FilterDialog;
import com.SMIT.WallPostSoftware.common.NotificationResponseParsing;
import com.google.android.gms.analytics.Tracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class HomeScreenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public TextView _notification_count;
    public ImageButton _filterButton;

    public ArrayList<LeaveRequestList> currentLvList = new ArrayList<>();
    public LeaveRequestFragment leaveRequestFragmentObj;
    public RequestApprovalFragment requestApprovalFragment;

    public LeaveSettingsApi leaveSettingsApi = new LeaveSettingsApi();
    public FilterDialog filterDialog = new FilterDialog();
    Context context;

    public RecyclerView _recylceView;

    boolean doubleBackToExitPressedOnce = false;
    View header;

    ProgressDialog progressDialog;

    public String imagrUrl ;
    public String req_url;
    public String jStatus;
    public String fragmentToLoad;

    public String lvId, lvAction;

    private Tracker mTracker;
    public String selectedFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen_activity);

        //GOOGLE ANALYTICS
//        AppController application = (AppController) getApplication();
//        mTracker = application.getDefaultTracker();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        header = navigationView.getHeaderView(0);
        setTitle("Leave Request");

        context = HomeScreenActivity.this;

        loadSettings(context);

        Preference pref= new Preference(context,"login");
        CompanyList company= pref.getCompany();


        Intent intent = getIntent();
        lvId = intent.getStringExtra("LEAVE");




        if(lvId !=null){
            lvAction = intent.getStringExtra("ACTION");
            NotificationResponseParsing notificationResponseParsing = new NotificationResponseParsing(getApplicationContext());
            notificationResponseParsing.getLeaveDetail(lvId, lvAction);

            lvId =null;
        }



        selectedFragment = "LEAVE REQUEST FRAG";
        leaveRequestFragmentObj = new LeaveRequestFragment();
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, leaveRequestFragmentObj);
        fragmentTransaction.commit();


        fragmentToLoad = intent.getStringExtra(Constants.IntentKey.Key.FRAGMENT_ID);

        // Krishna <code> Navigating to Approval Fragment Leave Approval is done</code>
        if (fragmentToLoad != null){
            Fragment fragment;
            switch (fragmentToLoad){
                case "0" : {
                    setTitle("Approvals");
                    selectedFragment = "REQUEST APPROVAL FRAG";
                    requestApprovalFragment = new RequestApprovalFragment();
                    fragment = requestApprovalFragment;
                    loadFragment(fragment);
                    break;
                }
                default:{
                    setTitle("Approvals");
                    selectedFragment = "REQUEST APPROVAL FRAG";
                    requestApprovalFragment = new RequestApprovalFragment();
                    fragment = requestApprovalFragment;
                    loadFragment(fragment);
                }
            }
        }


        _filterButton = (ImageButton) toolbar.findViewById(R.id.filter_lv);
        _filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


//if you added fragment via layout xml
                //GOOGLE ANAYTICS
//                mTracker.send(new HitBuilders.EventBuilder()
//                        .setCategory("Action")
//                        .setAction("Share")
//                        .build());

                if(selectedFragment.equals("LEAVE REQUEST FRAG")){
                    filterDialog.loadFilterDialog(context, leaveRequestFragmentObj, null);
                }else {
                    filterDialog.loadFilterDialog(context, null, requestApprovalFragment);
                }


            }
        });

        //Log.d("LENTTT ", empImage);
        if(company.employee.empProfilePic.length() > 1){
            loadImage(company.employee.empProfilePic);
        }

        TextView _empNametv = (TextView)header.findViewById(R.id.employee_name);
        _empNametv.setText(company.employee.empName);

        // Either Company name or Employee Email or Position will be shown
        TextView _empEmailtv = (TextView)header.findViewById(R.id.employee_email);
        _empEmailtv.setText(company.companyName);


        // Either Company name or Employee Email or Position will be shown
        TextView _empEmailPosition = (TextView)header.findViewById(R.id.employee_position);
        _empEmailPosition.setText(company.employee.getEmpDesignation());





        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();


        Menu menu = navigationView.getMenu();

        _notification_count = (TextView) MenuItemCompat.getActionView(navigationView.getMenu().
                findItem(R.id.leave_request));


        // Add items to the second group, and set to visible
        menu.setGroupCheckable(R.id.employee_menu, true, true);
        menu.setGroupVisible(R.id.employee_menu, true);
//        menu.setGroupCheckable(R.id.admin_menu, true, true);
//        menu.setGroupVisible(R.id.admin_menu, true);

        initializeCountDrawer();


    }



    public void loadSettings(Context context){

        leaveSettingsApi = new LeaveSettingsApi();
        leaveSettingsApi.loadLvSettings(context);


    }


    //Notification Counter
    private void initializeCountDrawer(){
        //Gravity property aligns the text
        _notification_count.setGravity(Gravity.CENTER_VERTICAL);
        _notification_count.setTypeface(null, Typeface.BOLD);
        _notification_count.setTextColor(getResources().getColor(R.color.colorBlue));
       // _notification_count.setText("99+");

    }

//    @Override
//    public void onBackPressed() {
//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        if (drawer.isDrawerOpen(GravityCompat.START)) {
//            drawer.closeDrawer(GravityCompat.START);
//        } else {
//            super.onBackPressed();
//        }
//    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Fragment fragment;

        switch (id){
//            case R.id.my_portal:
//                fragment = new MyPortalFragment();
//                break;

            case R.id.leave_request:
                setTitle("Leave Request");
                selectedFragment = "LEAVE REQUEST FRAG";
                leaveRequestFragmentObj = new LeaveRequestFragment();
                fragment = leaveRequestFragmentObj;

                break;

            case R.id.request_apprvl:
                setTitle("Approvals");
                selectedFragment = "REQUEST APPROVAL FRAG";
                requestApprovalFragment = new RequestApprovalFragment();
                fragment = requestApprovalFragment;
                break;
//
//            case R.id.resignation:
//                fragment = new ResignationFragment();
//                break;
//
//            case R.id.settings:
//                fragment = new SettingsFragment();
//                break;

            default:
                fragment = new MyPortalFragment();
                break;
        }


        //        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //Krishna code
    public void loadFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit();

    }



    public void loadImage(String imgUrl){
        RoundedImageView _imgView = (RoundedImageView)header.findViewById(R.id.imageView);
        FileUtils.loadImage(imgUrl,context,_imgView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_screen_menu, menu);
//        Preference preference = new Preference(getApplicationContext(),"login");
//       if (preference.getMultipleCompanies()){
//           menu.getItem(0).setVisible(true);
//       }else{
//           menu.getItem(0).setVisible(false);
//       }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        Log.d("MENU ","CLICKED");

        int id = item.getItemId();

        Log.d("ID NAME ", item.getTitle().toString());

//        if(id == R.id.about_us){
//            Log.d("ABOUT ", "US");
//            return true;
//        }
//
//        if(id == R.id.privicy_policy){
//            Log.d("PRIVICY ","POLICY");
//            return true;
//        }

        if(id == R.id.change_cmpny){
            Intent intent = new Intent(HomeScreenActivity.this, CompanyListActivity.class);
            startActivity(intent);
            return true;
        }

        if(id == R.id.log_out){

            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(HomeScreenActivity.this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Do you want to log out ?");
            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface positiveDialog, int which) {
                            positiveDialog.dismiss();
                            logOut();
                        }
                    });

            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface neagtivDialog, int i) {
                            neagtivDialog.dismiss();
                        }
                    });
            alertDialog.show();
            return true;

        }

        return super.onOptionsItemSelected(item);
    }

    public void logOut(){

        progressDialog = new ProgressDialog(HomeScreenActivity.this);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        req_url = Constants.LOGOUT_URL;
        //context = getApplicationContext();
        WebManager webManager = new WebManager(context);
        webManager.LogOut(req_url, new WebManager.VolleyCallback() {
            @Override
            public void onSuccess(String result) {
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    jStatus = jsonObject.getString("status");

                    if(jStatus.equals("success")){
                        SharedPreferences pref = getSharedPreferences("login", Context.MODE_PRIVATE);
                        pref.edit().clear().apply();
                        Intent intent = new Intent(HomeScreenActivity.this, WP_EMP_LoginActivity.class);
                        startActivity(intent);
                    }else {

                        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(HomeScreenActivity.this).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage(jStatus);
                        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String result) {

            }
        });




    }

    @Override
    public void onBackPressed() {


        if (doubleBackToExitPressedOnce) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }


}
