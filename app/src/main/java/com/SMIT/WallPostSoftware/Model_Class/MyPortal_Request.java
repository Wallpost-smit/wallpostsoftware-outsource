package com.SMIT.WallPostSoftware.Model_Class;

/**
 * Created by alex on 01/08/16.
 */
public class MyPortal_Request {

    private String leave_type, task;

    public MyPortal_Request(String leave_type, String task) {
        this.leave_type = leave_type;
        this.task = task;
    }

    public String getLeave_type() {
        return leave_type;
    }

    public void setLeave_type(String leave_type) {
        this.leave_type = leave_type;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }
}
