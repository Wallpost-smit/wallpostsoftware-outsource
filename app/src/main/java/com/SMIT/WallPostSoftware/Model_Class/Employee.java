package com.SMIT.WallPostSoftware.Model_Class;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by SMIT015 on 7/21/2016.
 */
public class Employee implements Serializable{

    public String empName;
    public String empId;
    public String empEmail;
    public String empProfilePic;
    public String empDesignation;
    public String empGrade;
    public String empStaffNo;
    public String empLineManager;
    public String empJoiningDate;
    public String department;
    public String  position;

    public ArrayList<Employee> replacedEmpList;
    public ArrayList<HashMap<Integer,Employee>> selectdListFrmResponse;
    public boolean checked;


    public Employee() {
    }

    public Employee(String empName, String empDesignation) {
        this.empName = empName;
        this.empDesignation = empDesignation;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public String getEmpEmail() {
        return empEmail;
    }

    public void setEmpEmail(String empEmail) {
        this.empEmail = empEmail;
    }

    public String getEmpProfilePic() {
        return empProfilePic;
    }

    public void setEmpProfilePic(String empProfilePic) {
        this.empProfilePic = empProfilePic;
    }

    public String getEmpDesignation() {
        return empDesignation;
    }

    public void setEmpDesignation(String empDesignation) {
        this.empDesignation = empDesignation;
    }

    public String getEmpLineManager() {
        return empLineManager;
    }

    public void setEmpLineManager(String empLineManager) {
        this.empLineManager = empLineManager;
    }

    public String getEmpGrade() {
        return empGrade;
    }

    public void setEmpGrade(String empGrade) {
        this.empGrade = empGrade;
    }

    public String getEmpStaffNo() {
        return empStaffNo;
    }

    public void setEmpStaffNo(String empStaffNo) {
        this.empStaffNo = empStaffNo;
    }

    public String getEmpJoiningDate() {
        return empJoiningDate;
    }

    public void setEmpJoiningDate(String empJoiningDate) {
        this.empJoiningDate = empJoiningDate;
    }


    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public ArrayList<Employee> getReplacedEmpList() {
        return replacedEmpList;
    }

    public void setReplacedEmpList(ArrayList<Employee> replacedEmpList) {
        this.replacedEmpList = replacedEmpList;
    }

    public ArrayList<HashMap<Integer, Employee>> getSelectdListFrmResponse() {
        return selectdListFrmResponse;
    }

    public void setSelectdListFrmResponse(ArrayList<HashMap<Integer, Employee>> selectdListFrmResponse) {
        this.selectdListFrmResponse = selectdListFrmResponse;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
