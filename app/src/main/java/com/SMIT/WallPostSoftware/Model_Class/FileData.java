package com.SMIT.WallPostSoftware.Model_Class;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by alex on 24/10/16.
 */
public class FileData implements Serializable {

    public String fileName;

    public String refrnceId;

    public String attachmentData;

    public ArrayList<HashMap<Integer,FileData>> fileListFrmResponse;

    public CompanyList company = new CompanyList();

    public ArrayList<HashMap<Integer, FileData>> getFileListFrmResponse() {
        return fileListFrmResponse;
    }

    public void setFileListFrmResponse(ArrayList<HashMap<Integer, FileData>> fileListFrmResponse) {
        this.fileListFrmResponse = fileListFrmResponse;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getRefrnceId() {
        return refrnceId;
    }

    public void setRefrnceId(String refrnceId) {
        this.refrnceId = refrnceId;
    }

    public String getAttachmentData() {
        return attachmentData;
    }

    public void setAttachmentData(String attachmentData) {
        this.attachmentData = attachmentData;
    }
}
