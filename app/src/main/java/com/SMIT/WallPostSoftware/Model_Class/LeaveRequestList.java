package com.SMIT.WallPostSoftware.Model_Class;

import java.io.Serializable;

/**
 * Created by alex on 03/08/16.
 */
public class LeaveRequestList implements Serializable {

    private String leave_id;
    private String leave_type;



    private String leave_type_id;
    private String leave_status;
    private String leave_frm;
    private String leave_to;
    private String leave_days;
    private String leave_resn;
    private String leave_contact;
    private String leave_email;
    private String leave_paid;
    private String leave_unpaid;

    private String leave_comment;


    private String rejected_reason;
    private String cancelled_reason;



    private String approval_comments;
    private String leave_exit_permit;
    private String leave_depature_date;
    private String Leave_depature_time;
    private String leave_return_date;
    private String leave_return_time;

    private String leave_tickets;
    private String origin;
    private String destination;
    private String leave_entitled_for;
    private String air_class;
    private String way_type;
    private String number_of_adults;
    private String Number_of_childrens;

    private String handOver_status;
    private String clearence_status;

    public String replaceRequird;

    public Employee employee = new Employee();

    public FileData fileData = new FileData();

    public CompanyList company = new CompanyList();

    public String getLeave_id() {
        return leave_id;
    }

    public void setLeave_id(String leave_id) {
        this.leave_id = leave_id;
    }

    public String getLeave_frm() {
        return leave_frm;
    }

    public String getLeave_type_id() {
        return leave_type_id;
    }

    public void setLeave_type_id(String leave_type_id) {
        this.leave_type_id = leave_type_id;
    }

    public void setLeave_frm(String leave_frm) {
        this.leave_frm = leave_frm;
    }

    public String getLeave_to() {
        return leave_to;
    }

    public void setLeave_to(String leave_to) {
        this.leave_to = leave_to;
    }

    public String getLeave_days() {
        return leave_days;
    }

    public void setLeave_days(String leave_days) {
        this.leave_days = leave_days;
    }

    public String getLeave_resn() {
        return leave_resn;
    }

    public void setLeave_resn(String leave_resn) {
        this.leave_resn = leave_resn;
    }

    public String getLeave_paid() {
        return leave_paid;
    }

    public void setLeave_paid(String leave_paid) {
        this.leave_paid = leave_paid;
    }

    public String getLeave_unpaid() {
        return leave_unpaid;
    }

    public void setLeave_unpaid(String leave_unpaid) {
        this.leave_unpaid = leave_unpaid;
    }

    public String getLeave_type() {
        return leave_type;
    }

    public void setLeave_type(String leave_type) {
        this.leave_type = leave_type;
    }

    public String getLeave_status() {
        return leave_status;
    }

    public void setLeave_status(String leave_status) {
        this.leave_status = leave_status;
    }


    public void setLeave_contact(String leave_contact) {
        this.leave_contact = leave_contact;
    }
    public String getLeave_contact() {
        return leave_contact;
    }

    public String getLeave_email() {
        return leave_email;
    }

    public void setLeave_email(String leave_email) {
        this.leave_email = leave_email;
    }


    public String getLeave_exit_permit() {
        return leave_exit_permit;
    }

    public void setLeave_exit_permit(String leave_exit_permit) {
        this.leave_exit_permit = leave_exit_permit;
    }

    public String getLeave_depature_date() {
        return leave_depature_date;
    }

    public void setLeave_depature_date(String leave_depature_date) {
        this.leave_depature_date = leave_depature_date;
    }

    public String getLeave_depature_time() {
        return Leave_depature_time;
    }

    public void setLeave_depature_time(String leave_depature_time) {
        Leave_depature_time = leave_depature_time;
    }

    public String getLeave_return_date() {
        return leave_return_date;
    }

    public void setLeave_return_date(String leave_return_date) {
        this.leave_return_date = leave_return_date;
    }

    public String getLeave_return_time() {
        return leave_return_time;
    }

    public void setLeave_return_time(String leave_return_time) {
        this.leave_return_time = leave_return_time;
    }

    public String getLeave_tickets() {
        return leave_tickets;
    }

    public void setLeave_tickets(String leave_tickets) {
        this.leave_tickets = leave_tickets;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getLeave_entitled_for() {
        return leave_entitled_for;
    }

    public void setLeave_entitled_for(String leave_entitled_for) {
        this.leave_entitled_for = leave_entitled_for;
    }

    public String getAir_class() {
        return air_class;
    }

    public void setAir_class(String air_class) {
        this.air_class = air_class;
    }

    public String getWay_type() {
        return way_type;
    }

    public void setWay_type(String way_type) {
        this.way_type = way_type;
    }

    public String getNumber_of_adults() {
        return number_of_adults;
    }

    public void setNumber_of_adults(String number_of_adults) {
        this.number_of_adults = number_of_adults;
    }

    public String getNumber_of_childrens() {
        return Number_of_childrens;
    }

    public void setNumber_of_childrens(String number_of_childrens) {
        Number_of_childrens = number_of_childrens;
    }


    public String getLeave_comment() {
        return leave_comment;
    }

    public void setLeave_comment(String leave_comment) {
        this.leave_comment = leave_comment;
    }

    public String getRejected_reason() {
        return rejected_reason;
    }

    public void setRejected_reason(String rejected_reason) {
        this.rejected_reason = rejected_reason;
    }
    public String getCancelled_reason() {
        return cancelled_reason;
    }

    public void setCancelled_reason(String cancelled_reason) {
        this.cancelled_reason = cancelled_reason;
    }
    public String getApproval_comments() {
        return approval_comments;
    }

    public void setApproval_comments(String approval_comments) {
        this.approval_comments = approval_comments;
    }

    public String getHandOver_status() {
        return handOver_status;
    }

    public void setHandOver_status(String handOver_status) {
        this.handOver_status = handOver_status;
    }

    public String getClearence_status() {
        return clearence_status;
    }

    public void setClearence_status(String clearence_status) {
        this.clearence_status = clearence_status;
    }

    public String getReplaceRequird() {
        return replaceRequird;
    }

    public void setReplaceRequird(String replaceRequird) {
        this.replaceRequird = replaceRequird;
    }


}
