package com.SMIT.WallPostSoftware.Model_Class;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by alex on 26/07/16.
 */
public class User {

    private static User instance;

    private String authToken;

    private String emplId;

    private String empName;

    private String userType;

    private String profileImageUrl;

    private String emplEmail;

    private User() {}

    public static User Instance()
    {
        //if no instance is initialized yet then create new instance
        //else return stored instance
        if (instance == null)
        {
            instance = new User();
        }
        return instance;
    }


    public void setEmplId(String emplId) {
        this.emplId = emplId;
    }

    public User(JSONObject jsonObject) {

        try {
            setAuthToken(jsonObject.getString("auth_token"));
            JSONObject jObj = jsonObject.getJSONObject("employee");
            setEmplId(jObj.getString("employee_id"));
            setEmpName(jObj.getString("name"));
            setProfileImageUrl(jObj.getString("profile_image_url"));
            setEmplEmail(jObj.getString("email"));


        }catch (JSONException e){
            e.printStackTrace();
        }
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }



    public String getUserType() {
        return userType;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }


    public String getAuthToken() {
        return authToken;
    }

    public String getEmplId() {
        return emplId;
    }

    public String getEmplEmail() {
        return emplEmail;
    }

    public void setEmplEmail(String emplEmail) {
        this.emplEmail = emplEmail;
    }
}
