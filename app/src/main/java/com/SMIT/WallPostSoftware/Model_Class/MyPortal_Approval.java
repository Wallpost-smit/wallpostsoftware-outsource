package com.SMIT.WallPostSoftware.Model_Class;

/**
 * Created by alex on 01/08/16.
 */
public class MyPortal_Approval {

    private String name, leave_type;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLeave_type() {
        return leave_type;
    }

    public void setLeave_type(String leave_type) {
        this.leave_type = leave_type;
    }

    public MyPortal_Approval(String name, String leave_type) {
        this.name = name;
        this.leave_type = leave_type;
    }

}
