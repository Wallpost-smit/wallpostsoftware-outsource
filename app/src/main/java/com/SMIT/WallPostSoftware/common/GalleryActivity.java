package com.SMIT.WallPostSoftware.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.SMIT.WallPostSoftware.Extras.FILE_UPLOADING_PARSER.FileDownloader;
import com.SMIT.WallPostSoftware.Model_Class.FileData;
import com.SMIT.WallPostSoftware.R;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class GalleryActivity extends AppCompatActivity {

    public ArrayList<FileData> fileFrmRespnse = new ArrayList<>();
    public ArrayList<Bitmap> bitmapList = new ArrayList<>();
    Bitmap bitmapImage;

    int imagePos  = 0;
    public ImageButton _galleryDissmisButtn;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gallery_activity);


        Intent intent = getIntent();
        fileFrmRespnse = (ArrayList<FileData>) intent.getSerializableExtra("IMAGES_ARRAY");
        imagePos = intent.getIntExtra("SELECTED_IMAGE_POS", 0);

        _galleryDissmisButtn = (ImageButton) findViewById(R.id.gallery_dismiss_buttn);

        _galleryDissmisButtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        // Krishna code commented
        //new backgroundLoading().execute(fileFrmRespnse);

//        for (int i =0;i<fileFrmRespnse.size(); i++){
//            FileData fileDataObj = fileFrmRespnse.get(i);
//            try {
//                URL url = new URL(fileDataObj.getAttachmentData());
//                bitmapImage = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//            } catch(IOException e) {
//                System.out.println(e);
//            }
//            bitmapList.add(bitmapImage);
//        }
//



    }
    // Krishna code
    @Override
    protected void onResume() {
        super.onResume();
        ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
        ImagePagerAdapter adapter = new ImagePagerAdapter();
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(imagePos);
        viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
    }

    private class ImagePagerAdapter extends PagerAdapter{

        @Override
        public int getCount() {
            // Krishna code
            return fileFrmRespnse.size();

        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((ImageView) object);

        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Context context = GalleryActivity.this;
            ImageView imageView = new ImageView(context);

            imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imageView.setPadding(10,0,10,0);

            // Krishna code
            FileData fileDataObj = fileFrmRespnse.get(position);
            FileDownloader.setImage( fileDataObj.getAttachmentData(),imageView,context);
            //imageView.setImageBitmap(Bitmap.createScaledBitmap(bitmapList.get(position), 1200,1200, false));

            ((ViewPager) container).addView(imageView, 0);

            return imageView;

        }



        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((ImageView) object);
        }
    }





    //BACKGROUND TASK
   private class backgroundLoading extends AsyncTask<ArrayList<FileData>, Integer, ArrayList<Bitmap>>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(GalleryActivity.this);
            progressDialog.setTitle("Processing...");
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected ArrayList<Bitmap> doInBackground(ArrayList<FileData>... arrayLists) {
            for (int i =0;i<fileFrmRespnse.size(); i++){
                FileData fileDataObj = fileFrmRespnse.get(i);
                try {
                    URL url = new URL(fileDataObj.getAttachmentData());
                   // bitmapImage = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                } catch(IOException e) {
                    System.out.println(e);
                }
                bitmapList.add(bitmapImage);
            }
            return bitmapList;
        }

        @Override
        protected void onPostExecute(ArrayList<Bitmap> bitmaps) {
            super.onPostExecute(bitmaps);

            progressDialog.dismiss();

            ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);
            ImagePagerAdapter adapter = new ImagePagerAdapter();
            viewPager.setAdapter(adapter);
            viewPager.setPageTransformer(true, new ZoomOutPageTransformer());
        }
    }

    public class ZoomOutPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.85f;
        private static final float MIN_ALPHA = 0.5f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();
            int pageHeight = view.getHeight();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 1) { // [-1,1]
                // Modify the default slide transition to shrink the page as well
                float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
                float vertMargin = pageHeight * (1 - scaleFactor) / 2;
                float horzMargin = pageWidth * (1 - scaleFactor) / 2;
                if (position < 0) {
                    view.setTranslationX(horzMargin - vertMargin / 2);
                } else {
                    view.setTranslationX(-horzMargin + vertMargin / 2);
                }

                // Scale the page down (between MIN_SCALE and 1)
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

                // Fade the page relative to its size.
                view.setAlpha(MIN_ALPHA +
                        (scaleFactor - MIN_SCALE) /
                                (1 - MIN_SCALE) * (1 - MIN_ALPHA));

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }




}

