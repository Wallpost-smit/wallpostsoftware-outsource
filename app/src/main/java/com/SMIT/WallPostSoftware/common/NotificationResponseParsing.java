package com.SMIT.WallPostSoftware.common;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.WebManager;
import com.SMIT.WallPostSoftware.HomeScreenActivity;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.LeaveReqDetActivity;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.LeaveApprvDetActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by alex on 07/10/16.
 */
public class NotificationResponseParsing {

    Context context;
    String reqUrl;

    public ArrayList<LeaveRequestList> apprvList  = new ArrayList<>();
    public ArrayList<Employee> empHandoverList = new ArrayList<>();


    public NotificationResponseParsing(Context context) {
        this.context = context;
    }

    public void getLeaveDetail(String leaveId, final String action){

        reqUrl = Constants.LEAVE_DETAIL_URL+leaveId+"/find";
        Log.d("REQ URL ", reqUrl);
        WebManager webManager = new WebManager(context);
        webManager.leaveDetail(reqUrl, new WebManager.VolleyCallback() {
            @Override
            public void onSuccess(String result) {

                Log.d("RESPONSEEEE ", result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject jData = jsonObject.getJSONObject("data");
                    JSONArray jLvApprv = jData.getJSONArray("leaveRequest");

                    for (int i =0; i<jLvApprv.length(); i++){

                        JSONObject jsonLv = jLvApprv.getJSONObject(i);

                        LeaveDetail leaveDetail = new LeaveDetail();
                        apprvList.add(leaveDetail.getLeaveData(jsonLv));


                    }

                    JSONArray jEmpApprv = jData.getJSONArray("employees");
                    for (int i=0; i<jEmpApprv.length(); i++){

                        Log.d("rererer", "RRr");
                        JSONObject jsonEmp = jEmpApprv.getJSONObject(i);
                        LeaveDetail leaveDetail = new LeaveDetail();
                        empHandoverList.add(leaveDetail.getHandOverEmplys(jsonEmp));

                    }

                    Log.d("TEST ", "ALEX");

                    LeaveRequestList leaveRequestList = apprvList.get(0);

                    Intent intent = new Intent();
                    Bundle bundle = new Bundle();
                    switch (action){
                        case Constants.Actions.CREATE:
                            intent = new Intent(context, LeaveApprvDetActivity.class);
                            intent.putExtra("LEAVE OBJECT", leaveRequestList);
                            intent.putExtra("HAND OVER EMPLOYES", empHandoverList);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            break;

                        case Constants.Actions.APPROVE:
                            intent = new Intent(context, LeaveReqDetActivity.class);
                            bundle.putSerializable("LEAVE", leaveRequestList);
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            break;

                        case Constants.Actions.REJECT:
                            intent = new Intent(context, LeaveReqDetActivity.class);
                            bundle.putSerializable("LEAVE", leaveRequestList);
                            intent.putExtras(bundle);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            break;

                        case Constants.Actions.CANCEL:
                            intent = new Intent(context, HomeScreenActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(intent);
                            break;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String result) {

            }
        });

    }
}
