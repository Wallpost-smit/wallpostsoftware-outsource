package com.SMIT.WallPostSoftware.common;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.WebManager;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter.SelectedEmployeeAdapter;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.LeaveApprvDetActivity;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.RequestApprovalFragment;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.SelectEmplyActivity;
import com.SMIT.WallPostSoftware.R;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by alex on 04/10/16.
 */
public class ApprovDialog {

    public String status,  clearance="0", replaced_required="0", handover ="0";


    public ArrayList<Employee> selectedEmpList;
    public ArrayList<Employee> selectdEmpDisplayList ;
    public ArrayList<Employee> empHandoverList = new ArrayList<>();
    public ArrayList<String> empIdList = new ArrayList<>();


    public String lv_actn_url, comments, frmWhere;


    private SelectedEmployeeAdapter sAdapter;

    public MaterialDialog _dialog, _rejectDialog;
    public Switch _clearanceSwitch, _handoverSwitch;
    public EditText _replacmntByOthers, _comments;
    public ImageButton _selectEmpButtn;
    public LinearLayout _selectEmplyLayout, _handoverLayout, _replaceOthersLayout;
    TextInputLayout _inputLayoutReplType,_inputLayoutComment,_inputLayoutOtherEmployee,_inputLayoutEmployeeList, _inputLayoutCancelReasn;
    public Spinner _replcmntReqSpinnr;
    public RecyclerView _handoverEmpRecyclr;

    public EditText _resn;

    private ProgressDialog progressDialog;


    Context context;

    RequestApprovalFragment req;
    LeaveApprvDetActivity leaveAct;
    LeaveRequestList leave;

    public ApprovDialog(RequestApprovalFragment req) {
        this.req = req;
    }

    public ApprovDialog(LeaveApprvDetActivity lv) {
        this.leaveAct = lv;
    }

    public void loadApprovDialog(final Context context, final String frmWhere, final ArrayList<Employee> selectedEmpList, final ArrayList<Employee> empHandoverList, final String status, final String lvId, final ArrayList<Employee> empSelectdFrmRespnse, final LeaveRequestList leave){

        this.context = context;
        this.leave = leave;

        Log.d("LEAVE ID  ",lvId);
        this.frmWhere = frmWhere;
        if(selectedEmpList != null){

            this.selectedEmpList = selectedEmpList;

            for(int i = 0; i<selectedEmpList.size(); i++){
                Employee lv = selectedEmpList.get(i);
            }
        }


        _dialog = new MaterialDialog.Builder(context)
                .title("Leave Approval")
                .cancelable(false)
                .customView(R.layout.leave_accept_dialog, true)
                .negativeText("Cancel")
                .positiveText("Approve")
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                        Log.d("APPROVE ", "BUTTN CLICKED");

                        submitForm( status, lvId);

                    }

                })

                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        _dialog.dismiss();
                    }
                })

                .build();




        _clearanceSwitch = (Switch) _dialog.findViewById(R.id.clearence_id);
        _clearanceSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean check) {

                if(check){
                    clearance="1";
                }else {
                    clearance="0";
                }

            }
        });

        _handoverSwitch = (Switch) _dialog.findViewById(R.id.handover_id);
        _handoverSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean check) {

                if(check){
                    handover = "1";
                }else {
                    handover = "0";
                }
            }
        });

        _replacmntByOthers = (EditText) _dialog.findViewById(R.id.replacmnt_id);
        _replacmntByOthers.addTextChangedListener(new MyTextWatcher(_replacmntByOthers));

        _comments = (EditText) _dialog.findViewById(R.id.comments_id);
        _comments.addTextChangedListener(new MyTextWatcher(_comments));

        _selectEmpButtn = (ImageButton) _dialog.findViewById(R.id.select_emp_buttn);
        _selectEmpButtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                _dialog.dismiss();

                Intent intent = new Intent(view.getContext(), SelectEmplyActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("LEAVE",leave);
                intent.putExtras(bundle);


                if(selectedEmpList !=null){
                    intent.putExtra("HAND OVER EMPLOYES", (Serializable) selectedEmpList);
                }else {
                    intent.putExtra("HAND OVER EMPLOYES", (Serializable) empHandoverList);
                }

                intent.putExtra("LEAVE ID",lvId);
                if(frmWhere.equals("REQUEST_APPROVAL_FRAGMENT")){
                    req.startActivityForResult(intent,1);

                }else {
                    leaveAct.startActivityForResult(intent,1);
                }
            }
        });



        _selectEmplyLayout = (LinearLayout) _dialog.findViewById(R.id.select_empl_layout);
        _handoverLayout = (LinearLayout) _dialog.findViewById(R.id.handover_layout);
        _replaceOthersLayout = (LinearLayout) _dialog.findViewById(R.id.replace_others_layout);
        _inputLayoutReplType = (TextInputLayout) _dialog.findViewById(R.id.input_layout_repl_type);
        _inputLayoutComment= (TextInputLayout) _dialog.findViewById(R.id.input_layout_comment);
        _inputLayoutEmployeeList = (TextInputLayout) _dialog.findViewById(R.id.input_layout_employee_list);
        _handoverEmpRecyclr = (RecyclerView) _dialog.findViewById(R.id.recyclr_handover_emp_list) ;


        _inputLayoutOtherEmployee = (TextInputLayout) _dialog.findViewById(R.id.input_layout_repl_employee);
        _replcmntReqSpinnr = (Spinner) _dialog.findViewById(R.id.replcmnt_req_spinner);
        _replcmntReqSpinnr.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                _inputLayoutReplType  .setErrorEnabled(false);
                switch (i){
                    case 0:


                            replaced_required = "0";
                            Log.d("PLEASE SELECT ", "SELECTED");
                            if(empSelectdFrmRespnse == null ){
                                _handoverEmpRecyclr.setVisibility(View.GONE);
                            }else if(empSelectdFrmRespnse.size() != 0){
                                Log.d("REC ", "VISIBLE");
                                _handoverEmpRecyclr.setVisibility(View.VISIBLE);
                            }else {
                                Log.d("REC ", "INVISIBLE");
                                _handoverEmpRecyclr.setVisibility(View.GONE);
                            }
                            _handoverLayout.setVisibility(View.GONE);
                            _selectEmplyLayout.setVisibility(View.GONE);



                        break;

                    case 1:

                            replaced_required = "2";
                            Log.d("YES ", "SELECTED");
                            loadReplacementData();
                            _handoverLayout.setVisibility(View.VISIBLE);
                            _selectEmplyLayout.setVisibility(View.VISIBLE);
                            _replaceOthersLayout.setVisibility(View.GONE);



                        break;

                    case 2:


                            replaced_required = "1";
                            Log.d("NO ", "SELECTED");
                            _selectEmplyLayout.setVisibility(View.GONE);
                            _handoverLayout.setVisibility(View.GONE);
                            _replaceOthersLayout.setVisibility(View.GONE);
                            _handoverEmpRecyclr.setVisibility(View.GONE);


                        break;

                    case 3:

                        replaced_required = "3";
                        Log.d("OTHERS ", "SELECTED");
                        _handoverLayout.setVisibility(View.GONE);
                        _selectEmplyLayout.setVisibility(View.GONE);
                        _replaceOthersLayout.setVisibility(View.VISIBLE);
                        _handoverEmpRecyclr.setVisibility(View.GONE);
                        break;
                }


                leave.setReplaceRequird(replaced_required);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });





            loadData();   // Load Spinner Data
            loadReplacementData();





//        if (empSelectdFrmRespnse!=null){
//            Log.d("APPRV ", "DIALL ");
//            _handoverEmpRecyclr.setVisibility(View.VISIBLE);
//
//            sAdapter = new SelectedEmployeeAdapter(empSelectdFrmRespnse);
//            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
//            _handoverEmpRecyclr.setLayoutManager(mLayoutManager);
//            _handoverEmpRecyclr.setItemAnimator(new DefaultItemAnimator());
//            _handoverEmpRecyclr.setAdapter(sAdapter);
//        }





        _dialog.show();
    }

    private void submitForm(final String status, final String lvId) {
        if (!validateReplacementType()) {
            Log.d("VALIDATE REPLACEMENT ", "IF");
            return;
        }
        else if (!validateOtherEmployee()){
            Log.d("OTHER EMPLOYEE ", "IF");
            return;
        }

        //Krishna
//        else if (!validateComment()){
//
//            Log.d("VALIDATE COMMENT ", "IF");
//            return;
//        }
        else if (!validateReplacedBy() ){
            Log.d("VALIDATE Replaced By ", "IF");
            return;
        }
        else{
            Log.d("SUBMTI ", "IF");
            submitApprovalData(status,lvId);
        }

    }

    public Boolean validateReplacedBy(){


    if (leave.getReplaceRequird().equals("2")) {
        if (selectedEmpList != null) {
            for (int i = 0; i < selectedEmpList.size(); i++) {
                Employee employee = selectedEmpList.get(i);
                if (employee.isChecked()) {
                    empIdList.add(employee.getEmpId());
                }
            }

            if (!(empIdList.size() > 0)) {
                _inputLayoutEmployeeList.setErrorEnabled(true);
                _inputLayoutEmployeeList.setError(context.getString(R.string.err_replacement_employee));
                return false;
            }
        } else {
            _inputLayoutEmployeeList.setErrorEnabled(true);
            _inputLayoutEmployeeList.setError(context.getString(R.string.err_replacement_employee));

            return false;
        }
    }

        return true;
    }

    public Boolean validateReplacementType(){
        if (replaced_required.equals("0")) {
            _inputLayoutReplType.setErrorEnabled(true);
            _inputLayoutReplType.setError(context.getString(R.string.err_replacement_type));
            return false;
        } else {

        }

        return true;
    }

    public Boolean validateComment(){
        if (_comments.getText().toString().isEmpty()) {
            _inputLayoutComment.setErrorEnabled(true);
            _inputLayoutComment.setError(context.getString(R.string.err_comments));
            return false;
        } else {
            _inputLayoutComment.setErrorEnabled(false);
        }

        return true;
    }

    public Boolean validateOtherEmployee(){


        if (_replacmntByOthers.getText().toString().isEmpty() && leave.getReplaceRequird().equals("3")) {
            _inputLayoutOtherEmployee.setErrorEnabled(true);
            _inputLayoutOtherEmployee.setError(context.getString(R.string.err_employee_name));
            return false;
        } else {
            _inputLayoutOtherEmployee.setErrorEnabled(false);
        }

        return true;
    }


    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.comments_id:
                    //validateComment();
                    break;
                case R.id.replacmnt_id:
                    //validateOtherEmployee();
                    break;


            }
        }
    }


    //SUBMIT APPROVAL DATA
    public void submitApprovalData(final String status, final String lvId){

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("status", status);
            jsonObject.put("id", lvId);
            jsonObject.put("comments", _comments.getText().toString());
            jsonObject.put("clearance", clearance);

            if(replaced_required.equals("1")){
                jsonObject.put("replaced_required", replaced_required);

                leavAction(jsonObject);
            }else if(replaced_required.equals("2")){
                jsonObject.put("replaced_required", replaced_required);
                jsonObject.put("handover", handover);


                    JSONArray jEmpIdArray = new JSONArray(empIdList);
                    jsonObject.put("replaced_by", jEmpIdArray);
                    leavAction(jsonObject);


            }else if(replaced_required.equals("3")){
                jsonObject.put("replaced_required", replaced_required);
                jsonObject.put("replaced_by_others",_replacmntByOthers.getText().toString());
                leavAction(jsonObject);
            }

            jsonObject.put("comments", _comments.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        _dialog.dismiss();
        Log.d("JSON OBJ ", jsonObject.toString());
        Log.d("REPLACE REQQ ", replaced_required);


    }

    //SENDING THE APPROVE LEAVE REQUEST
    public void leavAction(JSONObject jsonObject){

        Log.d("LEAVE ACTION ", jsonObject.toString());
        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        lv_actn_url = Constants.LEAVE_ACTION_URL;

        Log.d("ALEXXX ", jsonObject.toString());

        WebManager webManager = new WebManager(context);

        webManager.leavAction(lv_actn_url, jsonObject ,new WebManager.VolleyCallback() {
            @Override
            public void onSuccess(String result) {

                try {
                    JSONObject responObj = new JSONObject(result);
                    progressDialog.dismiss();

                    if(responObj.getString("result").equals("1")){

                        if(frmWhere.equals("REQUEST_APPROVAL_FRAGMENT")){
                            Log.d("RESPOSE ", frmWhere);

                            req.loadLvApprvList();
                        }else if(frmWhere.equals("LEAVE_APPRV_DET_ACTIVITY")){
                            Log.d("UPDATE ", "DATA");
                            leaveAct.updateData();

                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String result) {
                progressDialog.dismiss();

            }
        });
    }


    //LOAD REJECT DIALOG
    public void loadRejectDialog(final Context context, final String frmWhere, final String status, final String lvId, final String rejectReasn){
        this.context = context;

        this.frmWhere = frmWhere;

        _rejectDialog = new MaterialDialog.Builder(context)
                .title("Are you sure want to reject the leave ?")
                .customView(R.layout.cancel_reason, true)
                .cancelable(false)
                .negativeText("No")
                .positiveText("Yes")
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        //showToast("Password: " + passwordInput.getText().toString());

                        //dialog.dismiss();
                        //_resn = (EditText) dialog.findViewById(R.id.reason_cancel_lv);

                        Log.d("TEST ","TEST");
                        if(rejectReasn!=null){
                            _resn.setText(rejectReasn);
                        }
                        comments = _resn.getText().toString();

                        if(comments.length()>1){
                            dialog.dismiss();
                            JSONObject jsonObject = new JSONObject();
                            try {
                                jsonObject.put("status", status);
                                jsonObject.put("id", lvId);
                                jsonObject.put("comments", comments);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            if(frmWhere.equals("REQUEST_APPROVAL_FRAGMENT")){
                                req.leavAction(jsonObject);
                            }else if(frmWhere.equals("LEAVE_APPRV_DET_ACTIVITY")){
                                leavAction(jsonObject);
                            }

                        }else {
                            _inputLayoutCancelReasn.setErrorEnabled(true);
                            _inputLayoutCancelReasn.setError("Reason cannot be empty");

                        }

                    }

                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .build();
        _inputLayoutCancelReasn = (TextInputLayout) _rejectDialog.findViewById(R.id.input_layout_cancelReasn);

        _resn = (EditText) _rejectDialog.findViewById(R.id.reason_cancel_lv);
        _resn.setText(rejectReasn);


        _rejectDialog.show();
    }


    public void loadData() {

        Log.d("TEST ",leave.getClearence_status());

        final ArrayList<String> replcmntReqList = new ArrayList<String>();

        replcmntReqList.add("Please Select");
        replcmntReqList.add("Yes");
        replcmntReqList.add("No");
        replcmntReqList.add("Others");
        //   }


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, replcmntReqList);

        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        _replcmntReqSpinnr.setAdapter(dataAdapter);


        if ( leave.getReplaceRequird() != null) {

            if (leave.getReplaceRequird().equals("2")) {
                _replcmntReqSpinnr.setSelection(1);
                replaced_required = "2";

            } else if (leave.getReplaceRequird().equals("1")) {
                _replcmntReqSpinnr.setSelection(2);
                replaced_required = "1";
            } else if (leave.getReplaceRequird().equals("3")) {
                _replcmntReqSpinnr.setSelection(3);
                replaced_required = "3";
            } else {
                _replcmntReqSpinnr.setSelection(0);
                replaced_required = "0";
            }
        }

        //ALEX CODE
        if(leave.getHandOver_status().equals("4")){

            _handoverSwitch.setChecked(false);
            handover = "0";
        }else {

            _handoverSwitch.setChecked(true);
            handover = "1";

        }

        if(leave.getClearence_status().equals("4")){

            _clearanceSwitch.setChecked(false);
            clearance = "0";
        }else {
            _clearanceSwitch.setChecked(true);
            clearance = "1";

        }

        //Krishna CODE

        if (leave.getLeave_status().equals("1")){
            _comments.setText(leave.getApproval_comments());
        }
    }


    public void loadReplacementData(){
        if(selectedEmpList != null){

            selectdEmpDisplayList = new ArrayList<>();

            for(int i = 0; i<selectedEmpList.size(); i++){
                Employee employee = selectedEmpList.get(i);
                if(employee.isChecked()){
                    selectdEmpDisplayList.add(employee);
                }
            }
            _handoverEmpRecyclr.setVisibility(View.VISIBLE);
            sAdapter = new SelectedEmployeeAdapter(selectdEmpDisplayList);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
            _handoverEmpRecyclr.setLayoutManager(mLayoutManager);
            _handoverEmpRecyclr.setItemAnimator(new DefaultItemAnimator());
            _handoverEmpRecyclr.setAdapter(sAdapter);
        }else {
            _handoverEmpRecyclr.setVisibility(View.GONE);
        }

    }
}
