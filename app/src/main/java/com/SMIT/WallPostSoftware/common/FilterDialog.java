package com.SMIT.WallPostSoftware.common;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter.CurrentLvListAdapter;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.CurrentLvFragment;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.HistoryLvFragment;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.LeaveRequestFragment;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.RequestApprovalFragment;
import com.SMIT.WallPostSoftware.R;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;

/**
 * Created by alex on 17/10/16.
 */
public class FilterDialog {


    public MaterialDialog _dialog;
    public CheckBox _lvTypeCheckBox, _lvActionCheckBox;
    public RadioGroup _lvCurrentRadioGrp, _lvHistoryRadioGrp, _reqApprvlRadioGrp;
    public RadioButton _lvAllTypeRadioBtn;
    public LinearLayout _lvActionLayout;
    public RecyclerView _recycleView;
    public ArrayList<LeaveRequestList> leaveRequestList = new ArrayList<>();

    public LeaveRequestFragment lvReqFrag = new LeaveRequestFragment();
    public CurrentLvFragment currentLvFragment=new CurrentLvFragment();
    public HistoryLvFragment historyLvFragment = new HistoryLvFragment();
    public Context context;



    public void loadFilterDialog(final Context context, final LeaveRequestFragment lvReqFrag, final RequestApprovalFragment reqApprvlFrag){

        this.context = context;

        if(lvReqFrag!=null){

            Log.d("Kiliii ", String.valueOf(lvReqFrag.tabPosition));

            _dialog = new MaterialDialog.Builder(context)
                    .customView(R.layout.leave_filter_layout, false)
                    .negativeText("Cancel")
                    .autoDismiss(false)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            _dialog.dismiss();
                        }
                    })
                    .build();

            _lvActionLayout = (LinearLayout) _dialog.findViewById(R.id.lv_action_layout);
            _lvTypeCheckBox = (CheckBox) _dialog.findViewById(R.id.lv_type_checkbox);
            _lvActionCheckBox = (CheckBox) _dialog.findViewById(R.id.lv_action_checkbox);

            _lvCurrentRadioGrp = (RadioGroup) _dialog.findViewById(R.id.lv_current_radio_grp);
            _lvHistoryRadioGrp = (RadioGroup) _dialog.findViewById(R.id.lv_history_radio_grp);
            _lvAllTypeRadioBtn = (RadioButton) _dialog.findViewById(R.id.all_leaves);

            _lvAllTypeRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean check) {

                    if(check){

                        if(lvReqFrag.tabPosition == 0){
                            currentLvFragment.loadFilter(context, lvReqFrag, " ");
                            _dialog.dismiss();
                        }else {
                            historyLvFragment.loadFilter(context, lvReqFrag, " ");
                            _dialog.dismiss();
                        }
                    }
                }
            });

            if(lvReqFrag.tabPosition == 0){
                _lvCurrentRadioGrp.setVisibility(View.VISIBLE);
            }else {
                _lvHistoryRadioGrp.setVisibility(View.VISIBLE);
            }

//        _lvActionCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean check) {
//
//                if(check){
//                    _lvActionLayout.setVisibility(View.VISIBLE);
//                }else {
//                    _lvActionLayout.setVisibility(View.GONE);
//                }
//            }
//        });


            _lvCurrentRadioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
            {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    switch (checkedId){

                        case R.id.pending_leaves:
                            currentLvFragment.loadFilter(context, lvReqFrag, "0");
                            _dialog.dismiss();
                            break;

                        case R.id.approved_leaves:
                            currentLvFragment.loadFilter(context, lvReqFrag, "1");
                            _dialog.dismiss();
                            break;

                        default:
                            Log.d("TESTST ", String.valueOf(checkedId));
                            break;

                    }
                    //Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
                }
            });

            _lvHistoryRadioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
            {
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    // checkedId is the RadioButton selected
                    switch (checkedId){

                        case R.id.reject_leaves:
                            historyLvFragment.loadFilter(context, lvReqFrag, "2");
                            _dialog.dismiss();
                            break;

                        case R.id.cancelled_leaves:
                            historyLvFragment.loadFilter(context, lvReqFrag, "3");
                            _dialog.dismiss();
                            break;

                        default:
                            Log.d("TESTST ", String.valueOf(checkedId));
                            break;

                    }
                    //Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
                }
            });


//        _lvPendingCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean check) {
//
//                if(check){
//
//                    currentLvFragment.loadFilter(context, lvReqFrag);
//
//                }else {
//
//                }
//            }
//        });


            _dialog.show();
        }else  if(reqApprvlFrag!=null){

            _dialog = new MaterialDialog.Builder(context)
                    .customView(R.layout.leave_filter_layout, false)
                    .negativeText("Cancel")
                    .autoDismiss(false)
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            _dialog.dismiss();
                        }
                    })
                    .build();

            _lvActionLayout = (LinearLayout) _dialog.findViewById(R.id.lv_action_layout);
            _lvTypeCheckBox = (CheckBox) _dialog.findViewById(R.id.lv_type_checkbox);
            _lvActionCheckBox = (CheckBox) _dialog.findViewById(R.id.lv_action_checkbox);

            _reqApprvlRadioGrp = (RadioGroup) _dialog.findViewById(R.id.req_apprvl_radio_grp);
            _reqApprvlRadioGrp.setVisibility(View.VISIBLE);
            _lvAllTypeRadioBtn = (RadioButton) _dialog.findViewById(R.id.all_leaves);

            _lvAllTypeRadioBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean check) {

                    if(check){

                        reqApprvlFrag.loadFilter(context, reqApprvlFrag, " ");
                        _dialog.dismiss();

                    }
                }
            });

            _reqApprvlRadioGrp.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                    switch (checkedId){
                        case R.id.req_apprvl_pending_leaves:
                            reqApprvlFrag.loadFilter(context, reqApprvlFrag,"0");
                            _dialog.dismiss();
                            break;

                        case R.id.req_apprvl_approved_leaves:
                            reqApprvlFrag.loadFilter(context, reqApprvlFrag,"1");
                            _dialog.dismiss();
                            break;

                        case R.id.req_apprvl_rejected_leaves:
                            reqApprvlFrag.loadFilter(context, reqApprvlFrag,"2");
                            _dialog.dismiss();
                            break;

                        case R.id.req_apprvl_cancel_leaves:
                            reqApprvlFrag.loadFilter(context, reqApprvlFrag,"3");
                            _dialog.dismiss();
                            break;
                    }
                }
            });
            _dialog.show();

        }

    }
}
