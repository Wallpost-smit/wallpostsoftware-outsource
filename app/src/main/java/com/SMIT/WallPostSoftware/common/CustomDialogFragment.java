package com.SMIT.WallPostSoftware.common;


import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Extras.PermissionUtility;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter.SelectImageAdapter;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.SelectImage;
import com.SMIT.WallPostSoftware.R;

import java.io.IOException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CustomDialogFragment extends DialogFragment {

    public ImageView _uploadImage;
    public LinearLayout _rv_LinearLayoutRvContainer;
    private RecyclerView _selectImageView;
    public TextView _employeeName;

    public String userChoosenTask;
    int REQUEST_CAMERA = 100, SELECT_FILE = 101;

    String imagePath;
    ArrayList<String> imagePathList = new ArrayList<>();
    public ArrayList<SelectImage> bitmapList = new ArrayList<>();


    Bitmap bm, converetdImage;




    public CustomDialogFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.custom_dialog_fragment, container, false);
        getDialog().setTitle("Simple Dialog");

        View fileUploadLayout  = rootView.findViewById( R.id.upload_image );
        _employeeName = (TextView) fileUploadLayout.findViewById(R.id.employee_name);
        _rv_LinearLayoutRvContainer = (LinearLayout) fileUploadLayout.findViewById(R.id.rv_LinearLayoutRvContainer);
        _selectImageView = (RecyclerView) fileUploadLayout.findViewById(R.id.images_recycler_view);
        _uploadImage = (ImageView) fileUploadLayout.findViewById(R.id.attach_file);

        _employeeName.setText(getArguments().getString("EMPLOYEE_NAME"));
        _uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("UPLOAD ", "CLICKED");
                selectImage();
            }
        });
        return rootView;
    }

    //SELECTING THE IMAGE FOR UPLOAD DIALOG
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = PermissionUtility.checkPermission(getActivity());
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        Log.d("FF ","FF");
                        cameraIntent();
                } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        Log.d("FF ","FF");
                        galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
    private void galleryIntent() {

        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        Intent chooserIntent = Intent.createChooser(galleryIntent, "PICK IMAGE");
        startActivityForResult(chooserIntent, SELECT_FILE);
    }

    //SELECTING THE IMAGE FROM GALLERY
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImageUri = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getActivity().getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);



        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imagePath = cursor.getString(columnIndex);
            imagePathList.add(imagePath);
            Log.d("imagePath COLUMN ", String.valueOf(columnIndex));
            cursor.close();


            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                    converetdImage = Bitmap.createScaledBitmap(bm, 300, 300, true);;

                    SelectImage selectImage = new SelectImage();
                    selectImage.setBitmap(converetdImage);
                    selectImage.setImagePath(imagePath);
                    updateImageArray("ADD",selectImage);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

//            Picasso.with(this).load(new File(imagePath))
//                    .into(imageView);

        } else {

        }
//        bm = null;


    }

    public void updateImageArray(String action, SelectImage selectImage){


        if(action.equals("ADD")){
            bitmapList.add(selectImage);

        }else {
            bitmapList.remove(selectImage);

        }
        _rv_LinearLayoutRvContainer.setVisibility(View.VISIBLE);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        _selectImageView.setLayoutManager(mLayoutManager);
        _selectImageView.setItemAnimator(new DefaultItemAnimator());
        _selectImageView.setAdapter(new SelectImageAdapter(bitmapList, null, CustomDialogFragment.this));

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // In fragment class callback

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE){
               onSelectFromGalleryResult(data);
            }
            else if (requestCode == REQUEST_CAMERA){
                //  onCaptureImageResult(data);

                Log.d("FF ","FF");
            }
        }
    }

}
