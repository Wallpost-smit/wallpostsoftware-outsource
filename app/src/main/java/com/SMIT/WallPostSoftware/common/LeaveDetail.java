package com.SMIT.WallPostSoftware.common;

import android.util.Log;

import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.FileData;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by alex on 07/10/16.
 */
public class LeaveDetail {

   private JSONObject jsonObject = null;

    public ArrayList<Employee> empHandoverList = new ArrayList<>();


    public ArrayList<HashMap<Integer,Employee>> selectdEmpListFrmResponse;

    public ArrayList<HashMap<Integer,FileData>> fileListFrmResponse = new ArrayList<>();

    //GET LIST OF LEAVE OBJECT
    public LeaveRequestList getLeaveData(JSONObject jsonLv){
        this.jsonObject = jsonLv;
        LeaveRequestList leaveRequestList = new LeaveRequestList();
        Employee employee = new Employee();

        try {
            leaveRequestList.setLeave_id(jsonLv.getString("id"));
            leaveRequestList.setLeave_frm(jsonLv.getString("leave_from"));
            leaveRequestList.setLeave_to(jsonLv.getString("leave_to"));
            leaveRequestList.setLeave_days(jsonLv.getString("leave_days"));
            leaveRequestList.setLeave_paid(jsonLv.getString("paid_days"));
            leaveRequestList.setLeave_unpaid(jsonLv.getString("unpaid_days"));
            leaveRequestList.setLeave_status(jsonLv.getString("status"));
            leaveRequestList.setHandOver_status(jsonLv.getString("handover_status"));
            leaveRequestList.setClearence_status(jsonLv.getString("clearance_status"));
            leaveRequestList.setLeave_resn(jsonLv.getString("leave_reason"));
            leaveRequestList.setLeave_status(jsonLv.getString("status"));
            leaveRequestList.setLeave_contact(jsonLv.getString("contact_on_leave"));
            leaveRequestList.setLeave_email(jsonLv.getString("contact_email"));


            //                        REPLACED EMPLOYEEEE
            if(leaveRequestList.getLeave_status().equals("2")){
                leaveRequestList.setRejected_reason(jsonLv.getString("rejected_reason"));
            }if(leaveRequestList.getLeave_status().equals("1")){
                leaveRequestList.setApproval_comments(jsonLv.getString("approval_comments"));
                leaveRequestList.setReplaceRequird(jsonLv.getString("replaced_required"));
                if(leaveRequestList.getReplaceRequird().equals("2")){

                    selectdEmpListFrmResponse=new ArrayList<>();
                    JSONArray jReplaceEmplArray = jsonLv.getJSONArray("replaced_by");
                    Log.e("REPLACE ARRYA COUNT",""+jReplaceEmplArray.length());
                    HashMap<Integer,Employee> replacEmpMap=new HashMap<Integer, Employee>();
                    for(int j =0; j<jReplaceEmplArray.length(); j++){

                        //Anas Code
                        JSONObject jEmpReplacObj = jReplaceEmplArray.getJSONObject(j);

                        Employee e = new Employee();
                        e = parseEmployee(jEmpReplacObj);
                        replacEmpMap.put(j,e);
                        selectdEmpListFrmResponse.add(replacEmpMap);

                    }
                    leaveRequestList.employee.setSelectdListFrmResponse(selectdEmpListFrmResponse);

                }
            }if(leaveRequestList.getLeave_status().equals("3")){
                leaveRequestList.setCancelled_reason(jsonLv.getString("cancel_reason"));
            }

            //FILE DATA
            JSONArray jsonFileArry = jsonLv.getJSONArray("attach_doc");
            if (jsonFileArry.length() > 0){

                fileListFrmResponse=new ArrayList<>();

                HashMap<Integer,FileData> fileDataMap=new HashMap<Integer, FileData>();
                for (int k= 0;k<jsonFileArry.length(); k++){

                    JSONObject jsonFileObj = jsonFileArry.getJSONObject(k);

                    leaveRequestList.fileData = new FileData();

                    leaveRequestList.fileData.setFileName(jsonFileObj.getString("document_file_name"));
                    leaveRequestList.company.setCompanyId(jsonFileObj.getString("company_id"));
                    leaveRequestList.fileData.setRefrnceId(jsonFileObj.getString("reference_id"));
                    leaveRequestList.fileData.setAttachmentData(jsonFileObj.getString("attachment"));

                    fileDataMap.put(k,leaveRequestList.fileData);

                    fileListFrmResponse.add(fileDataMap);

                }

                leaveRequestList.fileData.setFileListFrmResponse(fileListFrmResponse);
            }

            JSONObject empObject = jsonLv.getJSONObject("employee");
            // Employee emp = new Employee();
            Log.d("ALEXX ", empObject.getString("profile_image"));
            leaveRequestList.employee.setEmpProfilePic(empObject.getString("profile_image"));
            //leaveApprvData.setEmployee(emp);

            JSONObject deptObject = empObject.getJSONObject("department");
            leaveRequestList.employee.setDepartment(deptObject.getString("name"));

            JSONObject posObject = empObject.getJSONObject("position");
            leaveRequestList.employee.setPosition(posObject.getString("name"));

            leaveRequestList.employee.setEmpName(empObject.getString("name") + " " +empObject.getString("middle_name") + " " +empObject.getString("short_name"));

            leaveRequestList.employee.setEmpStaffNo(empObject.getString("code"));
            leaveRequestList.employee.setEmpJoiningDate(empObject.getString("join_date"));

            JSONObject gradeObject = empObject.getJSONObject("grade");
            leaveRequestList.employee.setEmpGrade(gradeObject.getString("grade"));

            JSONObject lvTypObject = jsonLv.getJSONObject("leave_type");
            leaveRequestList.setLeave_type(lvTypObject.getString("name"));


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return leaveRequestList;

        }

    //HANDOVER EMPLOYEE DETAILS FROM THE SERVER
    public Employee parseEmployee(JSONObject empObject){
        Employee employee = new Employee();
        try {
            employee.setEmpProfilePic(empObject.getString("profile_image"));
            employee.setEmpName(empObject.getString("name") + " " + empObject.getString("middle_name") + " " + empObject.getString("short_name"));
            employee.setEmpStaffNo(empObject.getString("code"));
            employee.setEmpJoiningDate(empObject.getString("join_date"));

            if (!empObject.isNull("grade")){
                JSONObject gradeObject = empObject.getJSONObject("grade");
                employee.setEmpGrade(gradeObject.getString("grade"));
            }

            if (!empObject.isNull("department")){

                JSONObject deptObject = empObject.getJSONObject("department");
                employee.setDepartment(deptObject.getString("name"));
            }

            if (!empObject.isNull("position")){
                JSONObject posObject = empObject.getJSONObject("position");
                employee.setPosition(posObject.getString("name"));
            }
            empHandoverList.add(employee);

        }catch (Exception e){

        }
        return employee;
    }


    //GET LIST OF EMPLOYEES TO BE HANDOVER
    public Employee getHandOverEmplys(JSONObject jsonEmp){

        Employee employee = new Employee();

        try {
            employee.setEmpId(jsonEmp.getString("id"));
            employee.setEmpName(jsonEmp.getString("name") + " " +jsonEmp.getString("middle_name") + " " +jsonEmp.getString("short_name"));
            JSONObject jPosition = jsonEmp.getJSONObject("position");
            employee.setPosition(jPosition.getString("name"));
            employee.setEmpProfilePic(jsonEmp.getString("profile_image"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return employee;

    }

}

