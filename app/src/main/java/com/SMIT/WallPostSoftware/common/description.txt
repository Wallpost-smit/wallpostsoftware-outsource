All the files that have to be accessed globally should be added here.
For instance, this may include the WebService class or the Constants class.

Please feel free to create any sub folders or packages to group similar classes.