package com.SMIT.WallPostSoftware.common.Adapter;

import android.content.Context;

import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.SMIT.WallPostSoftware.Extras.FileUtils;
import com.SMIT.WallPostSoftware.Model_Class.FileData;
import com.SMIT.WallPostSoftware.R;

import java.util.ArrayList;

/**
 * Created by alex on 26/10/16.
 */
public class SlidingImage_Adapter extends PagerAdapter {

    private ArrayList<FileData> IMAGES;
    private LayoutInflater inflater;
    private Context context;


    public SlidingImage_Adapter(Context context,ArrayList<FileData> IMAGES) {
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = inflater.inflate(R.layout.slidingimages_layout, container, false);

        FileData fileImagObj = IMAGES.get(position);

        Log.d("BLAAA ", fileImagObj.getAttachmentData());

        ImageView imageView = (ImageView) itemView.findViewById(R.id.slide_image);
        FileUtils.loadImage(fileImagObj.getAttachmentData(),context,imageView);

        //imageView.setImageResource(IMAGES.get(position).getAttachmentData());

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return false;
    }
}
