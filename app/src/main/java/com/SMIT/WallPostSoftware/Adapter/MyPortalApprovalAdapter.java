package com.SMIT.WallPostSoftware.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.SMIT.WallPostSoftware.Model_Class.MyPortal_Approval;
import com.SMIT.WallPostSoftware.R;

import java.util.List;

/**
 * Created by alex on 01/08/16.
 */
public class MyPortalApprovalAdapter extends RecyclerView.Adapter<MyPortalApprovalAdapter.EmployeeApprovalViewHolder>  {

    private List<MyPortal_Approval> approvalList;


    public class EmployeeApprovalViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView _name, _type;
        public ImageButton _cancelButton;

        public EmployeeApprovalViewHolder(View view) {
            super(view);
            _name = (TextView) view.findViewById(R.id.name);
            _type = (TextView) view.findViewById(R.id.leave_type);
            _cancelButton = (ImageButton) view.findViewById(R.id.cancel_button);
            _cancelButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Toast.makeText(view.getContext(), "ITEM PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
        }
    }

    public MyPortalApprovalAdapter(List<MyPortal_Approval> employeeList) {
        this.approvalList = employeeList;
    }

    @Override
    public MyPortalApprovalAdapter.EmployeeApprovalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myportal_approval_row, parent, false);

        return new EmployeeApprovalViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyPortalApprovalAdapter.EmployeeApprovalViewHolder holder, int position) {

        MyPortal_Approval staff = approvalList.get(position);
        holder._name.setText(staff.getName());
        holder._type.setText(staff.getLeave_type());
    }

    @Override
    public int getItemCount() {
        return approvalList.size();
    }
}
