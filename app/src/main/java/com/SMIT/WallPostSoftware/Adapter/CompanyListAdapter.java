package com.SMIT.WallPostSoftware.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Extras.FileUtils;
import com.SMIT.WallPostSoftware.Extras.RoundedImageView;
import com.SMIT.WallPostSoftware.Model_Class.CompanyList;
import com.SMIT.WallPostSoftware.R;

import java.util.List;

/**
 * Created by alex on 06/09/16.
 */
public class CompanyListAdapter extends RecyclerView.Adapter<CompanyListAdapter.CompnyListViewHoldr> {

    private List<CompanyList> cmpnyList;

    private OnItemClickListener listener;
    private Context context;
    int selectedItem;
    public CompanyListAdapter(List<CompanyList> cmpnyList, OnItemClickListener listener,int selectedItem) {
        this.cmpnyList= cmpnyList;
        this.listener = listener;
        this.selectedItem = selectedItem;
    }

    // Define the listener interface
    public interface OnItemClickListener {

        void onItemClick(View itemView, int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class CompnyListViewHoldr extends RecyclerView.ViewHolder {
        public TextView _cmpnyNameTv, _cmpnyShrtNameTv;
        ImageView _cmpnySelected;
        public RoundedImageView _logo;
        public CompnyListViewHoldr(final View itemView) {
            super(itemView);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null)
                        selectedItem = getLayoutPosition();
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });


            _cmpnyNameTv = (TextView) itemView.findViewById(R.id.cmpny_name);
            _cmpnySelected = (ImageView)itemView.findViewById(R.id.cmpny_checked);
            _cmpnyShrtNameTv = (TextView) itemView.findViewById(R.id.cmpny_shrt_name);
            _logo =(RoundedImageView)itemView.findViewById(R.id.logo);

        }
    }


    @Override
    public CompnyListViewHoldr onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.company_list_row, parent, false);
        context = parent.getContext();
        return new CompnyListViewHoldr(itemView);
    }

    @Override
    public void onBindViewHolder(CompnyListViewHoldr holder, int position) {

        CompanyList companyList = cmpnyList.get(position);
        holder._cmpnyNameTv.setText(companyList.getCompanyName());
        holder._cmpnyShrtNameTv.setText(companyList.getCompanyShortName());
        //FileUtils.loadImage(companyList.getCompanyLogo(),R.drawable.default_user,context,holder._logo);

        if (selectedItem == position){
            holder._cmpnySelected.setColorFilter(context.getResources().getColor(R.color.colorPrimary));
        }
        else{
            holder._cmpnySelected.setColorFilter(context.getResources().getColor(R.color.colorLightGrey));

        }

        FileUtils.loadImage(companyList.getCompanyLogo(),context,holder._logo);



    }

    @Override
    public int getItemCount() {
        return cmpnyList.size();
    }

}
