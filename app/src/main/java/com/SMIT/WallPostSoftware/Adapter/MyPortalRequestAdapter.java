package com.SMIT.WallPostSoftware.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Model_Class.MyPortal_Request;
import com.SMIT.WallPostSoftware.R;

import java.util.List;

/**
 * Created by alex on 01/08/16.
 */
public class MyPortalRequestAdapter extends RecyclerView.Adapter<MyPortalRequestAdapter.EmployeeRequestViewHolder> {

    private List<MyPortal_Request> requestList;

    public class EmployeeRequestViewHolder extends RecyclerView.ViewHolder{
        public TextView _type, _task;

        public EmployeeRequestViewHolder(View itemView) {
            super(itemView);
            _type = (TextView) itemView.findViewById(R.id.leave_type);
            _task = (TextView) itemView.findViewById(R.id.task);
        }
    }

    public MyPortalRequestAdapter(List<MyPortal_Request> employeeList) {
        this.requestList = employeeList;
    }
    @Override
    public MyPortalRequestAdapter.EmployeeRequestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myportal_request_row, parent, false);

        return new EmployeeRequestViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyPortalRequestAdapter.EmployeeRequestViewHolder holder, int position) {

        MyPortal_Request employee_request = requestList.get(position);
        holder._type.setText(employee_request.getLeave_type());
        holder._task.setText(employee_request.getTask());
    }

    @Override
    public int getItemCount() {
        return requestList.size();
    }
}
