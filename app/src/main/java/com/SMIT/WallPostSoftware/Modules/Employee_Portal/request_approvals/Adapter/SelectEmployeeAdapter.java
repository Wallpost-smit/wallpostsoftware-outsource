package com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Extras.FileUtils;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.R;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by alex on 23/09/16.
 */
public class SelectEmployeeAdapter extends RecyclerView.Adapter<SelectEmployeeAdapter.SelectEmployeeListViewHolder> {

    public List<Employee> empList;
    public int count;
    public ArrayList<Employee> selectList = new ArrayList<>();
    private Context context;
    public class SelectEmployeeListViewHolder extends RecyclerView.ViewHolder {

        public TextView _nameTv, _desigTv;
        public CheckBox _checkBox;
        public CircleImageView _profileImage;

        public SelectEmployeeListViewHolder(View itemView) {
            super(itemView);

            _nameTv = (TextView) itemView.findViewById(R.id.name);
            _desigTv = (TextView) itemView.findViewById(R.id.designation);
            _profileImage =(CircleImageView)itemView.findViewById(R.id.profile_image);
            _checkBox = (CheckBox) itemView.findViewById(R.id.checkBox);
        }
    }


    public SelectEmployeeAdapter(ArrayList<Employee> empList) {
        this.empList = empList;
    }


    @Override
    public SelectEmployeeListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.select_employee_row, parent, false);
        context = parent.getContext();
        return new SelectEmployeeListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final SelectEmployeeListViewHolder holder, final int position) {

        final Employee employee = empList.get(position);

        holder._nameTv.setText(employee.getEmpName());
        holder._desigTv.setText(employee.getPosition());
        holder._checkBox.setOnCheckedChangeListener(null);
        FileUtils.loadImage(employee.getEmpProfilePic(),context,holder._profileImage);

        if(employee.isChecked()){
            count++;
            holder._checkBox.setChecked(empList.get(position).isChecked());
        }else {
            holder._checkBox.setChecked(empList.get(position).isChecked());
        }

        holder._checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                Log.d("CHECK BOX ", "CLICKEd");
                if(isChecked){
                    count++;
                    Log.e("ISchecked","true");
                    employee.setChecked(true);
                    selectList.add(employee);

                }else {
                    count--;
                    employee.setChecked(false);
                    selectList.remove(employee);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return empList.size();
    }

    public void setFilter(List<Employee> employeeList) {
        empList = new ArrayList<>();
        empList.addAll(employeeList);
        notifyDataSetChanged();
    }

}
