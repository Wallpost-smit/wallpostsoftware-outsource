package com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.SMIT.WallPostSoftware.Extras.FILE_UPLOADING_PARSER.FileDownloader;
import com.SMIT.WallPostSoftware.Model_Class.FileData;
import com.SMIT.WallPostSoftware.R;

import java.util.ArrayList;

/**
 * Created by alex on 24/10/16.
 */
public class DisplayImageAdapter extends RecyclerView.Adapter<DisplayImageAdapter.ImageViewHolder> {

    private ArrayList<FileData> bitmapList;
    private Context context;

    // Define listener member variable
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);

    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }


    public class ImageViewHolder extends RecyclerView.ViewHolder {

        public ImageView _fileImage;

        public ImageViewHolder(final View itemView) {
            super(itemView);

            _fileImage = (ImageView) itemView.findViewById(R.id.selected_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });

        }
    }

    public DisplayImageAdapter(ArrayList<FileData> bitmapList, OnItemClickListener listener) {
        this.bitmapList = bitmapList;
        this.listener = listener;

    }

    @Override
    public DisplayImageAdapter.ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_images_row, parent, false);

        context = parent.getContext();

        return new ImageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(DisplayImageAdapter.ImageViewHolder holder, int position) {

        FileData fileImagObj = bitmapList.get(position);


        Log.d("HELLLOO ", fileImagObj.getAttachmentData());

        FileDownloader.setImage( fileImagObj.getAttachmentData(),holder._fileImage,context);


    }

    @Override
    public int getItemCount() {
        return bitmapList.size();
    }


}
