package com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Extras.FileUtils;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.R;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by alex on 27/09/16.
 */
public class SelectedEmployeeAdapter extends RecyclerView.Adapter<SelectedEmployeeAdapter.SelectedEmployeeListViewHolder>  {

    private List<Employee> empList;
    private Context context;

    public class SelectedEmployeeListViewHolder extends RecyclerView.ViewHolder {

        public TextView _nameTv;
        public CircleImageView _profileImage;

        public SelectedEmployeeListViewHolder(View itemView) {
            super(itemView);

            _nameTv = (TextView) itemView.findViewById(R.id.name);
            _profileImage =(CircleImageView)itemView.findViewById(R.id.profile_image);

        }
    }

    public SelectedEmployeeAdapter(List<Employee> empList) {
        this.empList = empList;
    }



    @Override
    public SelectedEmployeeAdapter.SelectedEmployeeListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_empl_dialog_row, parent, false);
        context = parent.getContext();
        return new SelectedEmployeeListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SelectedEmployeeAdapter.SelectedEmployeeListViewHolder holder, int position) {

        Employee employee = empList.get(position);

        Log.d("TESTSTS ", employee.getEmpName());
        holder._nameTv.setText(employee.getEmpName());
        FileUtils.loadImage(employee.getEmpProfilePic(),context,holder._profileImage);

    }

    @Override
    public int getItemCount() {
        return empList.size();
    }

}
