package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.Entitled;


import java.util.ArrayList;
import java.util.List;


/**
 * Created by alex on 07/09/16.
 */
public class EntitldTypeSpinnerAdapter extends ArrayAdapter<Entitled> {

    private Context context;
    private ArrayList<Entitled> enTitldType;
    public EntitldTypeSpinnerAdapter(Context context, int textViewResourceId, ArrayList<Entitled> enTitldType) {
        super(context, textViewResourceId, enTitldType);

        this.context = context;
        this.enTitldType = enTitldType;
    }

    public int getCount(){
        return enTitldType.size();
    }

    public Entitled getItem(int position){
        return enTitldType.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setText(enTitldType.get(position).getEntitld_name());

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setPadding(30,30,30,30);
        label.setText(enTitldType.get(position).getEntitld_name());

        return label;
    }

}
