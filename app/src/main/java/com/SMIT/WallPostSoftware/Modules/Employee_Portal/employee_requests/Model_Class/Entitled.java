package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class;

/**
 * Created by alex on 07/09/16.
 */
public class Entitled {

    public String entitld_name;
    public String entitld_id;

    public String getEntitld_name() {
        return entitld_name;
    }

    public void setEntitld_name(String entitld_name) {
        this.entitld_name = entitld_name;
    }

    public String getEntitld_id() {
        return entitld_id;
    }

    public void setEntitld_id(String entitld_id) {
        this.entitld_id = entitld_id;
    }
}
