package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.SMIT.WallPostSoftware.Extras.CommonUtils;
import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.FILE_UPLOADING_PARSER.File_uploading_JSONParser;
import com.SMIT.WallPostSoftware.Extras.PermissionUtility;
import com.SMIT.WallPostSoftware.Extras.Preference;
import com.SMIT.WallPostSoftware.Extras.Util;
import com.SMIT.WallPostSoftware.Extras.WebManager;
import com.SMIT.WallPostSoftware.HomeScreenActivity;
import com.SMIT.WallPostSoftware.Model_Class.CompanyList;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.FileData;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter.AirportAdapter;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter.HandOverEmpAdapter;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter.SelectImageAdapter;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter.ViewPaymntDialogAdptr;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.AirportData;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.Entitled;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.LeaveType;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.SelectImage;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.ViewPaymentDialog;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter.DisplayImageAdapter;
import com.SMIT.WallPostSoftware.common.GalleryActivity;
import com.SMIT.WallPostSoftware.R;
import com.SMIT.WallPostSoftware.common.CustomDialogFragment;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class LeaveReqDetActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener, CompoundButton.OnCheckedChangeListener {

    public Spinner _spinnerLvType, _entitldSpinner, _airClassSpinner, _wayTypeSpinner;
    public Switch _exitPermitSwitch, _tcktSwitch;
    FrameLayout _container;
    LeaveRequestList leave;

    public EditText _lvStartDate, _lvEndDate, _depDate, _retrnDate, _depTime, _retTime, _contactNo, _contactEmail, _reason, _noAdultsTv, _noChlrnTv;
    public ImageView _lvStartDateImage, _lvEndDateImage, _depDateImage, _retrnDateImage, _depTimeImage, _retTimeImage, _attchFile, _lvStatusImage;
    public CardView _tcktDataLayout, _flightDataLayout,_leaveCommentLayout, _leaveStatusLayout, _empHandoverLayout;
    public LinearLayout _extPermtLayout, _tcktReqrdLayout, _statusLayout,rv_LinearLayoutRvContainer;
    public TextView  _totPayTv, _entitldTv, _totDaysTv, _paidTv, _unpaidTv, _aftrAplyTv, _bfrAplyTv, _lvCommentTv, _lvStatusTv;

    View _fileUpload;


    public AutoCompleteTextView _originAirport, _destnAirport;
    public FloatingActionButton _sendLvReqBtn;

    private RecyclerView _recyclerView, _selectImageView, _handOverEmpView;
    private List<ViewPaymentDialog> payList = new ArrayList<>();
    private ViewPaymntDialogAdptr mAdapter;
    public DisplayImageAdapter displayImageAdapter;

    private ProgressDialog progressDialog;

    final Calendar c = Calendar.getInstance();

    public String userId, contactNo, contactEmail, reason, employeeId, entitldFor, orginAirPt, destAirPt, depTime, retTime;
    public String req_url, alert_url, create_lv_url, exit_reqrd, tckt_reqrd;
    public long lvFromDate, lvToDate, depDate, retDate;
    public String jResult, jMessage, jTotPayment, jEntitld, jTotDays, jPaid, jUnpaid, jAftrApply, jBfrApply;
    public String userChoosenTask, startDate, endDate;
    public int days;

    public Boolean jExtPrmt, jTcktStatus, jOvrLaping, jElegbity, jPayItems;

    LeaveType selectedLv;
    private int sYear, sMonth, sDay, mHour, mMinute;
    private int eYear, eMonth, eDay, eHour, eMinute;

    ArrayList<LeaveType> leaveTypes = new ArrayList<>();
    ArrayList<Entitled> entitldTypes = new ArrayList<>();
    ArrayList<AirportData> airprtList = new ArrayList<>();
    ArrayList<ViewPaymentDialog> paymentList = new ArrayList<>();

    ArrayList<String> airclassList = new ArrayList<>();
    ArrayList<String> wayTypeList = new ArrayList<>();

    ArrayList<String> leaveNames = new ArrayList<String>();
    ArrayList<String> airportName = new ArrayList<>();
    ArrayList<String> entitldNames = new ArrayList<>();
    public ArrayList<FileData> fileFrmRespnse = new ArrayList<>();


    ArrayList<String> fileNamesList = new ArrayList<>();

    public ArrayList<Employee> empSelectdFrmRespnse = new ArrayList<>();
    public HandOverEmpAdapter handOverEmpAdapter;


    Util unix = new Util();

    int REQUEST_CAMERA = 100, SELECT_FILE = 101;
    Bitmap bm, converetdImage;
    public ArrayList<SelectImage> bitmapList = new ArrayList<>();

    public AirportAdapter airportAdapter;

    Context context;

    private String TAG = "LeaveReqDetActvity";

    private Toolbar mToolbar;

    String imagePath;
    ArrayList<String> imagePathList = new ArrayList<>();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.leave_req_det_activity);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setTitle("Apply Leave");
        _container = (FrameLayout) findViewById(R.id.dialog_fragment_container);
        context = getApplicationContext();

        Preference pref = new Preference(context, "login");
        CompanyList company = pref.getCompany();

        employeeId = company.employee.empId;



        _statusLayout = (LinearLayout) findViewById(R.id.status_layout);
        _lvStatusImage = (ImageView) findViewById(R.id.leave_status_img);
        _lvStatusTv = (TextView) findViewById(R.id.status);
        _lvCommentTv = (TextView) findViewById(R.id.comment);
        _empHandoverLayout = (CardView) findViewById(R.id.handover_emp_card_view);

        _extPermtLayout = (LinearLayout) findViewById(R.id.ext_permit_layout);
        _tcktReqrdLayout = (LinearLayout) findViewById(R.id.tckt_reqrd_layout);
        _tcktDataLayout = (CardView) findViewById(R.id.tckt_data_layout);
        _flightDataLayout = (CardView) findViewById(R.id.flight_data_layout);
        _leaveCommentLayout= (CardView) findViewById(R.id.leave_comment_layout);
        _leaveStatusLayout= (CardView) findViewById(R.id.leave_status_layout);
        _lvStartDateImage = (ImageView) findViewById(R.id.lv_frm_img);
        _lvStartDateImage.setOnClickListener(this);
        _lvEndDateImage = (ImageView) findViewById(R.id.lv_to_img);
        _lvEndDateImage.setOnClickListener(this);
        _depDateImage = (ImageView) findViewById(R.id.dep_date_img);
        _depDateImage.setOnClickListener(this);
        _retrnDateImage = (ImageView) findViewById(R.id.retrn_date_img);
        _retrnDateImage.setOnClickListener(this);
        _depTimeImage = (ImageView) findViewById(R.id.dep_time_img);
        _depTimeImage.setOnClickListener(this);
        _retTimeImage = (ImageView) findViewById(R.id.retrn_time_img);
        _retTimeImage.setOnClickListener(this);




        _spinnerLvType = (Spinner) findViewById(R.id.leave_type_spinner);
        _spinnerLvType.setOnItemSelectedListener(this);

        _exitPermitSwitch = (Switch) findViewById(R.id.exit_permit_switch);
        _exitPermitSwitch.setOnCheckedChangeListener(this);

        _tcktSwitch = (Switch) findViewById(R.id.tckt_reqrd_switch);
        _tcktSwitch.setOnCheckedChangeListener(this);

        _entitldSpinner = (Spinner) findViewById(R.id.entitled_type_spinner);
        _entitldSpinner.setOnItemSelectedListener(this);


        _lvStartDate = (EditText) findViewById(R.id.lv_start_date);
        _lvStartDate.setOnClickListener(this);
        _lvEndDate = (EditText) findViewById(R.id.lv_end_date);

        _contactNo = (EditText) findViewById(R.id.contact_no);
        _contactEmail = (EditText) findViewById(R.id.contact_email);
        _reason = (EditText) findViewById(R.id.reason);

        _depDate = (EditText) findViewById(R.id.dep_date);
        _depDate.setOnClickListener(this);
        _retrnDate = (EditText) findViewById(R.id.retrn_date);
        _retrnDate.setOnClickListener(this);
        _depTime = (EditText) findViewById(R.id.dep_time);
        _depTime.setOnClickListener(this);
        _retTime = (EditText) findViewById(R.id.retrn_time);
        _retTime.setOnClickListener(this);

        _originAirport = (AutoCompleteTextView) findViewById(R.id.origin_air_autoComText);
        _destnAirport = (AutoCompleteTextView) findViewById(R.id.destn_air_autoComText);

        exit_reqrd = "FALSE";
        tckt_reqrd = "FALSE";
        _lvEndDate.setOnClickListener(this);

        _airClassSpinner = (Spinner) findViewById(R.id.air_class_spinner);
        _wayTypeSpinner = (Spinner) findViewById(R.id.way_type_spinner);
        _noAdultsTv = (EditText) findViewById(R.id.no_adults);
        _noChlrnTv = (EditText) findViewById(R.id.no_childrn);

        _sendLvReqBtn = (FloatingActionButton) findViewById(R.id.fab);
        _sendLvReqBtn.setOnClickListener(this);

// Uncomment later
        _fileUpload = findViewById(R.id.upload_image);
        _selectImageView = (RecyclerView) _fileUpload.findViewById(R.id.images_recycler_view);
        rv_LinearLayoutRvContainer = (LinearLayout) _fileUpload.findViewById(R.id.rv_LinearLayoutRvContainer);
        _attchFile = (ImageView) _fileUpload.findViewById(R.id.attach_file);
        _attchFile.setOnClickListener(this);

        _handOverEmpView = (RecyclerView) findViewById(R.id.handover_emp_recycler_view);


        setUpData();//LOADING THE SETTINGS API

        _lvEndDate.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                // CALLING PAYMENT API TO CHECK OVERLAPPING OF LEAVE REQUEST
                if (_lvEndDate.getText().length() > 1) {
                    loadAlert(2);
                }

            }
        });

        _originAirport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 2) {
                    airprtList = new ArrayList<>();
                    getAirport(charSequence.toString());// CALLING THE AIRPORT API

                }

                if (charSequence.length() > 2) {
                    _originAirport.setThreshold(1);
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {


            }
        });


        _originAirport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int item, long l) {

                _originAirport.setText(airportAdapter.filteredAirport.get(item).getAirportName());
                _originAirport.setTextColor(Color.BLACK);
                orginAirPt = airportAdapter.filteredAirport.get(item).getAirportId();

            }
        });

        _destnAirport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {

                if (charSequence.length() == 2) {

                    airprtList = new ArrayList<>();
                    getAirport(charSequence.toString());// CALLING THE AIRPORT API
                }

                if (charSequence.length() > 2) {
                    _originAirport.setThreshold(1);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        _destnAirport.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int item, long l) {

                AirportData airportData = (AirportData) adapterView.getItemAtPosition(item);
                _destnAirport.setText(airportAdapter.filteredAirport.get(item).getAirportName());
                _destnAirport.setTextColor(Color.BLACK);
                destAirPt = airportAdapter.filteredAirport.get(item).getAirportId();

            }
        });

        //STATIC ENTITLED
        Entitled etType = new Entitled();
        etType.setEntitld_id("0");
        etType.setEntitld_name("Self");
        entitldNames.add("Self");
        entitldTypes.add(etType);

        etType = new Entitled();
        etType.setEntitld_id("1");
        etType.setEntitld_name("Self + Family");
        entitldNames.add("Self + Family");
        entitldTypes.add(etType);

        _entitldSpinner.setAdapter(new ArrayAdapter<String>(LeaveReqDetActivity.this, android.R.layout.simple_spinner_dropdown_item, entitldNames));
        _entitldSpinner.setOnItemSelectedListener(null);

        airclassList.add("Economy Class");
        airclassList.add("Business Class");
        _airClassSpinner.setAdapter(new ArrayAdapter<String>(LeaveReqDetActivity.this, android.R.layout.simple_spinner_dropdown_item, airclassList));
        _entitldSpinner.setOnItemSelectedListener(null);

        wayTypeList.add("One Way");
        wayTypeList.add("Round way");
        _wayTypeSpinner.setAdapter(new ArrayAdapter<String>(LeaveReqDetActivity.this, android.R.layout.simple_spinner_dropdown_item, wayTypeList));
        _entitldSpinner.setOnItemSelectedListener(null);



    }



    //SETTINGS FUNCTION CHECKING THE OBJECT
    public void setUpData() {
        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        if (bundle != null) {
            leave = (LeaveRequestList) bundle.getSerializable("LEAVE");


            if (leave != null) {

                _leaveStatusLayout.setVisibility(View.VISIBLE);
                switch (leave.getLeave_status()){
                    case "0":
                        _lvStatusImage.setColorFilter(getResources().getColor(R.color.colorYellow));
                        _lvStatusTv.setText(Constants.LeaveStatus.PENDING);
                        _lvStatusTv.setTextColor(getResources().getColor(R.color.colorYellow));
                        _leaveCommentLayout.setVisibility(View.GONE);

                        break;

                    case "1":
                        _lvStatusImage.setColorFilter(getResources().getColor(R.color.colorGreen));
                        _lvStatusTv.setText(Constants.LeaveStatus.APPROVED);
                        _lvStatusTv.setTextColor(getResources().getColor(R.color.colorGreen));

                        if (!leave.getApproval_comments().isEmpty()){
                            _leaveCommentLayout.setVisibility(View.VISIBLE);
                            _lvCommentTv.setText(leave.getApproval_comments());
                        }


                        //DISPLAYING HANDOVER EMPLOYEEES... NEED TO ADD HEREEE

                        if(leave.getReplaceRequird().equals("2")){
                            _empHandoverLayout.setVisibility(View.VISIBLE);
                            ArrayList<HashMap<Integer,Employee>> selectdEmpRespnsArry=leave.employee.getSelectdListFrmResponse();

                            for (int j =0; j<selectdEmpRespnsArry.size(); j++){

                                empSelectdFrmRespnse.add(selectdEmpRespnsArry.get(j).get(j));

                            }

                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                            _handOverEmpView.setLayoutManager(mLayoutManager);
                            _handOverEmpView.setItemAnimator(new DefaultItemAnimator());
                            handOverEmpAdapter = new HandOverEmpAdapter(empSelectdFrmRespnse, new HandOverEmpAdapter.OnItemClickListener() {
                                @Override
                                public void onItemClick(View itemView, int position) {

                                    Log.d("ITEM ", "CLICK");
                                    FragmentManager fm = getFragmentManager();
                                    CustomDialogFragment dialogFragment = new CustomDialogFragment();
                                    Bundle args = new Bundle();
                                    args.putString("EMPLOYEE_NAME", empSelectdFrmRespnse.get(position).empName);
                                    dialogFragment.setArguments(args);
                                    dialogFragment.show(fm, "Sample Fragment");
                                }
                            });
                            _handOverEmpView.setAdapter(handOverEmpAdapter);
                            handOverEmpAdapter.notifyDataSetChanged();

                        }else {
                            _empHandoverLayout.setVisibility(View.GONE);

                        }


                        break;

                    case "2":
                        _lvStatusImage.setColorFilter(getResources().getColor(R.color.colorRed));
                        _lvStatusTv.setText(Constants.LeaveStatus.REJECTED);
                        _lvStatusTv.setTextColor(getResources().getColor(R.color.colorRed));
                        if (!leave.getRejected_reason().isEmpty()){
                            _leaveCommentLayout.setVisibility(View.VISIBLE);
                            _lvCommentTv.setText(leave.getRejected_reason());
                        }
                        break;

                    case "3":
                        _lvStatusImage.setColorFilter(getResources().getColor(R.color.colorOrange));
                        _lvStatusTv.setText(Constants.LeaveStatus.CANCELLED);
                        _lvStatusTv.setTextColor(getResources().getColor(R.color.colorOrange));
                        if (!leave.getCancelled_reason().isEmpty()){
                            _leaveCommentLayout.setVisibility(View.VISIBLE);
                            _lvCommentTv.setText(leave.getCancelled_reason());
                        }
                        break;




                }

                _lvStartDate.setText(Util.getFormattedDate(leave.getLeave_frm()));
                lvFromDate = Util.getTimeStampFromDate(Util.getFormattedDateFromString(leave.getLeave_frm()));
                _lvStartDate.setOnClickListener(null);
                _lvStartDateImage.setOnClickListener(null);

                _lvEndDate.setText(Util.getFormattedDate(leave.getLeave_to()));
                lvToDate = Util.getTimeStampFromDate(Util.getFormattedDateFromString(leave.getLeave_to()));

                _lvEndDate.setOnClickListener(null);
                _lvEndDateImage.setOnClickListener(null);

                _contactNo.setText(leave.getLeave_contact());
                _contactNo.setFocusable(false);

                _contactEmail.setText(leave.getLeave_email());
                _contactEmail.setFocusable(false);

                _reason.setText(leave.getLeave_resn());
                _reason.setFocusable(false);
                _sendLvReqBtn.setVisibility(View.GONE);

                selectedLv = new LeaveType();
                selectedLv._lvId = leave.getLeave_type_id();

                leaveNames.clear();
                leaveNames.add(leave.getLeave_type());

                LeaveType leaveType = new LeaveType();
                leaveType.set_lvId(leave.getLeave_type_id());
                leaveType.set_lvType(leave.getLeave_type());
                leaveTypes.add(leaveType);

                if (leave.getLeave_exit_permit() != null) {
                    if (leave.getLeave_exit_permit().equals("true")) {
                        _extPermtLayout.setVisibility(View.VISIBLE);
                        _exitPermitSwitch.setChecked(true);
                        _exitPermitSwitch.setEnabled(false);

                        _depDate.setFocusable(false);
                        _depDate.setOnClickListener(null);
                        _depDateImage.setOnClickListener(null);

                        _depDate.setText(leave.getLeave_depature_date());

                        _retrnDate.setFocusable(false);
                        _retrnDate.setOnClickListener(null);
                        _retrnDateImage.setOnClickListener(null);

                        _retrnDate.setText(leave.getLeave_return_date());

                        _depTime.setFocusable(false);
                        _depTime.setOnClickListener(null);
                        _depDateImage.setOnClickListener(null);

                        _depTime.setText(leave.getLeave_depature_time());


                        _retTime.setFocusable(false);
                        _retTime.setOnClickListener(null);
                        _retrnDate.setOnClickListener(null);

                        _retTime.setText(leave.getLeave_return_time());
                    }
                }

                if (leave.getLeave_tickets() != null) {
                    if (leave.getLeave_tickets().equals("true")) {
                        _tcktReqrdLayout.setVisibility(View.VISIBLE);
                        _tcktSwitch.setChecked(true);
                        _tcktSwitch.setEnabled(false);

                        _originAirport.setFocusable(false);
                        _destnAirport.setFocusable(false);

                        _originAirport.setText(leave.getOrigin());
                        _destnAirport.setText(leave.getDestination());

                        //_airClassTv.setText(leave.getAir_class()); // TEMPORARY
                       // _wayTypeTv.setText(leave.getWay_type()); // TEMPORARY

                        _noAdultsTv.setText(leave.getNumber_of_adults());
                        _noAdultsTv.setFocusable(false);
                        _noChlrnTv.setText(leave.getNumber_of_childrens());
                        _noChlrnTv.setFocusable(false);
                    }
                }

                _spinnerLvType.setAdapter(new ArrayAdapter<String>(LeaveReqDetActivity.this, android.R.layout.simple_spinner_dropdown_item, leaveNames));
                _spinnerLvType.setOnItemSelectedListener(null);




                //FILES FROM RESPONSE
                if(leave.fileData.getFileListFrmResponse()!=null){

                    ArrayList<HashMap<Integer,FileData>> fileMapListFrmResponse=leave.fileData.getFileListFrmResponse();

                    for (int j =0; j<fileMapListFrmResponse.size(); j++){

                        fileFrmRespnse.add(fileMapListFrmResponse.get(j).get(j));

                    }


                    Log.d("LIST SIZEEE ", String.valueOf(leave.fileData.getFileListFrmResponse().size()));




                    _fileUpload.setVisibility(View.VISIBLE);
                    _attchFile.setVisibility(View.GONE);
                    rv_LinearLayoutRvContainer.setVisibility(View.VISIBLE);
                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
                    _selectImageView.setLayoutManager(mLayoutManager);
                    _selectImageView.setItemAnimator(new DefaultItemAnimator());

                    Log.d("HELLLL ", String.valueOf(fileFrmRespnse.size()));
                    displayImageAdapter = new DisplayImageAdapter(fileFrmRespnse, new DisplayImageAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(View itemView, int position) {

                            Intent galleryIntent = new Intent(LeaveReqDetActivity.this, GalleryActivity.class);
                            galleryIntent.putExtra("SELECTED_IMAGE_POS", position);
                            galleryIntent.putExtra("IMAGES_ARRAY", fileFrmRespnse);
                            startActivity(galleryIntent);
                        }
                    });
                    _selectImageView.setAdapter(displayImageAdapter);

//                    _fileDataRecyclrView.setAdapter(displayImageAdapter);
//                    displayImageAdapter.notifyDataSetChanged();
                }

            }
        } else {
            _statusLayout.setVisibility(View.GONE);

            loadData(); //LOAD THE SETTINGS
            _attchFile.setVisibility(View.VISIBLE);

            LeaveType defaultLeaveType = new LeaveType();
            defaultLeaveType.set_lvId("-1");
            defaultLeaveType.set_lvType("Please Select");
            defaultLeaveType.setRequires_certificate("0");
            defaultLeaveType.setMin_period("0");
            leaveTypes.add(defaultLeaveType);
            leaveNames.add("Please Select");
            _spinnerLvType.setAdapter(new ArrayAdapter<String>(LeaveReqDetActivity.this, android.R.layout.simple_spinner_dropdown_item, leaveNames));

        }

    }

    //LOADING LEAVE REQ SETTING API
    public void loadData() {

        req_url = Constants.CREATE_LEAVE_REQ_URL_SETTINGS + employeeId;

        progressDialog = new ProgressDialog(LeaveReqDetActivity.this);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        WebManager webManager = new WebManager(context);
        webManager.LeavReq(req_url, new WebManager.VolleyCallback() {
            @Override
            public void onSuccess(String result) {

                progressDialog.dismiss();
                Log.d("SETTINGS API RESPONSE ", result);
                try {
                    JSONObject json = new JSONObject(result);
                    jResult = json.getString("result");
                    jMessage = json.getString("message");
                    if (jResult.equals("1")) {
                        JSONObject jsonObject = json.getJSONObject("data");
                        JSONArray jsonLvType = jsonObject.getJSONArray("leaveTypes");

                        for (int i = 0; i < jsonLvType.length(); i++) {
                            JSONObject jObj = jsonLvType.getJSONObject(i);
                            LeaveType leaveType = new LeaveType();
                            leaveType.set_lvId(jObj.getString("id"));
                            leaveType.set_lvType(jObj.getString("name"));
                            leaveType.setRequires_certificate(jObj.getString("requires_certificate"));
                            leaveType.setMin_period(jObj.getString("min_period"));

                            leaveTypes.add(leaveType);
                            leaveNames.add(jObj.getString("name"));
                        }
                        _spinnerLvType.setAdapter(new ArrayAdapter<String>(LeaveReqDetActivity.this, android.R.layout.simple_spinner_dropdown_item, leaveNames));

                        jExtPrmt = jsonObject.getBoolean("exitPermit");
                        if (jExtPrmt) {
                            _extPermtLayout.setVisibility(View.VISIBLE);
                        } else {
                            _extPermtLayout.setVisibility(View.GONE);
                        }

                        jTcktStatus = jsonObject.getBoolean("leaveTicket");

                        if (jTcktStatus) {
                            JSONObject tckt = jsonObject.getJSONObject("ticketDetails");
                            //_airClassTv.setText(tckt.getString("air_class_type")); // TEMPORARY
                            //_wayTypeTv.setText(tckt.getString("way_type")); //TEMPORARY
                            _noAdultsTv.setText(tckt.getString("no_ticket_adult"));
                            _noChlrnTv.setText(tckt.getString("no_ticket_children"));
                            _tcktReqrdLayout.setVisibility(View.VISIBLE);
                        } else {
                            _tcktReqrdLayout.setVisibility(View.GONE);
                        }


                    } else {

                        displayAlert(jMessage);
                    }


                } catch (JSONException e) {
                    progressDialog.dismiss();


                    displayAlert(e.getMessage());

                }
                progressDialog.dismiss();
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                Log.d("ERROR ", error.toString());

                displayAlert(error);

            }

        });
    }


    //LOAD ALERT API
    public int loadAlert(final int flag) {
        paymentList = new ArrayList<>();
        if (!selectedLv._lvId.equals("-1")) {
            if (lvFromDate > 0) {
                if (lvToDate > 0) {
                    progressDialog = new ProgressDialog(LeaveReqDetActivity.this);
                    progressDialog.setTitle("Processing...");
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    alert_url = Constants.LEAVE_ALERT_DIALOG_URL + employeeId + "/type/" + selectedLv._lvId + "/from/" + lvFromDate + "/to/" + lvToDate;
                    Log.e("ALERT URL ", alert_url);

                    WebManager webManager = new WebManager(context);
                    webManager.LeavAlert(alert_url, new WebManager.VolleyCallback() {

                        @Override
                        public void onSuccess(String result) {
                            Log.d("ALERT RESPONSE ", result);
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                jResult = jsonObject.getString("result");
                                jMessage = jsonObject.getString("message");
                                if (jResult.equals("1")) {
                                    JSONObject jdata = jsonObject.getJSONObject("data");
                                    jOvrLaping = jdata.getBoolean("overlapping");
                                    jElegbity = jdata.getBoolean("eligible");
                                    jPayItems = jdata.getBoolean("payItems");
                                    //PAY ITEMS PARSING

                                    // DISPLAYING OVERLAPPING ONLY WHILE CREATING LEAVE REQUEST
                                    if (jOvrLaping && leave == null) {
                                        JSONObject jOverLappingDetails = jdata.getJSONObject("overlapping_details");
                                        displayAlert(jOverLappingDetails.getString("message"));
                                        _lvStartDate.setText("");
                                        _lvEndDate.setText("");
//                                        lvFromDate = 0;
//                                        lvToDate = 0;
//                                        eYear = 0;
                                    }

                                    if (jPayItems) {
                                        JSONObject jPaydet = jdata.getJSONObject("paymentDetails");
                                        JSONArray jItemArry = jPaydet.getJSONArray("items");


                                        for (int i = 0; i < jItemArry.length(); i++) {
                                            JSONObject jObj = jItemArry.getJSONObject(i);
                                            ViewPaymentDialog paymentDialog = new ViewPaymentDialog();
                                            paymentDialog.setAccntName(jObj.getString("name"));
                                            paymentDialog.setAmount(jObj.getString("amount"));
                                            paymentList.add(paymentDialog);
                                        }

                                        jTotPayment = jPaydet.getString("totalPayment");
                                    }

                                    //ELEGIBILITY PARSING
                                    if (jElegbity) {
                                        JSONObject jElegDet = jdata.getJSONObject("eligibility_details");
                                        jEntitld = jElegDet.getString("entitled");
                                        jTotDays = jElegDet.getString("totalNoDays");
                                        JSONObject jCurrnt = jElegDet.getJSONObject("current");
                                        jPaid = jCurrnt.getString("paid");
                                        jUnpaid = jCurrnt.getString("unpaid");
                                        jAftrApply = jElegDet.getString("afterApply");
                                        jBfrApply = jElegDet.getString("beforeApply");
                                    }


                                    if (flag == 0) {

                                        dispayPaymentInfo();

                                    } else if (flag == 1) {

                                        dispayEligibilityInfo();
                                    }

                                } else {
                                    JSONObject jData = jsonObject.getJSONObject("data");
                                    _lvStartDate.setText("");
                                    _lvEndDate.setText("");
                                    jMessage = jData.getString("message");
                                    displayAlert(jMessage);


                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(String result) {
                            progressDialog.dismiss();
                            try {
                                JSONObject jsonObject = new JSONObject(result);
                                Log.d("ERROR ", jsonObject.toString());

                                JSONObject jRsnObj = jsonObject.getJSONObject("reason");
                                displayAlert(jRsnObj.getString("message"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });
                } else {
                    displayAlert("End Date should not be empty");
                }
            } else {

                displayAlert(" From Date should not be empty");
            }
        } else {
            displayAlert("Please select the leave type");
        }

        if ((jPayItems != null) && (jElegbity != null)) {
            return 1;
        } else {
            return 0;
        }
    }

    //LOAD AIRPORT API
    public void getAirport(final String airprtName) {

        String airportUrl = Constants.AIRPORT_DETAILS_URL + airprtName;
        WebManager webManager = new WebManager(context);
        webManager.AirportData(airportUrl, new WebManager.VolleyCallback() {

            @Override
            public void onSuccess(String result) {
                Log.d("AIRPORT DATA SUCC", result);

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    jResult = jsonObject.getString("result");
                    jMessage = jsonObject.getString("message");
                    if (jResult.equals("1")) {

                        JSONArray jAirportData = jsonObject.getJSONArray("data");
                        for (int i = 0; i < jAirportData.length(); i++) {
                            JSONObject jAirpt = jAirportData.getJSONObject(i);
                            AirportData airportData = new AirportData();
                            airportData.setAirportId(jAirpt.getString("id"));
                            airportData.setAirportName(jAirpt.getString("airport"));
                            airportData.setAirportCode(jAirpt.getString("code"));
                            airportData.setAirportActvStats(jAirpt.getString("active_status"));
                            airprtList.add(airportData);
                            airportName.add(jAirpt.getString("airport"));
                        }
                        Log.d("AIRPORT NAME ", "" + airportName);
                        Log.d("AIRPORT LIST ", "" + airprtList);
                        airportAdapter = new AirportAdapter(getApplicationContext(), airprtList);
                        _originAirport.setAdapter(airportAdapter);
                        airportAdapter.notifyDataSetChanged();

                        //_originAirport.setThreshold(1);

                        _destnAirport.setAdapter(airportAdapter);
                        _destnAirport.setThreshold(1);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String result) {

            }
        });

    }

    // GENERIC DIALOG BOX
    public void displayAlert(String message) {

        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LeaveReqDetActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(message);
        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

    }

    public void dispayPaymentInfo() {
        if (jPayItems) {

            MaterialDialog dialog = new MaterialDialog.Builder(LeaveReqDetActivity.this)
                    .title("Pay Items")
                    .customView(R.layout.view_payment_dialog_layout, true)
                    .positiveText("Dismiss")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            //showToast("Password: " + passwordInput.getText().toString());
                        }

                    }).build();

            _recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclr_view_paymnt);

            mAdapter = new ViewPaymntDialogAdptr(paymentList);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
            _recyclerView.setLayoutManager(mLayoutManager);
            _recyclerView.setItemAnimator(new DefaultItemAnimator());
            _recyclerView.setAdapter(mAdapter);

            _totPayTv = (TextView) dialog.findViewById(R.id.tot_pay);
            _totPayTv.setText(jTotPayment);

            dialog.show();
        } else {

            displayAlert("Pay items are not available.");

        }
    }

    public void dispayEligibilityInfo() {
        if (jElegbity) {

            MaterialDialog dialog = new MaterialDialog.Builder(LeaveReqDetActivity.this)
                    .title("Eligibility Info")
                    .customView(R.layout.elegibility_dialog, true)
                    .positiveText("Dismiss")
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                            //showToast("Password: " + passwordInput.getText().toString());
                        }

                    }).build();


            _entitldTv = (TextView) dialog.findViewById(R.id.entitld_id);
            _entitldTv.setText(jEntitld + " days");
            _totDaysTv = (TextView) dialog.findViewById(R.id.tot_days);
            _totDaysTv.setText(jTotDays + " days");
            _paidTv = (TextView) dialog.findViewById(R.id.paid_id);
            _paidTv.setText(jPaid + " days");
            _unpaidTv = (TextView) dialog.findViewById(R.id.unpaid_id);
            _unpaidTv.setText(jUnpaid + " days");
            _aftrAplyTv = (TextView) dialog.findViewById(R.id.aftr_apply_id);
            _aftrAplyTv.setText(jAftrApply + " days");
            _bfrAplyTv = (TextView) dialog.findViewById(R.id.bfr_apply_id);
            _bfrAplyTv.setText(jBfrApply + " days");

            dialog.show();

        } else {

            displayAlert("There is no information.");

        }
    }

    //UPLOAD IMAGE
    public void uploadImage(){

        fileNamesList = new ArrayList<>();
        new AsyncTask<Void, Integer, Boolean>() {

            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressDialog = new ProgressDialog(LeaveReqDetActivity.this);
                progressDialog.setMessage("UPLOADING PLEASE WAIT");
                progressDialog.show();
            }

            @Override
            protected Boolean doInBackground(Void... params) {

                JSONObject jsonObject = File_uploading_JSONParser.uploadImages(context, bitmapList);
                if (jsonObject != null) {
                    Iterator<String> iter = jsonObject.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        Log.d("KEYYY ", key);
                        try {
                            String fileNames = String.valueOf(jsonObject.get(key));
                            fileNamesList.add(fileNames);

                        } catch (JSONException e) {
                            // Something went wrong!
                            Log.i("TAG", "Error : " + e.getLocalizedMessage());

                        }
                    }

                    if (fileNamesList.size() > 0) {
                        return true;

                    } else {
                        return false;
                    }
                }


                return false;

            }

            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);
                if (progressDialog != null)
                    progressDialog.dismiss();

                if (aBoolean){
                    //Toast.makeText(getApplicationContext(), "SUCESS", Toast.LENGTH_LONG).show();
                    sendReq();
                }

                else
                    Toast.makeText(getApplicationContext(), "timeout", Toast.LENGTH_LONG).show();

                imagePath = "";

            }
        }.execute();

    }


    //SEND LEAVE REQ
    public void validation() {
        contactNo = _contactNo.getText().toString();
        contactEmail = _contactEmail.getText().toString().trim();
        reason = _reason.getText().toString();

        Log.d("KILI POOI ", contactEmail);
        Log.d("BUBHU ", "HAHA");

        Boolean validEmail = isValidEmail(contactEmail);
        Boolean validPhone = isValidMobile(contactNo);

        if (lvFromDate > 0 && lvToDate > 0) {

            if (validPhone) {

                if (validEmail) {

                    if (reason.length() > 0) {

                        if(selectedLv.getRequires_certificate().equals("0")){

                            conformationAlert();

                        }else {
                            if(Integer.parseInt(selectedLv.getMin_period()) > days && bitmapList.size() > 0 ){

                                conformationAlert();
                            }else {
                                displayAlert("Please upload file");
                            }
                        }

                    } else {

                        displayAlert("Reason should not be empty");
                    }

                } else {

                    displayAlert("Please enter valid email");

                }

            } else {

                displayAlert("Please enter valid phone number");

            }
        } else {

            displayAlert("Please enter the valid From Date and To Date");

        }
    }


    //CONFORMATION ALERT
    public void conformationAlert(){

        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LeaveReqDetActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure want to apply leave from "+ _lvStartDate.getText().toString()+" to "+_lvEndDate.getText().toString() +" ?");
        alertDialog.setButton(android.app.AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                dialog.dismiss();

                if(bitmapList.size()>0){

                    uploadImage();// UPLOADING THE IMAGE
                }else {
                    sendReq();
                }
            }
        });
        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEGATIVE, "CANCEL",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }

    //SENDING LEAVE
    public void sendReq(){

        progressDialog = new ProgressDialog(LeaveReqDetActivity.this);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        create_lv_url = Constants.CREATE_LEAVE_URL + employeeId;


        WebManager webManager = new WebManager(context);
        webManager.addParams("leave_type_id", selectedLv._lvId);
        webManager.addParams("leave_from", Util.convertTimeStampToDate(lvFromDate));
        webManager.addParams("leave_to", Util.convertTimeStampToDate(lvToDate));
        webManager.addParams("contact_on_leave", contactNo);
        webManager.addParams("contact_email", contactEmail);
        webManager.addParams("leave_reason", reason);

        if (exit_reqrd.equals("TRUE")) {
            webManager.addParams("exit_required", "true");
            webManager.addParams("departure_date", depDate);
            webManager.addParams("departure_time", depTime);
            webManager.addParams("return_date", retDate);
            webManager.addParams("return_time", retTime);
        }
        if (tckt_reqrd.equals("TRUE")) {
            webManager.addParams("ticket", "true");
            webManager.addParams("origin", orginAirPt);
            webManager.addParams("destination", destAirPt);
            webManager.addParams("family_ticket", entitldFor);
            // webManager.addParams("air_class", _airClassTv.getText().toString()); // TEMPORARY
            // webManager.addParams("way_type", _wayTypeTv.getText().toString()); // TEMPORARY
            webManager.addParams("no_ticket_adult", _noAdultsTv.getText().toString());
            webManager.addParams("no_ticket_children", _noChlrnTv.getText().toString());

        }
        if(fileNamesList.size()>0){

            JSONArray jsonArray = new JSONArray(fileNamesList);
            webManager.addParams("file_name", jsonArray);
        }
        webManager.sendLvReq(create_lv_url, new WebManager.VolleyCallback() {

            @Override
            public void onSuccess(String result) {
                progressDialog.dismiss();
                try {

                    Log.d("SUCESS SEND REQ ", result);

                    JSONObject jsonObject = new JSONObject(result);
                    jResult = jsonObject.getString("result");
                    //jMessage = jsonObject.getString("message");
                    if (jResult.equals("1")) {

                        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LeaveReqDetActivity.this).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Leave Applied Successfully.");
                        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                        goBack();
                                    }
                                });
                        alertDialog.show();
                    } else {
                        JSONObject jData = jsonObject.getJSONObject("data");
                        JSONObject jOverLappingDetails = jData.getJSONObject("overlapping_details");
                        displayAlert(jOverLappingDetails.getString("message"));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String result) {

                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    Log.d("ERROR resp ", jsonObject.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    //SELECTING THE IMAGE FOR UPLOAD DIALOG
    private void selectImage() {
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                boolean result = PermissionUtility.checkPermission(LeaveReqDetActivity.this);
                if (items[item].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    if (result)
                        cameraIntent();
                    } else if (items[item].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    if (result)
                        galleryIntent();
                    } else if (items[item].equals("Cancel")) {
                        dialog.dismiss();
                    }
                }
            });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }
    private void galleryIntent() {

        final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_PICK);

        Intent chooserIntent = Intent.createChooser(galleryIntent, "PICK IMAGE");
        startActivityForResult(chooserIntent, SELECT_FILE);
    }


    //Krishna Code
    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String fileName = getFilename();
        File destination = new File(fileName);
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            SelectImage selectImage = new SelectImage();
            selectImage.setBitmap(thumbnail);
            selectImage.setImagePath(fileName);
            updateImageArray("ADD",selectImage);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static String getFilename() {
        File file = new File(Constants.LOCAL_IMAGE_BASE_PATH);
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + CommonUtils.getUUID() + ".jpg");
        Log.v("uriSting", uriSting);
        return uriSting;

    }



    //SELECTING THE IMAGE FROM GALLERY
    private void onSelectFromGalleryResult(Intent data) {

        Uri selectedImageUri = data.getData();
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(selectedImageUri, filePathColumn, null, null, null);



        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imagePath = cursor.getString(columnIndex);
            imagePathList.add(imagePath);
            Log.d("imagePath COLUMN ", String.valueOf(columnIndex));
            cursor.close();


            if (data != null) {
                try {
                    bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), data.getData());
                    converetdImage =Bitmap.createScaledBitmap(bm, 300, 300, true);;

                    SelectImage selectImage = new SelectImage();
                    selectImage.setBitmap(converetdImage);
                    selectImage.setImagePath(imagePath);
                    updateImageArray("ADD",selectImage);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }


        } else {

        }
//        bm = null;


    }

    public void updateImageArray(String action, SelectImage selectImage){


        if(action.equals("ADD")){
            bitmapList.add(selectImage);

        }else {
            bitmapList.remove(selectImage);

        }

        if(bitmapList.size()>0){
            rv_LinearLayoutRvContainer.setVisibility(View.VISIBLE);
        }else {
            rv_LinearLayoutRvContainer.setVisibility(View.GONE);
        }


        _selectImageView.setVisibility(View.VISIBLE);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        _selectImageView.setLayoutManager(mLayoutManager);
        _selectImageView.setItemAnimator(new DefaultItemAnimator());
        _selectImageView.setAdapter(new SelectImageAdapter(bitmapList, LeaveReqDetActivity.this, null));

    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE){
                onSelectFromGalleryResult(data);
            }
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

//        if (selectedLv !=null) {
//            if (selectedLv.get_lvType().equals("UnPaid Leave")) {
//                menu.getItem(0).setVisible(false);
//            }
//        }
        getMenuInflater().inflate(R.menu.lv_req_optns, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        int result = 0;
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        Log.d("MENU ", "CLICKEd");

        if (id == R.id.action_view_payment) {

            loadAlert(0);

        } else if (id == R.id.action_eleg_info) {
            loadAlert(1);
        } else if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        sYear = c.get(Calendar.YEAR);
        sMonth = c.get(Calendar.MONTH);
        sDay = c.get(Calendar.DAY_OF_MONTH);


        switch (view.getId()) {

            case R.id.lv_start_date:
            case R.id.lv_frm_img: {

                if (eYear != 0) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    lvFromDate = unix.Unix(year, monthOfYear, dayOfMonth, 00, 00);
                                    String test = Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth);

                                    startDate = Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth);
                                    Log.d("TESTTTTTTT ", test);
                                    _lvStartDate.setText(Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth));


                                    eYear = year;
                                    eMonth = monthOfYear;
                                    eDay = dayOfMonth;
                                }
                            }, eYear, eMonth, eDay);


                    datePickerDialog.show();
                }else {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    lvFromDate = unix.Unix(year, monthOfYear, dayOfMonth, 00, 00);
                                    _lvStartDate.setText(Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth));

                                    startDate = Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth);

                                    eYear = year;
                                    eMonth = monthOfYear;
                                    eDay = dayOfMonth;
                                }
                            }, sYear, sMonth, sDay);
                    datePickerDialog.show();
                }

            }
            break;

            case R.id.lv_end_date:
            case R.id.lv_to_img: {

                if (eYear != 0) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    validateDateField(view, year, monthOfYear, dayOfMonth);

                                    endDate = Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth);
                                    if(startDate!=null){
                                        days = Util.getDays(startDate, endDate);
                                    }

                                    eYear = year;
                                    eMonth = monthOfYear;
                                    eDay = dayOfMonth;
                                }
                            }, eYear, eMonth, eDay);

                    datePickerDialog.show();
                } else {
                    displayAlert("Please select Leave start date!");
                }
            }

            break;

            case R.id.dep_date:
            case R.id.dep_date_img: {

                if(eYear != 0){
                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    depDate = unix.Unix(year, monthOfYear, dayOfMonth, 00, 00);
                                    _depDate.setText(Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth));

                                    eYear = year;
                                    eMonth = monthOfYear;
                                    eDay = dayOfMonth;

                                }
                            }, eYear, eMonth, eDay);
                    datePickerDialog.show();
                }else {

                    DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {

                                    depDate = unix.Unix(year, monthOfYear, dayOfMonth, 00, 00);
                                    _depDate.setText(Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth));

                                    eYear = year;
                                    eMonth = monthOfYear;
                                    eDay = dayOfMonth;

                                }
                            }, eYear, eMonth, eDay);
                    datePickerDialog.show();

                }
            }
            break;

            case R.id.retrn_date:
            case R.id.retrn_date_img: {
                DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                retDate = unix.Unix(year, monthOfYear, dayOfMonth, 00, 00);
                                _retrnDate.setText(Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth));

                                eYear = year;
                                eMonth = monthOfYear;
                                eDay = dayOfMonth;
                            }
                        }, eYear, eMonth, eDay);
                datePickerDialog.show();
            }
            break;

            case R.id.dep_time:
            case R.id.dep_time_img: {
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                depTime = String.valueOf(hourOfDay + ":" + minute + ":" + "00");
                                _depTime.setText(hourOfDay + ":" + minute);

                                Log.d("MYYY ", String.valueOf(hourOfDay + ":" + minute + ":" + "00"));
                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
            break;


            case R.id.retrn_time:
            case R.id.retrn_time_img: {
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                        new TimePickerDialog.OnTimeSetListener() {

                            @Override
                            public void onTimeSet(TimePicker view, int hourOfDay,
                                                  int minute) {

                                retTime = String.valueOf(hourOfDay + ":" + minute + ":" + "00");
                                _retTime.setText(hourOfDay + ":" + minute);
                                Log.d("MYYY ", String.valueOf(hourOfDay + ":" + minute + ":" + "00"));

                            }
                        }, mHour, mMinute, false);
                timePickerDialog.show();
            }
            break;

            case R.id.fab:

                //uploadImage();

                validation();



                break;

            case R.id.attach_file:

                //showFileChooser();


                selectImage();

                break;
        }

    }

    //EMAIL VALIDATION
    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    //PHONE VALIDATION
    private boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }

    private void validateDateField(DatePicker view, int year,
                                   int monthOfYear, int dayOfMonth) {
        lvToDate = unix.Unix(year, monthOfYear, dayOfMonth, 00, 00);

        if (lvToDate >= lvFromDate) {
            Log.d("CORRECT ", "CORREDT");
            _lvEndDate.setText(Util.getDateStringFromDatePicker(year, monthOfYear, dayOfMonth));
            ;
        } else {

            //_lvEndDate.setText("");
            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(LeaveReqDetActivity.this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("End Date must be greater than the Start Date");
            alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            _lvEndDate.setText("");

                        }
                    });
            alertDialog.show();

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int item, long l) {

        Log.d("SELECTED ID ", leaveTypes.get(item).get_lvType());
        Spinner spinner = (Spinner) adapterView;
        if (spinner.getId() == R.id.leave_type_spinner) {
            selectedLv = leaveTypes.get(item);

            if(selectedLv.getRequires_certificate().equals("1")){
                _fileUpload.setVisibility(View.VISIBLE);
            }else {
                _fileUpload.setVisibility(View.GONE);
            }

        }

        if (spinner.getId() == R.id.entitled_type_spinner) {

            entitldFor = entitldTypes.get(item).entitld_id;
            Log.d("ENTITLED FOR ", entitldFor);
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        switch (compoundButton.getId()) {

            case R.id.exit_permit_switch:
                if (isChecked) {
                    _tcktDataLayout.setVisibility(View.VISIBLE);
                    exit_reqrd = "TRUE";
                } else {
                    _tcktDataLayout.setVisibility(View.GONE);
                    exit_reqrd = "FALSE";
                }
                break;

            case R.id.tckt_reqrd_switch:
                if (isChecked) {
                    _flightDataLayout.setVisibility(View.VISIBLE);
                    tckt_reqrd = "TRUE";
                } else {
                    _flightDataLayout.setVisibility(View.GONE);
                    tckt_reqrd = "FALSE";
                }
                break;
        }
    }


    public void goBack() {
        Intent i = new Intent(this, HomeScreenActivity.class);
        startActivityForResult(i, 1);
    }

}

