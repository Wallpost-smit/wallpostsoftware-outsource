package com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter;

import android.content.Context;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Extras.FileUtils;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.RequestApprovalFragment;
import com.SMIT.WallPostSoftware.R;
import com.SMIT.WallPostSoftware.common.ApprovDialog;
import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by alex on 21/09/16.
 */
public class LeaveApprvListAdaptr extends RecyclerView.Adapter<LeaveApprvListAdaptr.LeavApprvListViewHolder> {

    private List<LeaveRequestList> lvApprvList;
    private ArrayList<Employee> empHandoverList;
    public String status, lvId, comments;
    public MaterialDialog dialog;

    Context context;
    EditText _resn;
    private RequestApprovalFragment requestApprovalFragment;

    public ApprovDialog approvDialogObj;

    // Define listener member variable
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);

    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }



    public class LeavApprvListViewHolder extends RecyclerView.ViewHolder {
        public TextView _nameTv, _lvTypTv, _lvFrmTv, _lvToTv;
        public ImageButton _menuButtn, _cancelld, _apprvd;
        public CircleImageView _profileImage;


        public LeavApprvListViewHolder(final View itemView) {
            super(itemView);

            _nameTv = (TextView) itemView.findViewById(R.id.name);
            _lvTypTv = (TextView) itemView.findViewById(R.id.lv_type);
            _lvFrmTv = (TextView) itemView.findViewById(R.id.leave_frm);
            _lvToTv = (TextView) itemView.findViewById(R.id.leave_to);


            _menuButtn = (ImageButton) itemView.findViewById(R.id.sub_menu);
            _cancelld = (ImageButton) itemView.findViewById(R.id.cancelled);
            _apprvd = (ImageButton) itemView.findViewById(R.id.approved);
            _profileImage = (CircleImageView) itemView.findViewById(R.id.profile_image);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });
        }
    }

    public LeaveApprvListAdaptr(Context context, List<LeaveRequestList> lvApprvList, ArrayList<Employee> empHandoverList, RequestApprovalFragment requestApprovalFragment, OnItemClickListener listener) {
        this.context = context;
        this.lvApprvList = lvApprvList;
        this.empHandoverList = empHandoverList;
        this.requestApprovalFragment = requestApprovalFragment;
        this.listener = listener;
    }


    @Override
    public LeavApprvListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.apprv_lv_row, parent, false);

        return new LeavApprvListViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LeavApprvListViewHolder holder, final int position) {

        final LeaveRequestList lvData = lvApprvList.get(position);
        //Employee employee = empHandoverList.get(position);
        holder._nameTv.setText(lvData.employee.getEmpName());
        holder._lvTypTv.setText(lvData.getLeave_type());
        holder._lvFrmTv.setText(lvData.getLeave_frm());
        holder._lvToTv.setText(lvData.getLeave_to());
        FileUtils.loadImage(lvData.employee.getEmpProfilePic(),context,holder._profileImage);

        switch (lvData.getLeave_status()){

            case "0":
                holder._menuButtn.setVisibility(View.VISIBLE);
                holder._apprvd.setVisibility(View.GONE);
                holder._cancelld.setVisibility(View.GONE);
                break;

            case "1":
                holder._menuButtn.setVisibility(View.GONE);
                holder._apprvd.setVisibility(View.VISIBLE);
                holder._cancelld.setVisibility(View.GONE);
                break;

            case "2":
                holder._menuButtn.setVisibility(View.GONE);
                holder._apprvd.setVisibility(View.GONE);
                holder._cancelld.setVisibility(View.VISIBLE);
                break;

        }

        holder._menuButtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Log.d("LEAVE ID  ", lvData.getLeave_id());

                lvId = lvData.getLeave_id();
                PopupMenu popup = new PopupMenu(view.getContext(), view);
                popup.inflate(R.menu.popup_menu);
                popup.setGravity(Gravity.END);
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        item.getItemId();

                        switch (item.getItemId()){

                            case R.id.accept:
                                status = "approve";

                                approvDialogObj = new ApprovDialog(requestApprovalFragment);
                                approvDialogObj.loadApprovDialog(view.getContext(), "REQUEST_APPROVAL_FRAGMENT", null, empHandoverList,status, lvId, null, lvData);

                                //requestApprovalFragment.ApprovDialog(null, status, lvId);

                                break;

                            case R.id.reject:
                                status = "reject";
                                Log.d("CLICKED ", "REJECT");

                                approvDialogObj = new ApprovDialog(requestApprovalFragment);
                                approvDialogObj.loadRejectDialog(view.getContext(), "REQUEST_APPROVAL_FRAGMENT", status, lvId, null);

                                break;

                            case R.id.view_details:
                                requestApprovalFragment.empDetView(position, empHandoverList);
                                Log.d("CLICKED ", "VIEW DETAILS");
                                break;

                        }
                        return true;
                    }
                });
                popup.show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return lvApprvList.size();
    }

}
