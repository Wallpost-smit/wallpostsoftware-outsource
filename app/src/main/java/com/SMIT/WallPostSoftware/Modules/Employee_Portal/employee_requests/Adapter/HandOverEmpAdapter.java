package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Extras.FileUtils;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.SelectImage;
import com.SMIT.WallPostSoftware.R;

import java.util.ArrayList;

/**
 * Created by alex on 14/10/16.
 */
public class HandOverEmpAdapter extends RecyclerView.Adapter<HandOverEmpAdapter.HandOverEmpViewHolder> {

    private ArrayList<Employee> employeList;
    Context context;

    // Define listener member variable
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);

    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class HandOverEmpViewHolder extends RecyclerView.ViewHolder {

        public ImageView _empProfile;
        public TextView _empName;

        public HandOverEmpViewHolder(final View itemView) {
            super(itemView);

            _empProfile = (ImageView) itemView.findViewById(R.id.employee_image);
            _empName = (TextView) itemView.findViewById(R.id.employee_name);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });

        }
    }

    public HandOverEmpAdapter(ArrayList<Employee> employeList, OnItemClickListener listener) {
        this.employeList = employeList;
        this.listener = listener;
    }

    @Override
    public HandOverEmpViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.handover_emp_row, parent, false);

        context=parent.getContext();
        return new HandOverEmpViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HandOverEmpViewHolder holder, int position) {

        Employee handoverEmp = employeList.get(position);
        Log.d("PROFILE PIC ", handoverEmp.getEmpProfilePic());
        holder._empName.setText(handoverEmp.getEmpName());
        FileUtils.loadImage(handoverEmp.getEmpProfilePic(),context,holder._empProfile);

    }

    @Override
    public int getItemCount() {
        return employeList.size();
    }

}
