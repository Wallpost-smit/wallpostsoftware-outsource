package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.Preference;
import com.SMIT.WallPostSoftware.Extras.Util;
import com.SMIT.WallPostSoftware.Extras.WebManager;
import com.SMIT.WallPostSoftware.Model_Class.CompanyList;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.FileData;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter.CurrentLvListAdapter;
import com.SMIT.WallPostSoftware.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by alex on 20/09/16.
 */
public class CurrentLvFragment extends Fragment {

    ViewGroup rootView;

    public RecyclerView _lvRecycl;
    private CardView emptycard;
    public CurrentLvListAdapter currentLvListAdapterAdapter;
    public ArrayList<LeaveRequestList> curLvReqlist = new ArrayList<>();
    public ArrayList<Employee> empHandoverList = new ArrayList<>();
    public ArrayList<HashMap<Integer,Employee>> selectdEmpListFrmResponse = new ArrayList<>();

    public ArrayList<HashMap<Integer,FileData>> fileListFrmResponse = new ArrayList<>();


    ArrayList<LeaveRequestList> CurrentSortList = new ArrayList<>();
    ArrayList<LeaveRequestList> approvedList = new ArrayList<>();
    LeaveRequestList leaveRequestList = new LeaveRequestList();


    private ProgressDialog progressDialog;
    Context context;

    public String authToken, req_url, userId,employeeId;
    public String jResult, jMessage;

    public CurrentLvFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("LEAVE REQUEST ", "FRAGMENT ");
        rootView = (ViewGroup) inflater.inflate(R.layout.current_lv_fragment, container, false);

        Preference pref= new Preference(container.getContext(),"login");
        CompanyList company= pref.getCompany();

        employeeId = company.employee.empId;

        _lvRecycl = (RecyclerView) rootView.findViewById(R.id.recyclr_lv_list);
        emptycard = (CardView)rootView.findViewById(R.id.empty_card);
        emptycard.setVisibility(View.INVISIBLE);


        loadLvList();


        return rootView;
    }

    public void loadLvList(){

        {
            curLvReqlist = new ArrayList<>();

            req_url = Constants.LEAVE_LIST_URL+employeeId;


            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setTitle("Processing...");
            progressDialog.setMessage("Please wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            context = getActivity().getApplicationContext();
            WebManager webManager = new WebManager(context);
            webManager.LeaveList(req_url,new WebManager.VolleyCallback(){

                @Override
                public void onSuccess(String result) {
                    progressDialog.dismiss();

                    Log.d("LEAVe LIST RESP ", result);
                    try {
                        JSONObject jsonObject = new JSONObject(result);
                        jResult = jsonObject.getString("result");
                        jMessage = jsonObject.getString("message");
                        if(jResult.equals("1")){

                            JSONObject jData = jsonObject.getJSONObject("data");
                            JSONArray jLeavDataArry = jData.getJSONArray("leave_request");
                            Log.d("LEAVE DATA ",jLeavDataArry.toString());



                            for (int i =0; i<jLeavDataArry.length(); i++){

                                LeaveRequestList leaveRequestList = new LeaveRequestList();
                                JSONObject jsonLv = jLeavDataArry.getJSONObject(i);
                                leaveRequestList.setLeave_id(jsonLv.getString("id"));
                                JSONObject jLeavType = jsonLv.getJSONObject("leave_type");

                                leaveRequestList.setLeave_type(jLeavType.getString("name"));
                                leaveRequestList.setLeave_type_id(jLeavType.getString("id"));

                                leaveRequestList.setApproval_comments(jsonLv.getString("approval_comments"));
                                leaveRequestList.setRejected_reason(jsonLv.getString("rejected_reason"));
                                leaveRequestList.setCancelled_reason(jsonLv.getString("cancel_reason"));
                                leaveRequestList.setReplaceRequird(jsonLv.getString("replaced_required"));
                                if (jsonLv.getString("replaced_required").equals("2")){

                                    selectdEmpListFrmResponse=new ArrayList<>();
                                    JSONArray jsonRepalcedByArry = jsonLv.getJSONArray("replaced_by");

                                    HashMap<Integer,Employee> replacEmpMap=new HashMap<Integer, Employee>();

                                    for (int j =0; j<jsonRepalcedByArry.length(); j++){

                                        JSONObject jsonReplacedByObj = jsonRepalcedByArry.getJSONObject(j);
                                        Employee e = new Employee();
                                        e = parseEmployee(jsonReplacedByObj);
                                        replacEmpMap.put(j,e);
                                        selectdEmpListFrmResponse.add(replacEmpMap);

                                    }
                                    leaveRequestList.employee.setSelectdListFrmResponse(selectdEmpListFrmResponse);

                                }
                                //FILE DATA
                                JSONArray jsonFileArry = jsonLv.getJSONArray("attach_doc");
                                if (jsonFileArry.length() > 0){

                                    fileListFrmResponse=new ArrayList<>();

                                    HashMap<Integer,FileData> fileDataMap=new HashMap<Integer, FileData>();
                                    for (int k= 0;k<jsonFileArry.length(); k++){

                                        JSONObject jsonFileObj = jsonFileArry.getJSONObject(k);

                                        leaveRequestList.fileData = new FileData();

                                        leaveRequestList.fileData.setFileName(jsonFileObj.getString("document_file_name"));
                                        leaveRequestList.company.setCompanyId(jsonFileObj.getString("company_id"));
                                        leaveRequestList.fileData.setRefrnceId(jsonFileObj.getString("reference_id"));
                                        leaveRequestList.fileData.setAttachmentData(jsonFileObj.getString("attachment"));

                                        fileDataMap.put(k,leaveRequestList.fileData);

                                        fileListFrmResponse.add(fileDataMap);

                                    }

                                    leaveRequestList.fileData.setFileListFrmResponse(fileListFrmResponse);
                                }

                                if (jsonLv.get("exit_required").equals("1")){
                                    leaveRequestList.setLeave_exit_permit("true");
                                    leaveRequestList.setLeave_depature_date(jsonLv.getString("departure_date"));
                                    leaveRequestList.setLeave_depature_time(jsonLv.getString("departure_time"));
                                    leaveRequestList.setLeave_return_date(jsonLv.getString("return_date"));
                                    leaveRequestList.setLeave_return_time(jsonLv.getString("return_time"));
                                }

                                if (jsonLv.get("ticket").equals("Yes")){
                                    leaveRequestList.setLeave_tickets("true");
                                    if (!(jsonLv.isNull("origin"))){
                                        Log.d("LOG",Integer.toString(i));
                                        JSONObject origin = jsonLv.getJSONObject("origin");
                                        leaveRequestList.setOrigin(origin.getString("airport"));
                                        JSONObject destination = jsonLv.getJSONObject("destination");
                                        leaveRequestList.setDestination(destination.getString("airport"));
                                    }

                                    leaveRequestList.setLeave_entitled_for(jsonLv.getString("family_ticket"));
                                    leaveRequestList.setWay_type(jsonLv.getString("way_type"));
                                    leaveRequestList.setAir_class(jsonLv.getString("air_class"));
                                    leaveRequestList.setNumber_of_adults(jsonLv.getString("no_ticket_adult"));
                                    leaveRequestList.setNumber_of_childrens(jsonLv.getString("no_ticket_children"));

                                }
//
//                                if (Util.getCurrentDate().after(Util.getFormattedDateFromString(jsonLv.getString("leave_from")))){
//                                    //holder._lvcancelButton.setVisibility(View.INVISIBLE);
//
//                                }


                                if(!jsonLv.getString("status").equals("3") && !jsonLv.getString("status").equals("2") && !Util.getCurrentDate().after(Util.getFormattedDateFromString(jsonLv.getString("leave_from")))){
                                    leaveRequestList.setLeave_frm(jsonLv.getString("leave_from"));
                                    leaveRequestList.setLeave_to(jsonLv.getString("leave_to"));
                                    leaveRequestList.setLeave_days(jsonLv.getString("leave_days"));
                                    leaveRequestList.setLeave_status(jsonLv.getString("status"));
                                    leaveRequestList.setLeave_resn(jsonLv.getString("leave_reason"));
                                    leaveRequestList.setLeave_contact(jsonLv.getString("contact_on_leave"));
                                    leaveRequestList.setLeave_email(jsonLv.getString("contact_email"));
                                    curLvReqlist.add(leaveRequestList);
                                }


                            }

                        if (curLvReqlist.size() > 0){

                            for (int j =0; j<curLvReqlist.size(); j++){

                            }

                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
                            _lvRecycl.setLayoutManager(mLayoutManager);
                            _lvRecycl.setItemAnimator(new DefaultItemAnimator());

                            currentLvListAdapterAdapter = new CurrentLvListAdapter(curLvReqlist,CurrentLvFragment.this, new CurrentLvListAdapter.OnItemClickListener(){

                                @Override
                                public void onItemClick(View itemView, int position) {

                                    Intent intent = new Intent(getActivity(), LeaveReqDetActivity.class);
//                                Bundle bundle = new Bundle();
//                                bundle.putSerializable("LEAVE",lvReqlist.get(position));

                                    Bundle bundle = new Bundle();
                                    bundle.putSerializable("LEAVE",curLvReqlist.get(position));
                                    intent.putExtras(bundle);
                                    startActivity(intent);
                                }
                            });

                            _lvRecycl.setAdapter(currentLvListAdapterAdapter);
                            currentLvListAdapterAdapter.notifyDataSetChanged();

                            _lvRecycl.setVisibility(View.VISIBLE);
                            emptycard.setVisibility(View.INVISIBLE);
                        }else{
                            _lvRecycl.setVisibility(View.INVISIBLE);
                            emptycard.setVisibility(View.VISIBLE);
                        }



                        }


                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                }

                @Override
                public void onError(String result) {

                }
            });

        }

    }





    //HANDOVER EMPLOYEE DETAILS FROM THE SERVER
    public Employee parseEmployee(JSONObject empObject){
        Employee employee = new Employee();
        try {
            employee.setEmpProfilePic(empObject.getString("profile_image"));
            employee.setEmpName(empObject.getString("name") + " " + empObject.getString("middle_name") + " " + empObject.getString("short_name"));
            employee.setEmpStaffNo(empObject.getString("code"));
            employee.setEmpJoiningDate(empObject.getString("join_date"));

            if (!empObject.isNull("grade")){
                JSONObject gradeObject = empObject.getJSONObject("grade");
                employee.setEmpGrade(gradeObject.getString("grade"));
            }

            if (!empObject.isNull("department")){

                JSONObject deptObject = empObject.getJSONObject("department");
                employee.setDepartment(deptObject.getString("name"));
            }

            if (!empObject.isNull("position")){
                JSONObject posObject = empObject.getJSONObject("position");
                employee.setPosition(posObject.getString("name"));
            }
            empHandoverList.add(employee);

        }catch (Exception e){

        }
        return employee;
    }









    //LOADING FILTER FOR SORTING FROM HOME ACTIVITY
    public void loadFilter(final Context context, LeaveRequestFragment lvReqFrag, String status){

        CurrentSortList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        lvReqFrag.curntLvFragment._lvRecycl.setLayoutManager(mLayoutManager);
        lvReqFrag.curntLvFragment._lvRecycl.setItemAnimator(new DefaultItemAnimator());

        if(status.equals(" ")){

            CurrentSortList = lvReqFrag.curntLvFragment.curLvReqlist;
        }else {

            for (int i =0; i<lvReqFrag.curntLvFragment.curLvReqlist.size(); i++){

                leaveRequestList = lvReqFrag.curntLvFragment.curLvReqlist.get(i);
                Log.d("STATUSS ", leaveRequestList.getLeave_status());
                if(leaveRequestList.getLeave_status().equals(status)){

                    CurrentSortList.add(leaveRequestList);
                }
            }
        }



        lvReqFrag.curntLvFragment.currentLvListAdapterAdapter =new CurrentLvListAdapter(CurrentSortList, CurrentLvFragment.this, new CurrentLvListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {

                Intent intent = new Intent(context, LeaveReqDetActivity.class);

                Bundle bundle = new Bundle();
                bundle.putSerializable("LEAVE",CurrentSortList.get(position));
                intent.putExtras(bundle);
                context.startActivity(intent);
            }
        });
        lvReqFrag.curntLvFragment._lvRecycl.setAdapter(lvReqFrag.curntLvFragment.currentLvListAdapterAdapter);

        lvReqFrag.curntLvFragment.currentLvListAdapterAdapter.notifyDataSetChanged();

    }
}
