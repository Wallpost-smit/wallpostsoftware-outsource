package com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.FileUtils;
import com.SMIT.WallPostSoftware.HomeScreenActivity;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.FileData;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter.DisplayImageAdapter;
import com.SMIT.WallPostSoftware.R;
import com.SMIT.WallPostSoftware.common.ApprovDialog;
import com.SMIT.WallPostSoftware.common.GalleryActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class LeaveApprvDetActivity extends AppCompatActivity {

    public ImageView _statusImage, _profileImage;

    public TextView _statusTv, _lvTypeTv, _lvFrmTv, _lvToTv, _lvDaysTv,  _lvPaidTv, _lvContactNoTv, _lvEmailTv, _staffNoTv, _deptTv, _gradeTv, _joiningDateTv;
    public TextView _empNameTv, _empDesigTv, _lvReasnTv, _paidUnPaidLabelTv;

    public DisplayImageAdapter displayImageAdapter;
    public RecyclerView _fileDataRecyclrView;

    public FloatingActionButton _lvApprvBtn, _lvRejectBtn;

    public ApprovDialog approvDialogObj;

    private ArrayList<Employee> empHandoverList;
    public ArrayList<Employee> selectedEmpList;

    public ArrayList<Employee> empSelectdFrmRespnse = new ArrayList<>();
    public ArrayList<FileData> fileFrmRespnse = new ArrayList<>();

    public CardView _fileLayout;


    public String status, lvId, rejectedReason;

    Context context;

    public Boolean isClicked = false;

    LeaveRequestList leaveRequestList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leave_apprv_det_activity);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ApprovDialog apprvDialogObj=new ApprovDialog(LeaveApprvDetActivity.this);


        Intent intent = getIntent();
        leaveRequestList = (LeaveRequestList) intent.getSerializableExtra("LEAVE OBJECT");

        empHandoverList = (ArrayList<Employee>) getIntent().getSerializableExtra("HAND OVER EMPLOYES");
        Log.d("RRRR", empHandoverList.toString());
        setTitle(" ");





//        CollapsingToolbarLayout collapsingToolbar =
//                (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
//        collapsingToolbar.setTitle(lv.employee.empName);

        _profileImage = (ImageView) findViewById(R.id.profile_image);
        FileUtils.loadImage(leaveRequestList.employee.getEmpProfilePic(),this,_profileImage);

        _empNameTv = (TextView) findViewById(R.id.employee_name);
        _empDesigTv = (TextView) findViewById(R.id.employee_desig);


        _statusImage = (ImageView) findViewById(R.id.leave_status_img);
        _statusTv = (TextView) findViewById(R.id.leave_status);

        _lvTypeTv = (TextView) findViewById(R.id.leave_heading);
        _lvFrmTv = (TextView) findViewById(R.id.lv_frm);
        _lvToTv = (TextView) findViewById(R.id.lv_to);
        _lvDaysTv = (TextView) findViewById(R.id.lv_days);
        _lvPaidTv = (TextView) findViewById(R.id.lv_paid);
        _lvContactNoTv = (TextView) findViewById(R.id.lv_contact);
        _lvEmailTv = (TextView) findViewById(R.id.lv_emailId);
        _staffNoTv = (TextView) findViewById(R.id.staff_no);
        _deptTv = (TextView) findViewById(R.id.department);
        _gradeTv = (TextView) findViewById(R.id.grade);
        _joiningDateTv = (TextView) findViewById(R.id.joining_date);
        _lvReasnTv = (TextView) findViewById(R.id.lv_reasn);
        _paidUnPaidLabelTv = (TextView) findViewById(R.id.paid_unpaid_label);

        _fileLayout = (CardView) findViewById(R.id.file_card_layout);
        _fileDataRecyclrView = (RecyclerView) findViewById(R.id.file_recycler_view);

        _empNameTv.setText(leaveRequestList.employee.getEmpName());
        _empDesigTv.setText(leaveRequestList.employee.getPosition());

        _lvTypeTv.setText(leaveRequestList.getLeave_type());
        _lvFrmTv.setText(leaveRequestList.getLeave_frm());
        _lvToTv.setText(leaveRequestList.getLeave_to());
        _lvReasnTv.setText(leaveRequestList.getLeave_resn());
        _lvDaysTv.setText(leaveRequestList.getLeave_days() + "  " + "day(s)");
        _lvPaidTv.setText(leaveRequestList.getLeave_paid() + "  " + "day(s)");
        _lvContactNoTv.setText(leaveRequestList.getLeave_contact());
        _lvEmailTv.setText(leaveRequestList.getLeave_email());
        _staffNoTv.setText(leaveRequestList.employee.getEmpStaffNo());
        _deptTv.setText(leaveRequestList.employee.getDepartment());
        _gradeTv.setText(leaveRequestList.employee.getEmpGrade());
        _joiningDateTv.setText(leaveRequestList.employee.getEmpJoiningDate());


        lvId = leaveRequestList.getLeave_id();
        if(leaveRequestList.getLeave_status().equals("1")){

            if(leaveRequestList.getReplaceRequird().equals("2")){
                ArrayList<HashMap<Integer,Employee>> selectdEmpRespnsArry=leaveRequestList.employee.getSelectdListFrmResponse();


                Log.e("COUNTttttt", selectdEmpRespnsArry.toString());
                for (int i =0; i<selectdEmpRespnsArry.size(); i++){

                    Log.d("HELLO WORLDtttt ", selectdEmpRespnsArry.get(i).get(i).getEmpName());
                    empSelectdFrmRespnse.add(selectdEmpRespnsArry.get(i).get(i));
                    for(int j = 0; j<empHandoverList.size(); j++){
                        Employee employee = selectdEmpRespnsArry.get(i).get(i);
                        if (employee.getEmpName().equals(empHandoverList.get(j).getEmpName()))
                        {
                            empHandoverList.get(j).setChecked(true);
                        }
                    }
                }
            }
        }
        context = getApplicationContext();


        //FILES FROM RESPONSE
        if(leaveRequestList.fileData.getFileListFrmResponse()!=null){

            _fileLayout.setVisibility(View.VISIBLE);
            ArrayList<HashMap<Integer,FileData>> fileMapListFrmResponse=leaveRequestList.fileData.getFileListFrmResponse();

            for (int j =0; j<fileMapListFrmResponse.size(); j++){

                Log.d("FILESSSSS ", fileMapListFrmResponse.get(j).get(j).getAttachmentData());
                fileFrmRespnse.add(fileMapListFrmResponse.get(j).get(j));

            }


            Log.d("LIST SIZEEE ", String.valueOf(leaveRequestList.fileData.getFileListFrmResponse().size()));

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            _fileDataRecyclrView.setLayoutManager(mLayoutManager);
            _fileDataRecyclrView.setItemAnimator(new DefaultItemAnimator());

            Log.d("HELLLL ", String.valueOf(fileFrmRespnse.size()));
            displayImageAdapter = new DisplayImageAdapter(fileFrmRespnse, new DisplayImageAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View itemView, int position) {

                    Intent galleryIntent = new Intent(LeaveApprvDetActivity.this, GalleryActivity.class);
                    galleryIntent.putExtra("IMAGES_ARRAY", fileFrmRespnse);
                    galleryIntent.putExtra("SELECTED_IMAGE_POS", position);
                    startActivity(galleryIntent);
                }
            });

            _fileDataRecyclrView.setAdapter(displayImageAdapter);
            displayImageAdapter.notifyDataSetChanged();
        }


        Log.d("LEAVE TYPE ", leaveRequestList.getLeave_type());
        if(leaveRequestList.getLeave_status().equals("2")){
            rejectedReason = leaveRequestList.getRejected_reason();
        }else {
            rejectedReason = null;
        }



        switch (leaveRequestList.getLeave_status()){

            case "0":
                _statusImage.setColorFilter(getResources().getColor(R.color.colorYellow));
                _statusTv.setText("PENDING");
                _statusTv.setTextColor(getResources().getColor(R.color.colorYellow));
                break;

            case "1":
                _statusImage.setColorFilter(getResources().getColor(R.color.colorGreen));
                _statusTv.setText("APPROVED");
                _statusTv.setTextColor(getResources().getColor(R.color.colorGreen));
                break;

            case "2":
                _statusImage.setColorFilter(getResources().getColor(R.color.colorRed));
                _statusTv.setText("REJECTED");
                _statusTv.setTextColor(getResources().getColor(R.color.colorRed));
                break;

            case "3":
                _statusImage.setColorFilter(getResources().getColor(R.color.colorOrange));
                _statusTv.setText("CANCELLED");
                _statusTv.setTextColor(getResources().getColor(R.color.colorOrange));
                break;
        }

        _lvApprvBtn = (FloatingActionButton) findViewById(R.id.approve_fab);
        if(leaveRequestList.getLeave_status().equals("1")){
            _lvApprvBtn.setVisibility(View.GONE);
        }

        _lvApprvBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isClicked = true;
                approvDialogObj = new ApprovDialog(LeaveApprvDetActivity.this);
                if(empSelectdFrmRespnse!=null){

                    approvDialogObj.loadApprovDialog(view.getContext(), "LEAVE_APPRV_DET_ACTIVITY", empHandoverList, empHandoverList,"approve", lvId, empSelectdFrmRespnse,leaveRequestList);

                }else {
                    approvDialogObj.loadApprovDialog(view.getContext(), "LEAVE_APPRV_DET_ACTIVITY", null, empHandoverList,"approve", lvId, null,leaveRequestList);

                }
            }
        });


        _lvRejectBtn = (FloatingActionButton) findViewById(R.id.reject_fab);
        if(leaveRequestList.getLeave_status().equals("2")){
            _lvRejectBtn.setVisibility(View.GONE);
            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams)_lvApprvBtn.getLayoutParams();
            params.setMargins(0, 0, 50, 0); //substitute parameters for left, top, right, bottom
            _lvApprvBtn.setLayoutParams(params);


        }
        _lvRejectBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isClicked = false;
                LeaveApprvDetActivity lv = new LeaveApprvDetActivity();
                approvDialogObj = new ApprovDialog(LeaveApprvDetActivity.this);
                approvDialogObj.loadRejectDialog(view.getContext(), "LEAVE_APPRV_DET_ACTIVITY","reject", lvId, rejectedReason);
            }
        });

    }

    public void updateData(){

        if(isClicked){
            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Leave Approved Successfully.");
            alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            goBack();
                        }
                    });
            alertDialog.show();
        }else {
            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(this).create();
            alertDialog.setTitle("Alert");
            alertDialog.setMessage("Leave Rejected Successfully.");
            alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            goBack();
                        }
                    });
            alertDialog.show();
        }

    }

    public void goBack(){


        Intent intent = new Intent(this,HomeScreenActivity.class);
        intent.putExtra(Constants.IntentKey.Key.FRAGMENT_ID, "0");
        startActivityForResult(intent, 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {

            if(resultCode == 0){
                selectedEmpList = new ArrayList<>();

                lvId = data.getStringExtra("LEAVE ID");

                Bundle bundle = data.getExtras();
                leaveRequestList= (LeaveRequestList) bundle.getSerializable("LEAVE");


                approvDialogObj = new ApprovDialog(this);
                if(empSelectdFrmRespnse!=null){

                    approvDialogObj.loadApprovDialog(this, "LEAVE_APPRV_DET_ACTIVITY", (ArrayList<Employee>) data.getSerializableExtra("ARRAYLIST"), null, "approve", lvId, empSelectdFrmRespnse,leaveRequestList);

                }else {

                    approvDialogObj.loadApprovDialog(this, "LEAVE_APPRV_DET_ACTIVITY", (ArrayList<Employee>) data.getSerializableExtra("ARRAYLIST"), null, "approve", lvId, null,leaveRequestList);

                }

            }

        }
    }

}
