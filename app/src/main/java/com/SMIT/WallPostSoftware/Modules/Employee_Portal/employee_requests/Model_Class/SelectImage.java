package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class;

import android.graphics.Bitmap;

/**
 * Created by alex on 12/10/16.
 */
public class SelectImage {

    public Bitmap bitmap;

    public String imagePath;


    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}


