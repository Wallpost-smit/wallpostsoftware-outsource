package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.API;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.Preference;
import com.SMIT.WallPostSoftware.Extras.WebManager;
import com.SMIT.WallPostSoftware.Model_Class.CompanyList;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.LeaveType;
import com.SMIT.WallPostSoftware.R;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by alex on 17/10/16.
 */
public class LeaveSettingsApi {

    private ProgressDialog _progressDialog;
    public MaterialDialog _dialog;
    public CheckBox _lvTypeCheckBox, _lvActionCheckBox;
    public LinearLayout _lvActionLayout;

    public Context context;
    public String reqUrl, employeeId;



    public ArrayList<String> leaveTypeList = new ArrayList<>();
    public ArrayList<String> leaveNames = new ArrayList<>();



    public ArrayList<String> getLeaveTypeList() {
        return leaveTypeList;
    }

    public void setLeaveTypeList(ArrayList<String> leaveTypeList) {
        this.leaveTypeList = leaveTypeList;
    }




    public void loadLvSettings(final Context context){

        this.context = context;
        Preference pref = new Preference(context, "login");
        CompanyList company = pref.getCompany();

        employeeId = company.employee.empId;

        reqUrl = Constants.CREATE_LEAVE_REQ_URL_SETTINGS + employeeId;

        _progressDialog = new ProgressDialog(context);
        _progressDialog.setTitle("Processing...");
        _progressDialog.setMessage("Please wait...");
        _progressDialog.setCancelable(false);
        _progressDialog.show();

        WebManager webManager = new WebManager(context);
        webManager.LeavReq(reqUrl, new WebManager.VolleyCallback() {
            @Override
            public void onSuccess(String result) {

                _progressDialog.dismiss();
                Log.d("SETTINGS API RESPONSE ", result);
                try {
                    JSONObject json = new JSONObject(result);

                    JSONObject jsonObject = json.getJSONObject("data");
                    JSONArray jsonLvType = jsonObject.getJSONArray("leaveTypes");

                    for (int i = 0; i < jsonLvType.length(); i++) {
                        JSONObject jObj = jsonLvType.getJSONObject(i);
                        leaveNames.add(jObj.getString("name"));
                    }

                    LeaveSettingsApi.this.setLeaveTypeList(leaveNames);



                } catch (JSONException e) {
                    _progressDialog.dismiss();


                    android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage(e.getMessage());
                    alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                    //    finish();
                                }
                            });
                    alertDialog.show();
                    e.printStackTrace();
                }
                _progressDialog.dismiss();
            }

            @Override
            public void onError(String error) {
                _progressDialog.dismiss();
                Log.d("ERROR ", error.toString());

                android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                alertDialog.setTitle("Alert");
                alertDialog.setMessage(error);
                alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                // finish();

                            }
                        });
                alertDialog.show();
            }

        });
    }


}
