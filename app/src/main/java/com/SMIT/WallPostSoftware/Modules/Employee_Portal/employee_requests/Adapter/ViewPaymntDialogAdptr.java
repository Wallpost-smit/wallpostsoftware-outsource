package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.ViewPaymentDialog;
import com.SMIT.WallPostSoftware.R;

import java.util.List;

/**
 * Created by alex on 25/08/16.
 */
public class ViewPaymntDialogAdptr extends RecyclerView.Adapter<ViewPaymntDialogAdptr.PaymntViewHolder> {

    private List<ViewPaymentDialog> payList;

    public class PaymntViewHolder extends RecyclerView.ViewHolder {
        public TextView _accntNameTv, _amountTv;

        public PaymntViewHolder(View view) {
            super(view);
            _accntNameTv = (TextView) view.findViewById(R.id.accnt_name);
            _amountTv = (TextView) view.findViewById(R.id.amount);
        }
    }

    public ViewPaymntDialogAdptr(List<ViewPaymentDialog> payList){
        this.payList = payList;
    }
    @Override
    public PaymntViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_paymt_dialog_row, parent, false);

        return new PaymntViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PaymntViewHolder holder, int position) {

        ViewPaymentDialog viewPaymentDialog = payList.get(position);
        holder._accntNameTv.setText(viewPaymentDialog.getAccntName());
        holder._amountTv.setText(viewPaymentDialog.getAmount());
    }

    @Override
    public int getItemCount() {
        return payList.size();
    }


}
