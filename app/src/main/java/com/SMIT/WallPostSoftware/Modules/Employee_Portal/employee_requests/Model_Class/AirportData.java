package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class;

/**
 * Created by alex on 23/08/16.
 */
public class AirportData {

    public String airportId;

    public String airportName;

    public String airportCode;

    public String airportActvStats;

    public String getAirportId() {
        return airportId;
    }

    public void setAirportId(String airportId) {
        this.airportId = airportId;
    }

    public String getAirportName() {
        return airportName;
    }

    public void setAirportName(String airportName) {
        this.airportName = airportName;
    }

    public String getAirportCode() {
        return airportCode;
    }

    public void setAirportCode(String airportCode) {
        this.airportCode = airportCode;
    }

    public String getAirportActvStats() {
        return airportActvStats;
    }

    public void setAirportActvStats(String airportActvStats) {
        this.airportActvStats = airportActvStats;
    }
}
