package com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter.SelectEmployeeAdapter;
import com.SMIT.WallPostSoftware.R;

import java.io.Serializable;
import java.util.ArrayList;



public class SelectEmplyActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private SelectEmployeeAdapter sAdapter;
    public RecyclerView _handoverEmpRecyclr;

    public ArrayList<Employee> empHandoverList = new ArrayList<>();


    public String lvId;
    private LeaveRequestList leave;

    private Toolbar mToolbar;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_emply_activity);

        context = getApplicationContext();

        setTitle("Select Employee");

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(mToolbar);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        empHandoverList = (ArrayList<Employee>) getIntent().getSerializableExtra("HAND OVER EMPLOYES");

        lvId = getIntent().getStringExtra("LEAVE ID");

        Intent i = getIntent();
        Bundle bundle = i.getExtras();
        leave = (LeaveRequestList)bundle.getSerializable("LEAVE");

        _handoverEmpRecyclr = (RecyclerView) findViewById(R.id.recyclr_handover_emp_list) ;
        sAdapter = new SelectEmployeeAdapter(empHandoverList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        _handoverEmpRecyclr.setLayoutManager(mLayoutManager);
        _handoverEmpRecyclr.setItemAnimator(new DefaultItemAnimator());
        _handoverEmpRecyclr.setAdapter(sAdapter);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.done:

                if(sAdapter.count>0){

                    Intent intent = new Intent();
                    intent.putExtra("ARRAYLIST", (Serializable) empHandoverList);
                    intent.putExtra("LEAVE ID", lvId);

                    Bundle bundle = new Bundle();
                    bundle.putSerializable("LEAVE",leave);
                    intent.putExtras(bundle);

                    setResult(0, intent);
                    finish();

                }else {
                    android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(SelectEmplyActivity.this).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Please select the employee");
                    alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }




    private ArrayList<Employee> filter(ArrayList<Employee> objects, String query) {
        query = query.toLowerCase();

        ArrayList<Employee> filterdList = new ArrayList<>();
        for (Employee object : objects) {

            Log.d("TESTTT ",object.empName.toLowerCase());
            final String text = object.getEmpName().toLowerCase();
            if (text.contains(query)) {
                filterdList.add(object);
            }
        }
        return filterdList;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        ArrayList<Employee> filteredModelList = filter(empHandoverList, newText);


        sAdapter.empList = filteredModelList;
        Log.e("Filtered list",""+empHandoverList.size());
        sAdapter.notifyDataSetChanged();

        return true;
    }


    @Override
    public void onBackPressed() {

        Intent intent = new Intent();
        intent.putExtra("ARRAYLIST", "NULL");
        setResult(1, intent);
        finish();


    }
}
