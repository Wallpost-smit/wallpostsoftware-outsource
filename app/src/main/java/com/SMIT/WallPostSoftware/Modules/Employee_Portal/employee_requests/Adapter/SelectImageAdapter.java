package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.LeaveReqDetActivity;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.SelectImage;
import com.SMIT.WallPostSoftware.R;
import com.SMIT.WallPostSoftware.common.CustomDialogFragment;

import java.util.ArrayList;

/**
 * Created by alex on 12/10/16.
 */
public class SelectImageAdapter extends RecyclerView.Adapter<SelectImageAdapter.ImageViewHolder>{

    private ArrayList<SelectImage> bitmapList;
    public LeaveReqDetActivity lv;
    public CustomDialogFragment customDialogFrag;


    public class ImageViewHolder extends RecyclerView.ViewHolder {

        public ImageView _selectImg, _deletImg;

        public ImageViewHolder(View itemView) {
            super(itemView);

            _selectImg = (ImageView) itemView.findViewById(R.id.selected_image);
            _deletImg = (ImageView) itemView.findViewById(R.id.delete_image);
            _deletImg.setVisibility(View.VISIBLE);
        }
    }

    public SelectImageAdapter(ArrayList<SelectImage> bitmapList, LeaveReqDetActivity lv, CustomDialogFragment customDialogFragment) {
        this.bitmapList = bitmapList;
        this.lv = lv;
        this.customDialogFrag = customDialogFragment;
    }

    @Override
    public ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.selected_images_row, parent, false);

        return new ImageViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ImageViewHolder holder, final int position) {

        SelectImage selectImage = bitmapList.get(position);
        holder._selectImg.setImageBitmap(selectImage.getBitmap());
        holder._deletImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("DELETE ","CLICKED");
                if(lv!=null){
                    lv.updateImageArray("delete", bitmapList.get(position));
                }else if (customDialogFrag!=null){

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return bitmapList.size();
    }


}
