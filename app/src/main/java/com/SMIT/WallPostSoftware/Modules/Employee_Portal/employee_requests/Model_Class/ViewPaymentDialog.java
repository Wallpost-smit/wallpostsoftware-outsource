package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class;

/**
 * Created by alex on 25/08/16.
 */
public class ViewPaymentDialog {

    public String accntName, amount;


    public String getAccntName() {
        return accntName;
    }

    public void setAccntName(String accntName) {
        this.accntName = accntName;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
