package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.AirportData;
import com.SMIT.WallPostSoftware.R;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 23/08/16.
 */
public class AirportAdapter extends ArrayAdapter<AirportData> {

    public final List<AirportData> airportDataList;
    public List<AirportData> filteredAirport = new ArrayList<>();
    public AirportData airportData;

    public AirportAdapter(Context context, List<AirportData> airportDataList) {
        super(context, 0, airportDataList);
        this.airportDataList = airportDataList;
    }

    @Override
    public int getCount() {
        return filteredAirport.size();
    }

    @Override
    public Filter getFilter() {
        return new AirportFilter(this, airportDataList);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item from filtered list.
        airportData = filteredAirport.get(position);
        // Inflate your custom row layout as usual.
        LayoutInflater inflater = LayoutInflater.from(getContext());
        convertView = inflater.inflate(R.layout.auto_complete_row, parent, false);

        TextView tvName = (TextView) convertView.findViewById(R.id.auto_compl_row);
        Log.e("OUTPUT",""+airportData.getAirportName());
        tvName.setText(airportData.getAirportName());
        tvName.setTextColor(Color.BLACK);


        return convertView;
    }


}







