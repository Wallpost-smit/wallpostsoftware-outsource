package com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.WebManager;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter.LeaveApprvListAdaptr;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.request_approvals.Adapter.SelectedEmployeeAdapter;

import com.SMIT.WallPostSoftware.R;
import com.SMIT.WallPostSoftware.common.ApprovDialog;
import com.SMIT.WallPostSoftware.common.LeaveDetail;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 */
public class RequestApprovalFragment extends Fragment {

    //Anas Declarations
    public ArrayList<HashMap<Integer,Employee>> selectdEmpListFrmResponse;
    public ArrayList<LeaveRequestList> apprvList  = new ArrayList<>();
    ArrayList<LeaveRequestList> reqAprvlSortList = new ArrayList<>();

    public ArrayList<Employee> empHandoverList = new ArrayList<>();
    public ArrayList<Employee> selectedEmpList;

    public ArrayList<String> empIdList = new ArrayList<>();
    public RecyclerView recyclerView;
    private LeaveApprvListAdaptr mAdapter;

    private SelectedEmployeeAdapter sAdapter;

    public String req_url, lv_actn_url;
    public String status, lvId,  clearance="0", replaced_required="0", handover ="0";


    public   LeaveRequestList leave;
    private ProgressDialog progressDialog;
    private CardView emptycard;

    public ApprovDialog approvDialogObj;
    private EventBus eventBus = EventBus.getDefault();

    Context context;

    ViewGroup rootView;

    public RequestApprovalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = (ViewGroup)  inflater.inflate(R.layout.request_approval_fragment, container, false);

        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclr_lv_apprvl_list);

        emptycard = (CardView)rootView.findViewById(R.id.empty_card);
        emptycard.setVisibility(View.INVISIBLE);


        context = getActivity().getApplicationContext();
        loadLvApprvList();


        return rootView;
    }


    @Override
    public void onStart() {
        super.onStart();

        if (!EventBus.getDefault().isRegistered(this)) {
            eventBus.register(this); // registering the bus
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        eventBus.unregister(this); // registering the bus
    }

    public void loadLvApprvList(){

        apprvList  = new ArrayList<>();

        req_url = Constants.LEAVE_APPRV_LIST_URL;

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        loadData(progressDialog);


    }

    //SENDING THE APPROVE LEAVE REQUEST
    public void leavAction(JSONObject jsonObject){

        Log.d("LEAVE ACTION ", jsonObject.toString());
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        lv_actn_url = Constants.LEAVE_ACTION_URL;

        WebManager webManager = new WebManager(context);

        webManager.leavAction(lv_actn_url, jsonObject ,new WebManager.VolleyCallback() {
            @Override
            public void onSuccess(String result) {

                try {
                    JSONObject responObj = new JSONObject(result);
                    progressDialog.dismiss();


                    if(responObj.getString("result").equals("1")){

                        loadLvApprvList();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String result) {
                progressDialog.dismiss();

            }
        });
    }

    //DETAILED VIEW ACTION
    public void empDetView(int position, ArrayList<Employee> empHandoverList){

        LeaveRequestList lv = apprvList.get(position);
        Intent intent = new Intent(getActivity(), LeaveApprvDetActivity.class);
        intent.putExtra("LEAVE OBJECT", lv);
        intent.putExtra("HAND OVER EMPLOYES", empHandoverList);

        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // In fragment class callback

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {

            if(resultCode == 0){
                selectedEmpList = new ArrayList<>();

                lvId = data.getStringExtra("LEAVE ID");

                Bundle bundle = data.getExtras();
                leave = (LeaveRequestList) bundle.getSerializable("LEAVE");

                ArrayList<Employee> testlist = (ArrayList<Employee>) data.getSerializableExtra("ARRAYLIST");

                for (int i =0 ;i<testlist.size(); i++){

                    Employee test = testlist.get(i);
                }

                approvDialogObj = new ApprovDialog(this);
                approvDialogObj.loadApprovDialog(getContext(), "REQUEST_APPROVAL_FRAGMENT", (ArrayList<Employee>) data.getSerializableExtra("ARRAYLIST"), null, "approve", lvId,null,leave);

            }

        }
        else if (requestCode == 2){
            loadLvApprvList();
        }

    }



    @Subscribe( threadMode = ThreadMode.MAIN)
    public void onEvent(Bundle notification) {
        this.context = context;

        if (notification.getString("route").equals(Constants.EmployementPortal.LEAVE)){

            if (notification.getString("action").equals(Constants.Actions.CREATE)){
                loadData(null);
            }
        }


    };


    public void loadData(final ProgressDialog progressDialog){

        context = getActivity().getApplicationContext();
        WebManager webManager = new WebManager(context);

        webManager.leavApprvList(req_url, new WebManager.VolleyCallback(){

            @Override
            public void onSuccess(String result) {

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    JSONObject jData = jsonObject.getJSONObject("data");

                    if (progressDialog!=null)
                    progressDialog.dismiss();

                    if (!jData.isNull("leaveRequest")){

                        JSONArray jLvApprv = jData.getJSONArray("leaveRequest");

                        for (int i =0; i<jLvApprv.length(); i++){
                            JSONObject jsonLv = jLvApprv.getJSONObject(i);
                            LeaveDetail leaveDetail = new LeaveDetail();
                            apprvList.add(leaveDetail.getLeaveData(jsonLv));
                        }

                        JSONArray jEmpApprv = jData.getJSONArray("employees");
                        for (int i=0; i<jEmpApprv.length(); i++){

                            Log.d("rererer", "RRr");
                            JSONObject jsonEmp = jEmpApprv.getJSONObject(i);
                            LeaveDetail leaveDetail = new LeaveDetail();
                            empHandoverList.add(leaveDetail.getHandOverEmplys(jsonEmp));


                        }


                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
                        recyclerView.setLayoutManager(mLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(new LeaveApprvListAdaptr(context, apprvList, empHandoverList, RequestApprovalFragment.this, new LeaveApprvListAdaptr.OnItemClickListener() {
                            @Override
                            public void onItemClick(View itemView, int position) {
                                LeaveRequestList lv = apprvList.get(position);

                                empDetView(position, empHandoverList);
                            }
                        }));
                        emptycard.setVisibility(View.INVISIBLE);
                    }else {
                        recyclerView.setVisibility(View.INVISIBLE);
                        emptycard.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String result) {

            }
        });

    }



    public void loadFilter(final Context context, RequestApprovalFragment reqAprvlFrag, String status){

        reqAprvlSortList = new ArrayList<>();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        reqAprvlFrag.recyclerView.setLayoutManager(mLayoutManager);
        reqAprvlFrag.recyclerView.setItemAnimator(new DefaultItemAnimator());

        if(status.equals(" ")){

            reqAprvlSortList = reqAprvlFrag.apprvList;
        }else {

            for (int i =0; i<reqAprvlFrag.apprvList.size(); i++){

                leave = reqAprvlFrag.apprvList.get(i);
                Log.d("STATUSS ", leave.getLeave_status());
                if(leave.getLeave_status().equals(status)){

                    reqAprvlSortList.add(leave);
                }
            }
        }

        reqAprvlFrag.recyclerView.setLayoutManager(mLayoutManager);
        reqAprvlFrag.recyclerView.setItemAnimator(new DefaultItemAnimator());
        reqAprvlFrag.recyclerView.setAdapter(new LeaveApprvListAdaptr(context, reqAprvlSortList, empHandoverList, RequestApprovalFragment.this, new LeaveApprvListAdaptr.OnItemClickListener() {
            @Override
            public void onItemClick(View itemView, int position) {

                LeaveRequestList lv = reqAprvlSortList.get(position);
                Intent intent = new Intent(getActivity(), LeaveApprvDetActivity.class);
                intent.putExtra("LEAVE OBJECT", lv);
                intent.putExtra("HAND OVER EMPLOYES", empHandoverList);

                startActivity(intent);
            }
        }));




    }

}

