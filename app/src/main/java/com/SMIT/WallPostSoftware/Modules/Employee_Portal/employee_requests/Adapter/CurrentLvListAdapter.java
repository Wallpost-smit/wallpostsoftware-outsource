package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Extras.CircularTextView;
import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.Preference;
import com.SMIT.WallPostSoftware.Extras.Util;
import com.SMIT.WallPostSoftware.Extras.WebManager;
import com.SMIT.WallPostSoftware.Model_Class.LeaveRequestList;
import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.CurrentLvFragment;
import com.SMIT.WallPostSoftware.R;
import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by alex on 01/09/16.
 */
public class CurrentLvListAdapter extends RecyclerView.Adapter<CurrentLvListAdapter.LvListViewHolder> {
    private List<LeaveRequestList> leavList;
    private Context context;
    private CurrentLvFragment leaveRequestFragment;

    public String employeeId, requrl, lvId, resn;

    public String jResult, jMessage;



    // Define listener member variable
    private OnItemClickListener listener;
    // Define the listener interface
    public interface OnItemClickListener {
        void onItemClick(View itemView, int position);

    }
    // Define the method that allows the parent activity or fragment to define the listener
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public class LvListViewHolder extends RecyclerView.ViewHolder {
        public TextView _lvNameTv, _lvFrmTv, _lvToTv, _lvResn, _lvStatus,_lvLeaveCmtTv;
        public CardView _lvHandoverButton,_lvcancelButton;
        public ImageView _lvStatusImg;
        public LinearLayout _statusLayoutColor,_leaveCommentLayout;
        public CircularTextView _lvDays;
        public LvListViewHolder(final View itemView) {
            super(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Triggers click upwards to the adapter on click
                    if (listener != null)
                        listener.onItemClick(itemView, getLayoutPosition());
                }
            });


            _statusLayoutColor = (LinearLayout) itemView.findViewById(R.id.status_color_layout);
            _leaveCommentLayout = (LinearLayout) itemView.findViewById(R.id.leave_comment_layout);
            _lvLeaveCmtTv = (TextView) itemView.findViewById(R.id.leave_comment);
            _lvNameTv = (TextView) itemView.findViewById(R.id.leave_heading);
            _lvFrmTv = (TextView) itemView.findViewById(R.id.leave_frm);
            _lvToTv = (TextView) itemView.findViewById(R.id.leave_to);
            _lvStatus = (TextView) itemView.findViewById(R.id.leave_status);
            _lvDays = (CircularTextView) itemView.findViewById(R.id.leave_days);
            _lvHandoverButton = (CardView) itemView.findViewById(R.id.handover_button);
            _lvcancelButton = (CardView) itemView.findViewById(R.id.cancel_button);
            //_lvDays.setBackgroundColor(Color.parseColor("#00BCD4"));
            _lvStatusImg = (ImageView) itemView.findViewById(R.id.leave_status_img);
        }


    }

    @Override
    public void onBindViewHolder(LvListViewHolder holder, int position) {

        Log.d("LISSSt ", leavList.toString());
        final LeaveRequestList lv = leavList.get(position);
        holder._lvNameTv.setText(lv.getLeave_type());
        holder._lvFrmTv.setText(lv.getLeave_frm());
        holder._lvToTv.setText(lv.getLeave_to());
        holder._lvStatus.setText(lv.getLeave_status());
        holder._lvHandoverButton.setVisibility(View.GONE);
        holder._lvcancelButton.setVisibility(View.VISIBLE);
        holder._lvcancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d("POSITION ", lv.getLeave_id());

                MaterialDialog dialog = new MaterialDialog.Builder(context)
                        .title("Are you sure want to cancel the leave ?")
                        .customView(R.layout.cancel_reason, true)
                        .negativeText("No")
                        .positiveText("Yes")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                //showToast("Password: " + passwordInput.getText().toString());

                                EditText _resn = (EditText) dialog.findViewById(R.id.reason_cancel_lv);
                                if(_resn.getText().length()>1){
                                    dialog.dismiss();
                                    cancelLeave(lv.getLeave_id(), _resn.getText().toString());
                                }else {
                                    android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                                    alertDialog.setTitle("Sorry!");
                                    alertDialog.setMessage("Reason cannot be empty");
                                    alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });
                                    alertDialog.show();
                                }
                            }

                        })
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .build();

                dialog.show();

            }
        });

        //Krishna Code
        holder._lvLeaveCmtTv.setEllipsize(TextUtils.TruncateAt.END);
        holder._lvLeaveCmtTv.setMaxLines(1);

        switch(lv.getLeave_status()) {
            case "0":
                holder._lvStatus.setText(Constants.LeaveStatus.PENDING);
                holder._lvStatusImg.setColorFilter(context.getResources().getColor(R.color.colorYellow));
                holder._lvStatus.setTextColor(context.getResources().getColor(R.color.colorYellow));
                holder._statusLayoutColor.setBackgroundColor(context.getResources().getColor(R.color.colorYellow));
                holder._lvHandoverButton.setVisibility(View.INVISIBLE);

                // Check if current date is greater then leave requested date
                if (Util.getCurrentDate().after(Util.getFormattedDateFromString(lv.getLeave_frm()))){
                    holder._lvcancelButton.setVisibility(View.INVISIBLE);
                }
                    holder._leaveCommentLayout.setVisibility(View.GONE);
                break;

            case "1":
                holder._lvStatus.setText(Constants.LeaveStatus.APPROVED);
                holder._lvStatusImg.setColorFilter(context.getResources().getColor(R.color.colorGreen));
                holder._lvStatus.setTextColor(context.getResources().getColor(R.color.colorGreen));
                holder._statusLayoutColor.setBackgroundColor(context.getResources().getColor(R.color.colorGreen));
                holder._lvHandoverButton.setVisibility(View.INVISIBLE);
                // Check if current date is greater then leave requested date
                if (Util.getCurrentDate().after(Util.getFormattedDateFromString(lv.getLeave_frm()))){
                    holder._lvcancelButton.setVisibility(View.INVISIBLE);
                }

                if (lv.getApproval_comments().isEmpty()){
                    holder._leaveCommentLayout.setVisibility(View.GONE);
                }else{
                    holder._leaveCommentLayout.setVisibility(View.VISIBLE);
                    holder._lvLeaveCmtTv.setText(lv.getApproval_comments());

                }
              break;

            case "2":
                holder._lvStatus.setText(Constants.LeaveStatus.REJECTED);
                holder._lvStatusImg.setColorFilter(context.getResources().getColor(R.color.colorRed));
                holder._lvStatus.setTextColor(context.getResources().getColor(R.color.colorRed));
                holder._statusLayoutColor.setBackgroundColor(context.getResources().getColor(R.color.colorRed));
                holder._lvHandoverButton.setVisibility(View.INVISIBLE);
                holder._lvcancelButton.setVisibility(View.INVISIBLE);
                break;

            case "3":
                holder._lvStatus.setText(Constants.LeaveStatus.CANCELLED);
                holder._lvStatusImg.setColorFilter(context.getResources().getColor(R.color.colorOrange));
                holder._lvStatus.setTextColor(context.getResources().getColor(R.color.colorOrange));
                holder._statusLayoutColor.setBackgroundColor(context.getResources().getColor(R.color.colorOrange));
                holder._lvHandoverButton.setVisibility(View.INVISIBLE);
                holder._lvcancelButton.setVisibility(View.INVISIBLE);
                break;
            default:

        }


        holder._lvDays.setText(lv.getLeave_days()+" "+ "day(s)");
        holder._lvDays.setStrokeWidth(2);



    }

    public CurrentLvListAdapter(List<LeaveRequestList> lvList, CurrentLvFragment requestFragment, OnItemClickListener listener) {
        this.leavList= lvList;
        this.listener = listener;
        leaveRequestFragment=requestFragment;
    }

    @Override
    public LvListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.leave_list_row, parent, false);
        context=parent.getContext();
        return new LvListViewHolder(itemView);
    }

    @Override
    public int getItemCount() {
        return leavList.size();
    }

    //Cancel Leave
    public void cancelLeave(String leave_id, String reasn) {

        Log.d("TRUE ", "CLICKEd");
        this.lvId = leave_id;
        this.resn = reasn;

        Preference pref= new Preference(context,"login");
        employeeId = pref.getCompany().employee.empId;

        requrl = Constants.LEAVE_CANCEL_URL+employeeId;


        WebManager webManager = new WebManager(context);
        webManager.cancelLvReq(requrl, lvId, resn , new WebManager.VolleyCallback() {
            @Override
            public void onSuccess(String result) {

                Log.d("CANCEL SUCESS ", result);

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    jResult = jsonObject.getString("result");
                    jMessage = jsonObject.getString("message");
                    if(jResult.equals("1")){


                        leaveRequestFragment.loadLvList();

                    }else {
                        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(context).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage(jMessage);
                        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String result) {

            }
        });

    }

}
