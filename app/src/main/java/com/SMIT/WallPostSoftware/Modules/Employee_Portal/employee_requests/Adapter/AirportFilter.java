package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter;

import android.widget.Filter;

import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.AirportData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 24/08/16.
 */
public class AirportFilter extends Filter {

    AirportAdapter adapter;
    List<AirportData> originalList;
    List<AirportData> filteredList;


    public AirportFilter(AirportAdapter adapter, List<AirportData> originalList) {
        super();
        this.adapter = adapter;
        this.originalList = originalList;
        this.filteredList = new ArrayList<>();
    }


    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        filteredList.clear();
        final FilterResults results = new FilterResults();

        if (constraint == null || constraint.length() == 0) {
            filteredList.addAll(originalList);
        } else {
            final String filterPattern = constraint.toString().toLowerCase().trim();

            // Your filtering logic goes in here
            for (final AirportData airportData : originalList) {
                if (airportData.getAirportName().toLowerCase().contains(filterPattern)) {
                    filteredList.add(airportData);
                }
            }
        }
        results.values = filteredList;
        results.count = filteredList.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {

        adapter.filteredAirport.clear();
        adapter.filteredAirport.addAll((List) filterResults.values);
        adapter.notifyDataSetChanged();
    }
}
