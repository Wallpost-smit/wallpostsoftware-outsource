package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class;

/**
 * Created by alex on 19/08/16.
 */
public class LeaveType {

    public String _lvId;
    public String _lvType;

    public String requires_certificate;

    public String min_period;

    public String get_lvId() {
        return _lvId;
    }

    public void set_lvId(String _lvId) {
        this._lvId = _lvId;
    }

    public String get_lvType() {
        return _lvType;
    }

    public void set_lvType(String _lvType) {
        this._lvType = _lvType;
    }

    public String getRequires_certificate() {
        return requires_certificate;
    }

    public void setRequires_certificate(String requires_certificate) {
        this.requires_certificate = requires_certificate;
    }

    public String getMin_period() {
        return min_period;
    }

    public void setMin_period(String min_period) {
        this.min_period = min_period;
    }
}
