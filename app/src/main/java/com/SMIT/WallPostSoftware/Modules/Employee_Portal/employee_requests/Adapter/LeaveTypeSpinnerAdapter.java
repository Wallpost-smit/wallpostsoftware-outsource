package com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.SMIT.WallPostSoftware.Modules.Employee_Portal.employee_requests.Model_Class.LeaveType;

import java.util.ArrayList;

/**
 * Created by alex on 19/08/16.
 */
public class LeaveTypeSpinnerAdapter extends ArrayAdapter<LeaveType> {

    private Context context;
    private ArrayList<LeaveType> lvType;

    public LeaveTypeSpinnerAdapter(Context context, int textViewResourceId,
                               ArrayList<LeaveType> lvType) {
        super(context, textViewResourceId, lvType);
        this.context = context;
        this.lvType = lvType;
    }

    public int getCount(){
        return lvType.size();
    }

    public LeaveType getItem(int position){
        return lvType.get(position);
    }

    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setText(lvType.get(position).get_lvType());

        return label;
    }

    @Override
    public View getDropDownView(int position, View convertView,
                                ViewGroup parent) {
        TextView label = new TextView(context);
        label.setTextColor(Color.BLACK);
        label.setPadding(30,30,30,30);
        label.setText(lvType.get(position).get_lvType());

        return label;
    }
}
