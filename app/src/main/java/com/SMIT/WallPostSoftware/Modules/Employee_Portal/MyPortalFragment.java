package com.SMIT.WallPostSoftware.Modules.Employee_Portal;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.SMIT.WallPostSoftware.Adapter.MyPortalRequestAdapter;
import com.SMIT.WallPostSoftware.Extras.DividerItemDecoration;
import com.SMIT.WallPostSoftware.Extras.VerticalSpaceItemDecoration;
import com.SMIT.WallPostSoftware.Model_Class.MyPortal_Approval;
import com.SMIT.WallPostSoftware.Adapter.MyPortalApprovalAdapter;
import com.SMIT.WallPostSoftware.Model_Class.MyPortal_Request;
import com.SMIT.WallPostSoftware.R;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MyPortalFragment extends Fragment implements View.OnClickListener{

    public Button _approvalButton;
    public Button _myReqButton;
    private List<MyPortal_Approval> myPortal_approvals = new ArrayList<>();
    private List<MyPortal_Request> myPortal_requests = new ArrayList<>();

    private RecyclerView _approvalRecyView, _requestRecyView;
    private MyPortalApprovalAdapter approvalAdapter;
    private MyPortalRequestAdapter requestAdapter;
    ViewGroup rootView;
    int flag_i=0, flag_j=0, apprvl_flag = 0, my_req_flag = 0;

    public Boolean apprvClicked = false;
    public Boolean reqClicked = false;

    public MyPortalFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d("MY PORTAL ", "Fragment");

        rootView = (ViewGroup) inflater.inflate(
                R.layout.my_portal_fragment, container, false);

        _approvalRecyView = (RecyclerView) rootView.findViewById(R.id.recyclr_apprvl);
        _requestRecyView = (RecyclerView) rootView.findViewById(R.id.recyclr_reqst);


        MyPortal_Approval approval = new MyPortal_Approval("Kevin John ", "Annual leave");
        myPortal_approvals.add(approval);
        approval = new MyPortal_Approval("Test",  "2015");
        myPortal_approvals.add(approval);
        approval = new MyPortal_Approval("Dummy", "2015");
        myPortal_approvals.add(approval);

        approvalAdapter = new MyPortalApprovalAdapter(myPortal_approvals);


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        _approvalRecyView.setLayoutManager(mLayoutManager);
        _approvalRecyView.setItemAnimator(new DefaultItemAnimator());
        _approvalRecyView.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.horizontal_dividr));


        _approvalRecyView.setAdapter(approvalAdapter);
        approvalAdapter.notifyDataSetChanged();


        MyPortal_Request request = new MyPortal_Request("TEST  ", "HELLO");
        myPortal_requests.add(request);
        request = new MyPortal_Request("TEST  ", "HELLO");
        myPortal_requests.add(request);

        requestAdapter = new MyPortalRequestAdapter(myPortal_requests);
        RecyclerView.LayoutManager nLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        _requestRecyView.setLayoutManager(nLayoutManager);
        _requestRecyView.setItemAnimator(new DefaultItemAnimator());
        _requestRecyView.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.horizontal_dividr));
        _requestRecyView.setAdapter(requestAdapter);
        requestAdapter.notifyDataSetChanged();


        _approvalButton = (Button) rootView.findViewById(R.id.apprvl_button);
        _approvalButton.setOnClickListener(this);
        _myReqButton = (Button) rootView.findViewById(R.id.req_button);
        _myReqButton.setOnClickListener(this);

        return rootView;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){

            case R.id.apprvl_button:
                if(!apprvClicked){
                    _approvalRecyView.setVisibility(View.VISIBLE);
                    _requestRecyView.setVisibility(View.GONE);
                    apprvClicked = true;
                    reqClicked = false;
                }else {
                    _approvalRecyView.setVisibility(View.GONE);
                    _requestRecyView.setVisibility(View.GONE);
                    apprvClicked = false;
                }
                break;

            case R.id.req_button:
                if(!reqClicked){
                    _approvalRecyView.setVisibility(View.GONE);
                    _requestRecyView.setVisibility(View.VISIBLE);
                    reqClicked = true;
                    apprvClicked = false;
                }else {
                    _requestRecyView.setVisibility(View.GONE);
                    _approvalRecyView.setVisibility(View.GONE);
                    reqClicked = false;
                }
                break;
        }
    }
}
