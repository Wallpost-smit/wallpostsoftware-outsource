package com.SMIT.WallPostSoftware;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.SMIT.WallPostSoftware.Extras.Constants;
import com.SMIT.WallPostSoftware.Extras.PermissionUtility;
import com.SMIT.WallPostSoftware.Extras.PhoneDataUtility;
import com.SMIT.WallPostSoftware.Extras.Preference;
import com.SMIT.WallPostSoftware.Extras.WebManager;
import com.SMIT.WallPostSoftware.GCM.QuickstartPreferences;
import com.SMIT.WallPostSoftware.GCM.RegistrationIntentService;
import com.SMIT.WallPostSoftware.Model_Class.CompanyList;
import com.SMIT.WallPostSoftware.Model_Class.Employee;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import rebus.permissionutils.PermissionEnum;
import rebus.permissionutils.PermissionUtils;

public class WP_EMP_LoginActivity extends AppCompatActivity {

    public String req_url;
    public String app_version;
    public String device_id;
    public String device_model;
    public String device_os;
    public String tag_json_obj = "json_login_req";
    public String gcm;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "WP_EMP_LoginActivity";
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private boolean isReceiverRegistered;
    boolean result;

    public String jStatus, jAuthToken, jUserId, jMessage, jUserType, jUserName, jProfileImageUrl, jEmail,jEmployeeID;
    public Button _loginButton;
    public EditText _userName;
    public EditText _userPassword;
    public EditText _userAccountNo;
    TextInputLayout _inputLayoutUserName,_inputLayoutUserPassword,_inputLayoutUserAccountNo;

    private ProgressDialog progressDialog;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);


        PermissionEnum permissionEnum = PermissionEnum.WRITE_EXTERNAL_STORAGE;
        boolean granted = PermissionUtils.isGranted(WP_EMP_LoginActivity.this, PermissionEnum.READ_PHONE_STATE);
        result = granted;
        _inputLayoutUserName = (TextInputLayout) findViewById(R.id.input_layout_user_name);
        _inputLayoutUserPassword = (TextInputLayout) findViewById(R.id.input_layout_password);
        _inputLayoutUserAccountNo = (TextInputLayout) findViewById(R.id.input_layout_account_no);

        _userName = (EditText) findViewById(R.id.user_name);
        _userPassword = (EditText) findViewById(R.id.password);
        _userAccountNo = (EditText) findViewById(R.id.account_no);
        _loginButton = (Button) findViewById(R.id.login_button);

        _userName.addTextChangedListener(new MyTextWatcher(_userName));
        _userPassword.addTextChangedListener(new MyTextWatcher(_userPassword));
        _userAccountNo.addTextChangedListener(new MyTextWatcher(_userAccountNo));

        PhoneDataUtility phoneDataUtility = new PhoneDataUtility();
        app_version = phoneDataUtility.getAppVersion(WP_EMP_LoginActivity.this);
        device_id = phoneDataUtility.getDeviceId(WP_EMP_LoginActivity.this);
        device_model = phoneDataUtility.getDeviceName();
        device_os = phoneDataUtility.getDeviceOs();

        mRegistrationBroadcastReceiver = new BroadcastReceiver(){

            @Override
            public void onReceive(Context context, Intent intent) {

                gcm = intent.getStringExtra("TOKEN");
                Preference pref = new Preference(getApplicationContext(),"login");
                pref.setGCM(gcm);
                Log.d("GCM  ", gcm);
            }
        };

        registerReceiver();

        if (checkPlayServices()) {
            // Start IntentService to register this application with GCM.
            Intent intent = new Intent(this, RegistrationIntentService.class);
            startService(intent);

        }

        _loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(result){
                submitForm();

                }else {

                    android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(WP_EMP_LoginActivity.this).create();
                    alertDialog.setTitle("Alert");
                    alertDialog.setMessage("Phone data Permission needed");
                    alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            });
                    alertDialog.show();
                }

            }
        });


    }

    /**
     * Validating form
     */
    private void submitForm() {
        if (!validateAccountNumber()) {
            return;
        }

        if (!validateUserName()) {
            return;
        }

        if (!validatePassword()) {
            return;
        }

        login();
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.user_name:
                    validateUserName();
                    break;
                case R.id.account_no:
                    validateAccountNumber();
                    break;
                case R.id.password:
                    validatePassword();
                    break;
            }
        }
    }

    private boolean validateAccountNumber() {
        if (_userAccountNo.getText().toString().trim().isEmpty()) {
            _inputLayoutUserAccountNo.setErrorEnabled(true);
            _inputLayoutUserAccountNo.setError(getString(R.string.err_msg_account_no));
            requestFocus(_userAccountNo);
            return false;
        } else {
            _inputLayoutUserAccountNo.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateUserName() {
        if (_userName.getText().toString().trim().isEmpty()) {
            _inputLayoutUserName.setErrorEnabled(true);
            _inputLayoutUserName.setError(getString(R.string.err_msg_user_name));
            requestFocus(_userName);
            return false;
        } else {
            _inputLayoutUserName.setErrorEnabled(false);
        }

        return true;
    }


    private boolean validatePassword() {
        if (_userPassword.getText().toString().trim().isEmpty()) {
            _inputLayoutUserPassword.setErrorEnabled(true);
            _inputLayoutUserPassword.setError(getString(R.string.err_msg_password));
            requestFocus(_userPassword);
            return false;
        } else {
            _inputLayoutUserPassword.setErrorEnabled(false);
        }

        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();

    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        isReceiverRegistered = false;
        super.onPause();
    }

    private void registerReceiver(){
        if(!isReceiverRegistered) {
            LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                    new IntentFilter(QuickstartPreferences.REGISTRATION_COMPLETE));

            LocalBroadcastManager.getInstance(this).registerReceiver(
                    mRegistrationBroadcastReceiver, new IntentFilter("SERVER_TOKEN"));
            isReceiverRegistered = true;

        }
    }

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else {
                Log.i(TAG, "This device is not supported.");
                finish();
            }
            return false;
        }
        return true;
    }
    public  String getDeviceUid(Context context) {

        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public void login(){

        req_url = Constants.LOGIN_URL;

        progressDialog = new ProgressDialog(WP_EMP_LoginActivity.this);
        progressDialog.setTitle("Processing...");
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        WebManager webManager = new WebManager(getApplicationContext());
        webManager.addParams("accountno",_userAccountNo.getText().toString());
        webManager.addParams("apptype","MOBILE");
        webManager.addParams("username",_userName.getText().toString());
        webManager.addParams("password",_userPassword.getText().toString());
        webManager.addParams("environment","Development");
        webManager.addParams("deviceuid",getDeviceUid(getApplicationContext()));
        webManager.addParams("deviceos","ANDROID");
        webManager.addParams("devicetoken",gcm);

        webManager.Login(req_url, new WebManager.VolleyCallback(){

            @Override
            public void onSuccess(String result) {
                Log.d("LOGIN SUCC ", result);
                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(result);


                    if (jsonObject.has("error")){

                            android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(WP_EMP_LoginActivity.this).create();
                            alertDialog.setTitle("Alert");
                            alertDialog.setMessage(jsonObject.getString("reason"));
                            alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            dialog.dismiss();
                                        }
                                    });
                            alertDialog.show();

                    }

                    else if(jsonObject.getString("status").equals("success")){
                        Preference preference = new Preference(getApplicationContext(),"login");
                        preference.clearPreference();

                        jAuthToken = jsonObject.getString("auth_token");
                        Log.d("AUTH TOKEN ", jAuthToken);
                        JSONArray jCmpnyArry = jsonObject.getJSONArray("companies");

                        // Store auth token & loggedIn in preference
                        preference.setLogged(true);
                        preference.setAuthToken(jAuthToken);
                        preference.setCompanyList(jCmpnyArry.toString());
                        if (jCmpnyArry.length() == 1){
                            JSONObject jCmpnyCell = jCmpnyArry.getJSONObject(0);
                            CompanyList company = new CompanyList();
                            company.setCompanyId(jCmpnyCell.getString("company_id"));
                            company.setCompanyName(jCmpnyCell.getString("company_name"));
                            company.setCompanyLogo(jCmpnyCell.getString("company_logo"));
                            company.setCompanyShortName(jCmpnyCell.getString("short_name"));

                            Employee employee = new Employee();
                            JSONObject jEmpCell = jCmpnyCell.getJSONObject("employee");
                            employee.setEmpId(jEmpCell.getString("employee_id"));
                            employee.setEmpName(jEmpCell.getString("name"));
                            employee.setEmpEmail(jEmpCell.getString("email"));
                            employee.setEmpProfilePic(jEmpCell.getString("profile_image"));
                            employee.setEmpDesignation(jEmpCell.getString("designation"));
                            employee.setEmpLineManager(jEmpCell.getString("line_manager"));
                            company.setEmployee(employee);

                             preference.setCompany(company);



                            Intent intent = new Intent(WP_EMP_LoginActivity.this, HomeScreenActivity.class);
                            startActivity(intent);
                        }else{
                            // Store company JSON in preference

                            Intent intent = new Intent(WP_EMP_LoginActivity.this, CompanyListActivity.class);
                            startActivity(intent);
                        }


                    }
                    else{

                        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(WP_EMP_LoginActivity.this).create();
                        alertDialog.setTitle("Alert");
                        alertDialog.setMessage("Some went wrong. Please try after sometime!");
                        alertDialog.setButton(android.app.AlertDialog.BUTTON_NEUTRAL, "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.dismiss();
                                    }
                                });
                        alertDialog.show();
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(String result) {
                progressDialog.dismiss();
            }
        });

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);

    }
}
